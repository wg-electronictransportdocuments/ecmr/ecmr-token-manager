/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/* istanbul ignore file */
import { Injectable, Logger, NotFoundException } from '@nestjs/common';

import { SystemMessage } from '../../../messages';
import { TendermintService } from '../../../tendermint/service/tendermint.service';
import { SaveHashTokenDTO } from '../dto/save-HashToken.dto';
import {
  QueryGetDocumentHashHistoryResponse,
  QueryGetDocumentHashResponse,
} from 'src/tendermint/controller/dto/queryHashToken';
import { MsgCreateHashToken, MsgFetchDocumentHash, MsgFetchDocumentHashHistory } from 'src/generated/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic/module/types/businesslogic/tx';

@Injectable()
export class HashTokenService {
  private logger = new Logger('HashTokenController');
  constructor(private tendermintService: TendermintService) {}

  private TENDERMINT_TYPE_URL_BUSINESSLOGIC = '/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic';
  private MSG_FIND_BY_ID = 'MsgFetchDocumentHash';
  private MSG_FIND_BY_ID_RESPONSE = 'QueryGetDocumentHashResponse';
  private MSG_FIND_HISTORY_BY_ID = 'MsgFetchDocumentHashHistory';
  private MSG_FIND_HISTORY_BY_ID_RESPONSE = 'QueryGetDocumentHashHistoryResponse';
  private MSG_CREATE = 'MsgCreateHashToken';
  private MSG_CREATE_RESPONSE = 'MsgIdResponse';

  private mnemonic = process.env.MNEMONIC;
  private blockchainAddress = process.env.WALLET;

  public async findHistory(id: string): Promise<QueryGetDocumentHashHistoryResponse> {
    const msg: MsgFetchDocumentHashHistory = {
      creator: '',
      id: id,
    };

    const result: QueryGetDocumentHashHistoryResponse = await this.tendermintService.decodeResult(
      await this.tendermintService.buildSignAndBroadcast(
        this.mnemonic,
        [msg],
        this.MSG_FIND_HISTORY_BY_ID,
        this.TENDERMINT_TYPE_URL_BUSINESSLOGIC
      ),
      `${this.TENDERMINT_TYPE_URL_BUSINESSLOGIC}.${this.MSG_FIND_HISTORY_BY_ID_RESPONSE}`
    );

    if (!result) throw new NotFoundException(SystemMessage.ERROR_NOT_FOUND);
    return result;
  }

  public async findById(id: string): Promise<QueryGetDocumentHashResponse> {
    const msg: MsgFetchDocumentHash = {
      creator: '',
      id: id,
    };

    const result: QueryGetDocumentHashResponse = await this.tendermintService.decodeResult(
      await this.tendermintService.buildSignAndBroadcast(
        this.mnemonic,
        [msg],
        this.MSG_FIND_BY_ID,
        this.TENDERMINT_TYPE_URL_BUSINESSLOGIC
      ),
      `${this.TENDERMINT_TYPE_URL_BUSINESSLOGIC}.${this.MSG_FIND_BY_ID_RESPONSE}`
    );

    if (!result) throw new NotFoundException(SystemMessage.ERROR_NOT_FOUND);
    return result;
  }

  public async createOne(msg: SaveHashTokenDTO): Promise<QueryGetDocumentHashResponse> {
    const request: MsgCreateHashToken = {
      creator: this.blockchainAddress,
      changeMessage: msg.changeMessage,
      segmentId: '0',
      document: msg.document,
      hash: msg.hash,
      hashFunction: msg.hashFunction,
      metadata: msg.metadata,
    };

    const result = await this.tendermintService.buildSignAndBroadcast(
      this.mnemonic,
      [request],
      this.MSG_CREATE,
      this.TENDERMINT_TYPE_URL_BUSINESSLOGIC
    );
    
    console.log(result);

    await this.tendermintService.decodeResult(
      result, `${this.TENDERMINT_TYPE_URL_BUSINESSLOGIC}.${this.MSG_CREATE_RESPONSE}`
    );

    return this.findById(msg.document);
  }
}
