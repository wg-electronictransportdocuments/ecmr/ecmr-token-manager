/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpModule } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { TendermintService } from '../../../tendermint/service/tendermint.service';
import { TokenmanagerModule } from '../../tokenmanager-module';
import { HashTokenService } from './hashToken.service';
import { SaveHashTokenDTO } from '../dto/save-HashToken.dto';
import {
  QueryGetDocumentHashHistoryResponse,
  QueryGetDocumentHashResponse,
} from 'src/tendermint/controller/dto/queryHashToken';

describe('TransportPaperService', () => {
  let service: HashTokenService;
  let blockchain: TendermintService;

  const hashResponse: QueryGetDocumentHashResponse = {
    creator: 'CREATOR',
    timestamp: 'TIMESTAMP',
    hash: 'HASH',
    hashFunction: 'HASH_FUNCTION',
    metadata: 'METADATA',
  };

  const historyResponse: QueryGetDocumentHashHistoryResponse = {
    history: [hashResponse, hashResponse],
  };

  const tokenRequest: SaveHashTokenDTO = {
    changeMessage: 'CHANGE_MESSAGE',
    document: 'DOCUMENT',
    hash: 'HASH',
    hashFunction: 'HASH_FUNCTION',
    metadata: 'METADATA',
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [HttpModule],
      controllers: [HashTokenService],
      providers: [TendermintService, TokenmanagerModule],
    }).compile();

    service = module.get<HashTokenService>(HashTokenService);
    blockchain = module.get<TendermintService>(TendermintService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('createOne', () => {
    it('should store a new hash', async () => {
      spyOn(blockchain, 'buildSignAndBroadcast').and.returnValue(0);
      spyOn(blockchain, 'decodeResult').and.returnValue(hashResponse);

      expect(await service.createOne(tokenRequest)).toEqual(hashResponse);
    });

    it('should throw an exception', async () => {
      spyOn(blockchain, 'query').and.throwError('error');
      expect(service.createOne(tokenRequest)).rejects.toThrow();
    });
  });

  describe('findOne', () => {
    it('should find a document', async () => {
      spyOn(blockchain, 'buildSignAndBroadcast').and.returnValue(0);
      spyOn(blockchain, 'decodeResult').and.returnValue(hashResponse);
      expect(await service.findById('document')).toEqual(hashResponse);
    });

    it('should throw an exception', async () => {
      spyOn(blockchain, 'buildSignAndBroadcast').and.returnValue(0);
      spyOn(blockchain, 'decodeResult').and.returnValue(null);
      expect(service.findById('document')).rejects.toThrow();
    });
  });

  describe('findHistory', () => {
    it('should find a documents history', async () => {
      spyOn(blockchain, 'buildSignAndBroadcast').and.returnValue(0);
      spyOn(blockchain, 'decodeResult').and.returnValue(historyResponse);
      expect(await service.findHistory('document')).toEqual(historyResponse);
    });

    it('should throw an exception', async () => {
      spyOn(blockchain, 'buildSignAndBroadcast').and.returnValue(0);
      spyOn(blockchain, 'decodeResult').and.returnValue(null);
      expect(service.findHistory('document')).rejects.toThrow();
    });
  });
});
