/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ApiProperty } from '@nestjs/swagger';

export class SaveHashTokenDTO {
  @ApiProperty() changeMessage: string;
  @ApiProperty() document: string;
  @ApiProperty() hash: string;
  @ApiProperty() hashFunction: string;
  @ApiProperty() metadata: string;
}
