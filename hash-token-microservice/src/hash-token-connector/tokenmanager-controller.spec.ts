/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpModule, NotFoundException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import {
  QueryGetDocumentHashHistoryResponse,
  QueryGetDocumentHashResponse,
} from 'src/tendermint/controller/dto/queryHashToken';
import { TendermintService } from '../tendermint/service/tendermint.service';
import { SaveHashTokenDTO } from './hashToken/dto/save-HashToken.dto';
import { HashTokenService } from './hashToken/service/hashToken.service';
import { TokenManagerController } from './tokenmanager-controller';

describe('TokenManagerController', () => {
  let controller: TokenManagerController;
  let service: HashTokenService;

  const hashResponse: QueryGetDocumentHashResponse = {
    creator: 'CREATOR',
    timestamp: 'TIMESTAMP',
    hash: 'HASH',
    hashFunction: 'HASH_FUNCTION',
    metadata: 'METADATA',
  };

  const historyResponse: QueryGetDocumentHashHistoryResponse = {
    history: [hashResponse, hashResponse],
  };

  const tokenRequest: SaveHashTokenDTO = {
    changeMessage: 'CHANGE_MESSAGE',
    document: 'DOCUMENT',
    hash: 'HASH',
    hashFunction: 'HASH_FUNCTION',
    metadata: 'METADATA',
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [HttpModule],
      controllers: [TokenManagerController],
      providers: [HashTokenService, TendermintService],
    }).compile();

    controller = module.get<TokenManagerController>(TokenManagerController);
    service = module.get<HashTokenService>(HashTokenService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
    expect(service).toBeDefined();
  });

  describe('findHash', () => {
    it('should find a document', async () => {
      spyOn(service, 'findById').and.returnValue(hashResponse);
      expect(await controller.findHash('document')).toEqual(hashResponse);
    });

    it('should throw an exception', async () => {
      spyOn(service, 'findById').and.throwError('error');
      expect(controller.findHash('document')).rejects.toThrow();
    });
  });

  describe('findHashHistory', () => {
    it('should find a documents history', async () => {
      spyOn(service, 'findHistory').and.returnValue(historyResponse);
      expect(await controller.findHashHistory('document')).toEqual(historyResponse);
    });

    it('should throw an exception', async () => {
      spyOn(service, 'findHistory').and.throwError('error');
      expect(controller.findHashHistory('document')).rejects.toThrow();
    });
  });

  describe('createHash', () => {
    it('should store a new hash', async () => {
      spyOn(service, 'createOne').and.returnValue(hashResponse);
      expect(await controller.createHash(tokenRequest)).toEqual(hashResponse);
    });

    it('should throw an exception', async () => {
      spyOn(service, 'createOne').and.throwError('error');
      expect(controller.createHash(tokenRequest)).rejects.toThrow();
    });
  });
});
