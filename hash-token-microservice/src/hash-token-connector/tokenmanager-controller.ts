/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Body, Controller, Get, HttpException, HttpStatus, Logger, Param, Post } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import {
  QueryGetDocumentHashHistoryResponse,
  QueryGetDocumentHashResponse,
} from 'src/tendermint/controller/dto/queryHashToken';
import { Config } from '../config';
import { SaveHashTokenDTO } from './hashToken/dto/save-HashToken.dto';
import { HashTokenService } from './hashToken/service/hashToken.service';

@ApiTags(Config.HASH_CONTROLLER_API_TAG)
@Controller(Config.HASH_CONTROLLER_ENDPOINT)
export class TokenManagerController {
  constructor(private readonly hashTokenService: HashTokenService) {}

  @Get(Config.HASH_CONTROLLER_GET_HASH_ENDPOINT)
  @ApiOperation({
    summary: Config.HASH_CONTROLLER_GET_HASH_SUMMARY,
    description: Config.HASH_CONTROLLER_GET_HASH_DESCRIPTION,
  })
  public async findHash(@Param('id') id: string): Promise<QueryGetDocumentHashResponse> {
    try {
      return await this.hashTokenService.findById(id);
    } catch (e) {
      Logger.error(e);
      throw new HttpException(e, HttpStatus.NOT_FOUND);
    }
  }

  @Get(Config.HASH_CONTROLLER_GET_HASH_HISTORY_ENDPOINT)
  @ApiOperation({
    summary: Config.HASH_CONTROLLER_GET_HASH_HISTORY_SUMMARY,
    description: Config.HASH_CONTROLLER_GET_HASH_HISTORY_DESCRIPTION,
  })
  public async findHashHistory(@Param('id') id: string): Promise<QueryGetDocumentHashHistoryResponse> {
    try {
      return await this.hashTokenService.findHistory(id);
    } catch (e) {
      Logger.error(e);
      throw new HttpException(e, HttpStatus.NOT_FOUND);
    }
  }

  @Post(Config.HASH_CONTROLLER_CREATE_HASH_ENDPOINT)
  @ApiOperation({
    summary: Config.HASH_CONTROLLER_CREATE_HASH_SUMMARY,
    description: Config.HASH_CONTROLLER_CREATE_HASH_DESCRIPTION,
  })
  public async createHash(@Body() dto: SaveHashTokenDTO): Promise<QueryGetDocumentHashResponse> {
    try {
      return await this.hashTokenService.createOne(dto);
    } catch (e) {
      Logger.error(e);
      throw new HttpException(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
