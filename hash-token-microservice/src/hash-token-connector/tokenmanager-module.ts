/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpModule, Module } from '@nestjs/common';
import { HashTokenService } from './hashToken/service/hashToken.service';
import { TendermintService } from '../tendermint/service/tendermint.service';
import { TokenManagerController } from './tokenmanager-controller';

@Module({
  imports: [HttpModule],
  providers: [HashTokenService, TendermintService],
  controllers: [TokenManagerController],
})
export class TokenmanagerModule {}
