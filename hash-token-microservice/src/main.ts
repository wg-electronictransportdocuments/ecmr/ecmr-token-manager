/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { INestApplication } from '@nestjs/common';
import { DocumentBuilder, OpenAPIObject, SwaggerModule } from '@nestjs/swagger';
import { Config } from './config';

async function bootstrap() {
  const app: INestApplication = await NestFactory.create(AppModule);
  app.setGlobalPrefix(Config.GLOBAL_PREFIX);

  const config: Pick<
    OpenAPIObject,
    'openapi' | 'info' | 'servers' | 'security' | 'tags' | 'externalDocs'
  > = new DocumentBuilder()
    .setTitle(Config.APPLICATION_TITLE)
    .setDescription(Config.APPLICATION_DESCRIPTION)
    .setVersion(Config.APPLICATION_VERSION)
    .build();
  const document: OpenAPIObject = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup(Config.SWAGGER_PATH, app, document);

  app.enableCors();
  await app.startAllMicroservicesAsync();
  await app.listen(3000, () => console.log('Microservice is listening'));
}
bootstrap();
