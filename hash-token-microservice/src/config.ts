/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

export enum Config {
  // Application configuration
  APPLICATION_TITLE = 'digitalconsignmentnote.hash-token-microservice',
  APPLICATION_DESCRIPTION = 'digitalconsignmentnote',
  APPLICATION_VERSION = '1.0',
  SWAGGER_PATH = 'api',

  GLOBAL_PREFIX = 'hash',

  // HashController
  HASH_CONTROLLER_API_TAG = 'HashTokenController',
  HASH_CONTROLLER_ENDPOINT = 'v1',

  HASH_CONTROLLER_GET_HASH_ENDPOINT = ':id',
  HASH_CONTROLLER_GET_HASH_SUMMARY = 'Gets the hash of an existing document identified by id',
  HASH_CONTROLLER_GET_HASH_DESCRIPTION = 'Gets the hash of an existing document identified by id',

  HASH_CONTROLLER_GET_HASH_HISTORY_ENDPOINT = ':id/history',
  HASH_CONTROLLER_GET_HASH_HISTORY_SUMMARY = 'Gets the history of hashes of an existing document identified by id',
  HASH_CONTROLLER_GET_HASH_HISTORY_DESCRIPTION = 'Gets the history of hashes of an existing document identified by id',

  HASH_CONTROLLER_CREATE_HASH_ENDPOINT = '',
  HASH_CONTROLLER_CREATE_HASH_SUMMARY = 'Sets the hash of a document',
  HASH_CONTROLLER_CREATE_HASH_DESCRIPTION = 'Sets the hash of a document',

  SERVER_PORT = '8085',

  TENDERMINT_CHAIN_ID = '/silicon-economy/base/blockchainbroker/token-manager/TokenManager/BusinessLogic/',
  TENDERMINT_TYPE_URL = '/silicon_economy.base.blockchainbroker.token_manager.businesslogic',
  ADDRESS_PREFIX = 'cosmos',

  TENDERMINT_MAX_RETRIES = '50',
  TENDERMINT_MAX_GAS = '1800000000',
}
