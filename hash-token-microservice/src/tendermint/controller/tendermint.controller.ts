/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Controller, Logger } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';
import { TendermintService } from '../service/tendermint.service';
import { QueryDto } from './dto/query.dto';

@Controller('tendermint')
export class TendermintController {
  logger = new Logger('Main');

  constructor(private readonly appService: TendermintService) {}

  @MessagePattern('query')
  async query(query: QueryDto) {
    return this.appService.query(query.nodeUri, query.chainId, query.path);
  }
}
