/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/* istanbul ignore file */
import { DirectSecp256k1HdWallet, EncodeObject, Registry } from '@cosmjs/proto-signing';
import { DecodeObject } from '@cosmjs/proto-signing/build/registry';
import { BroadcastTxResponse, isBroadcastTxSuccess, SigningStargateClient } from '@cosmjs/stargate';
import { Coin } from '@cosmjs/stargate/build/codec/cosmos/base/v1beta1/coin';
import { HttpService, Injectable, Logger } from '@nestjs/common';
import { QueryGetDocumentHashHistoryResponse, QueryGetDocumentHashResponse } from '../../generated/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic/module/types/businesslogic/query';
import { MsgCreateHashToken, MsgFetchDocumentHash, MsgFetchDocumentHashHistory, MsgIdResponse } from '../../generated/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic/module/types/businesslogic/tx';

@Injectable()
export class TendermintService {
  logger = new Logger('Main');
  tries = 0;
  maxTries: number = +process.env.TENDERMINT_MAX_RETRIES || 50;

  /** A standard coin object - Can be set to 0 as we do not use any monetary token systems.  */
  stdCoins: Coin[] = [{ denom: 'token', amount: '0' }];

  /** How much gas a transaction may use - Set to big enough value to accept all transactions.  */
  stdGas: string = process.env.TENDERMINT_MAX_GAS;

  /** Memo that is attached to a transaction  */
  stdMemo: 'Sent From Border Backend';

  /** Registry to decode Blockchain responses. */
  registry: any;

  /** PART OF THE HOTFIX FOR VERSION 0.24 */
  private maxTimeout = 3;
  private timeoutMs = 1000;

  /** Creates a new Tendermint Service instance. Injects a Http Service. */
  constructor(private readonly httpService: HttpService) {
    const types = [
      ['/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgCreateHashToken', MsgCreateHashToken],
      ['/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgIdResponse', MsgIdResponse],
      ['/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgFetchDocumentHash', MsgFetchDocumentHash],
      ['/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.QueryGetDocumentHashResponse', QueryGetDocumentHashResponse],
      ['/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgFetchDocumentHashHistory', MsgFetchDocumentHashHistory],
      ['/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.QueryGetDocumentHashHistoryResponse', QueryGetDocumentHashHistoryResponse],
    ];

    this.registry = new Registry(<any>types);
  }

  /**
   * Generic Query generator function.
   * Queries the chain endpoints specified by the function parameters.
   * @param nodeUri The address of the Blockchain to dial into.
   * @param chainId The id of the Tendermint instance running.
   * @param path The path of the resource to query for.
   * @deprecated
   * @returns
   */
   async query(nodeUri: string, chainId: string, path: string) {
    Logger.log(`${nodeUri}/${chainId}/${path}`);
    return (await this.httpService.get(`${nodeUri}/${chainId}/${path}`).toPromise()).data;
  }

  /**
     * Prepare an incoming transaction into a Cosmos/Tendermint message format.
     * @param mnemonic The private Key used to sign with.
     * @param txInput The transaction input to sign.
     * @param type The type of a transaction Msg. For example: "MsgCreateExportToken"
     * @param typeUrl The URL under which the API is served. For example: "/org.borderblockchain.importtoken"
     * @returns
     */
  async buildSignAndBroadcast(
    mnemonic: string,
    txInput: any[],
    type: string,
    typeUrl: string
  ): Promise<BroadcastTxResponse> {
    const messages: EncodeObject[] = [];
    for (const txs of txInput) {
      txs.creator = await this.getCosmosAddress(mnemonic);
      messages.push({
        typeUrl: `${typeUrl}.${type}`,
        value: txs,
      });
    }
    return this.signAndBroadcast(mnemonic, messages);
  }

  private async getCosmosAddress(mnemonic: string): Promise<string> {
    const [creatorAddress] = await (
      await DirectSecp256k1HdWallet.fromMnemonic(mnemonic, undefined, process.env.WALLET_ADDRESS_PREFIX)
    ).getAccounts();

    return creatorAddress.address;
  }  
  
  /**
    * This is the last step in the writing process onto the tendermint/cosmos instance.
    * It signs a pre-packaged Array of Encode Objects and broadcasts it to chain peers.
    * Use buildSignAndBroadcast if you need to build your Encode objects first.
    * @param wallet The Wallet address used for signing.
    * @param mnemonic The private Key used to sign with.
    * @param messages The array of messages of Encode Objects.
    * @returns
    */
  async signAndBroadcast(mnemonic: string, messages: readonly EncodeObject[]): Promise<BroadcastTxResponse> {
    const blockchainRPCEndpoint = `${process.env.API_BLOCKCHAIN_RPC}`;
    console.log(blockchainRPCEndpoint);
    
    const signer = await DirectSecp256k1HdWallet.fromMnemonic(mnemonic);
    const wallet = await this.getCosmosAddress(mnemonic);
    const stargateClient = SigningStargateClient;
    const client = await stargateClient.connectWithSigner(blockchainRPCEndpoint, signer, { registry: this.registry });

    console.log(messages);

    for (let i = 0; i < this.maxTimeout; i++) {
      const response: BroadcastTxResponse = await client.signAndBroadcast(
        wallet,
        messages,
        { amount: this.stdCoins, gas: this.stdGas },
        this.stdMemo
      );

      if (isBroadcastTxSuccess(response)) {
        return response;
      }
      await this.delay(this.timeoutMs);
    }
  }

  private delay(ms: number) {
    return new Promise((resolve) => setTimeout(resolve, ms));
  }

  /**
   * Decodes a result. The response is specified in the tendermint/cosmos go module.
   * @param result The response given by the chain.
   * @param responseType The type of response expected. Needs to be registered in the registry of this class.
   * @returns
   */
  async decodeResult(result: BroadcastTxResponse, responseType: string): Promise<any> {
    try {
      const decodedResult: DecodeObject = this.registry.decode({
        typeUrl: responseType,
        value: result.data[0].data,
      });

      return decodedResult;
    } catch {
      console.log('Transaction OK, But nothing to decode.');
      return null;
    }
  }
}
