/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpModule, Module } from '@nestjs/common';
import { TendermintController } from './controller/tendermint.controller';
import { TendermintService } from './service/tendermint.service';

@Module({
  imports: [HttpModule],
  providers: [TendermintService],
  controllers: [TendermintController],
})
export class TendermintModule {}
