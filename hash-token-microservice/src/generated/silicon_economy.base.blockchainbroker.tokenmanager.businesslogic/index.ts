import { txClient, queryClient, MissingWalletError , registry} from './module'

import { DocumentTokenMapper } from "./module/types/businesslogic/document_token_mapper"
import { QueryGetTokenCopiesRequest } from "./module/types/businesslogic/query"
import { QueryGetTokenCopiesResponse } from "./module/types/businesslogic/query"
import { QueryAllTokenCopiesRequest } from "./module/types/businesslogic/query"
import { QueryAllTokenCopiesResponse } from "./module/types/businesslogic/query"
import { QueryGetDocumentTokenMapperRequest } from "./module/types/businesslogic/query"
import { QueryGetDocumentTokenMapperResponse } from "./module/types/businesslogic/query"
import { QueryAllDocumentTokenMapperRequest } from "./module/types/businesslogic/query"
import { QueryAllDocumentTokenMapperResponse } from "./module/types/businesslogic/query"
import { TokenCopies } from "./module/types/businesslogic/token_copies"
import { MsgCreateTokenCopiesResponse } from "./module/types/businesslogic/tx"
import { MsgUpdateTokenCopiesResponse } from "./module/types/businesslogic/tx"
import { MsgDeleteTokenCopiesResponse } from "./module/types/businesslogic/tx"
import { MsgDeleteDocumentTokenMapperResponse } from "./module/types/businesslogic/tx"


export { DocumentTokenMapper, QueryGetTokenCopiesRequest, QueryGetTokenCopiesResponse, QueryAllTokenCopiesRequest, QueryAllTokenCopiesResponse, QueryGetDocumentTokenMapperRequest, QueryGetDocumentTokenMapperResponse, QueryAllDocumentTokenMapperRequest, QueryAllDocumentTokenMapperResponse, TokenCopies, MsgCreateTokenCopiesResponse, MsgUpdateTokenCopiesResponse, MsgDeleteTokenCopiesResponse, MsgDeleteDocumentTokenMapperResponse };

async function initTxClient(vuexGetters) {
	return await txClient(vuexGetters['common/wallet/signer'], {
		addr: vuexGetters['common/env/apiTendermint']
	})
}

async function initQueryClient(vuexGetters) {
	return await queryClient({
		addr: vuexGetters['common/env/apiCosmos']
	})
}

function mergeResults(value, next_values) {
	for (let prop of Object.keys(next_values)) {
		if (Array.isArray(next_values[prop])) {
			value[prop]=[...value[prop], ...next_values[prop]]
		}else{
			value[prop]=next_values[prop]
		}
	}
	return value
}

function getStructure(template) {
	let structure = { fields: [] }
	for (const [key, value] of Object.entries(template)) {
		let field: any = {}
		field.name = key
		field.type = typeof value
		structure.fields.push(field)
	}
	return structure
}

const getDefaultState = () => {
	return {
				
				_Structure: {
						DocumentTokenMapper: getStructure(DocumentTokenMapper.fromPartial({})),
						QueryGetTokenCopiesRequest: getStructure(QueryGetTokenCopiesRequest.fromPartial({})),
						QueryGetTokenCopiesResponse: getStructure(QueryGetTokenCopiesResponse.fromPartial({})),
						QueryAllTokenCopiesRequest: getStructure(QueryAllTokenCopiesRequest.fromPartial({})),
						QueryAllTokenCopiesResponse: getStructure(QueryAllTokenCopiesResponse.fromPartial({})),
						QueryGetDocumentTokenMapperRequest: getStructure(QueryGetDocumentTokenMapperRequest.fromPartial({})),
						QueryGetDocumentTokenMapperResponse: getStructure(QueryGetDocumentTokenMapperResponse.fromPartial({})),
						QueryAllDocumentTokenMapperRequest: getStructure(QueryAllDocumentTokenMapperRequest.fromPartial({})),
						QueryAllDocumentTokenMapperResponse: getStructure(QueryAllDocumentTokenMapperResponse.fromPartial({})),
						TokenCopies: getStructure(TokenCopies.fromPartial({})),
						MsgCreateTokenCopiesResponse: getStructure(MsgCreateTokenCopiesResponse.fromPartial({})),
						MsgUpdateTokenCopiesResponse: getStructure(MsgUpdateTokenCopiesResponse.fromPartial({})),
						MsgDeleteTokenCopiesResponse: getStructure(MsgDeleteTokenCopiesResponse.fromPartial({})),
						MsgDeleteDocumentTokenMapperResponse: getStructure(MsgDeleteDocumentTokenMapperResponse.fromPartial({})),
						
		},
		_Registry: registry,
		_Subscriptions: new Set(),
	}
}

// initial state
const state = getDefaultState()

export default {
	namespaced: true,
	state,
	mutations: {
		RESET_STATE(state) {
			Object.assign(state, getDefaultState())
		},
		QUERY(state, { query, key, value }) {
			state[query][JSON.stringify(key)] = value
		},
		SUBSCRIBE(state, subscription) {
			state._Subscriptions.add(JSON.stringify(subscription))
		},
		UNSUBSCRIBE(state, subscription) {
			state._Subscriptions.delete(JSON.stringify(subscription))
		}
	},
	getters: {
				
		getTypeStructure: (state) => (type) => {
			return state._Structure[type].fields
		},
		getRegistry: (state) => {
			return state._Registry
		}
	},
	actions: {
		init({ dispatch, rootGetters }) {
			console.log('Vuex module: silicon_economy.base.blockchainbroker.tokenmanager.businesslogic initialized!')
			if (rootGetters['common/env/client']) {
				rootGetters['common/env/client'].on('newblock', () => {
					dispatch('StoreUpdate')
				})
			}
		},
		resetState({ commit }) {
			commit('RESET_STATE')
		},
		unsubscribe({ commit }, subscription) {
			commit('UNSUBSCRIBE', subscription)
		},
		async StoreUpdate({ state, dispatch }) {
			state._Subscriptions.forEach(async (subscription) => {
				try {
					const sub=JSON.parse(subscription)
					await dispatch(sub.action, sub.payload)
				}catch(e) {
					throw new Error('Subscriptions: ' + e.message)
				}
			})
		},
		
		async sendMsgFetchGetSegment({ rootGetters }, { value, fee = [], memo = '' }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgFetchGetSegment(value)
				const result = await txClient.signAndBroadcast([msg], {fee: { amount: fee, 
	gas: "200000" }, memo})
				return result
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgFetchGetSegment:Init Could not initialize signing client. Wallet is required.')
				}else{
					throw new Error('TxClient:MsgFetchGetSegment:Send Could not broadcast Tx: '+ e.message)
				}
			}
		},
		async sendMsgUpdateTokenCopies({ rootGetters }, { value, fee = [], memo = '' }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgUpdateTokenCopies(value)
				const result = await txClient.signAndBroadcast([msg], {fee: { amount: fee, 
	gas: "200000" }, memo})
				return result
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgUpdateTokenCopies:Init Could not initialize signing client. Wallet is required.')
				}else{
					throw new Error('TxClient:MsgUpdateTokenCopies:Send Could not broadcast Tx: '+ e.message)
				}
			}
		},
		async sendMsgCreateSegmentWithId({ rootGetters }, { value, fee = [], memo = '' }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgCreateSegmentWithId(value)
				const result = await txClient.signAndBroadcast([msg], {fee: { amount: fee, 
	gas: "200000" }, memo})
				return result
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgCreateSegmentWithId:Init Could not initialize signing client. Wallet is required.')
				}else{
					throw new Error('TxClient:MsgCreateSegmentWithId:Send Could not broadcast Tx: '+ e.message)
				}
			}
		},
		async sendMsgCreateWalletWithId({ rootGetters }, { value, fee = [], memo = '' }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgCreateWalletWithId(value)
				const result = await txClient.signAndBroadcast([msg], {fee: { amount: fee, 
	gas: "200000" }, memo})
				return result
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgCreateWalletWithId:Init Could not initialize signing client. Wallet is required.')
				}else{
					throw new Error('TxClient:MsgCreateWalletWithId:Send Could not broadcast Tx: '+ e.message)
				}
			}
		},
		async sendMsgDeleteTokenCopies({ rootGetters }, { value, fee = [], memo = '' }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgDeleteTokenCopies(value)
				const result = await txClient.signAndBroadcast([msg], {fee: { amount: fee, 
	gas: "200000" }, memo})
				return result
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgDeleteTokenCopies:Init Could not initialize signing client. Wallet is required.')
				}else{
					throw new Error('TxClient:MsgDeleteTokenCopies:Send Could not broadcast Tx: '+ e.message)
				}
			}
		},
		async sendMsgRevertToGenesis({ rootGetters }, { value, fee = [], memo = '' }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgRevertToGenesis(value)
				const result = await txClient.signAndBroadcast([msg], {fee: { amount: fee, 
	gas: "200000" }, memo})
				return result
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgRevertToGenesis:Init Could not initialize signing client. Wallet is required.')
				}else{
					throw new Error('TxClient:MsgRevertToGenesis:Send Could not broadcast Tx: '+ e.message)
				}
			}
		},
		async sendMsgFetchGetWallet({ rootGetters }, { value, fee = [], memo = '' }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgFetchGetWallet(value)
				const result = await txClient.signAndBroadcast([msg], {fee: { amount: fee, 
	gas: "200000" }, memo})
				return result
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgFetchGetWallet:Init Could not initialize signing client. Wallet is required.')
				}else{
					throw new Error('TxClient:MsgFetchGetWallet:Send Could not broadcast Tx: '+ e.message)
				}
			}
		},
		async sendMsgFetchAllSegmentHistory({ rootGetters }, { value, fee = [], memo = '' }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgFetchAllSegmentHistory(value)
				const result = await txClient.signAndBroadcast([msg], {fee: { amount: fee, 
	gas: "200000" }, memo})
				return result
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgFetchAllSegmentHistory:Init Could not initialize signing client. Wallet is required.')
				}else{
					throw new Error('TxClient:MsgFetchAllSegmentHistory:Send Could not broadcast Tx: '+ e.message)
				}
			}
		},
		async sendMsgFetchToken({ rootGetters }, { value, fee = [], memo = '' }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgFetchToken(value)
				const result = await txClient.signAndBroadcast([msg], {fee: { amount: fee, 
	gas: "200000" }, memo})
				return result
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgFetchToken:Init Could not initialize signing client. Wallet is required.')
				}else{
					throw new Error('TxClient:MsgFetchToken:Send Could not broadcast Tx: '+ e.message)
				}
			}
		},
		async sendMsgCreateSegment({ rootGetters }, { value, fee = [], memo = '' }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgCreateSegment(value)
				const result = await txClient.signAndBroadcast([msg], {fee: { amount: fee, 
	gas: "200000" }, memo})
				return result
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgCreateSegment:Init Could not initialize signing client. Wallet is required.')
				}else{
					throw new Error('TxClient:MsgCreateSegment:Send Could not broadcast Tx: '+ e.message)
				}
			}
		},
		async sendMsgCreateToken({ rootGetters }, { value, fee = [], memo = '' }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgCreateToken(value)
				const result = await txClient.signAndBroadcast([msg], {fee: { amount: fee, 
	gas: "200000" }, memo})
				return result
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgCreateToken:Init Could not initialize signing client. Wallet is required.')
				}else{
					throw new Error('TxClient:MsgCreateToken:Send Could not broadcast Tx: '+ e.message)
				}
			}
		},
		async sendMsgCreateHashToken({ rootGetters }, { value, fee = [], memo = '' }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgCreateHashToken(value)
				const result = await txClient.signAndBroadcast([msg], {fee: { amount: fee, 
	gas: "200000" }, memo})
				return result
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgCreateHashToken:Init Could not initialize signing client. Wallet is required.')
				}else{
					throw new Error('TxClient:MsgCreateHashToken:Send Could not broadcast Tx: '+ e.message)
				}
			}
		},
		async sendMsgMoveTokenToSegment({ rootGetters }, { value, fee = [], memo = '' }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgMoveTokenToSegment(value)
				const result = await txClient.signAndBroadcast([msg], {fee: { amount: fee, 
	gas: "200000" }, memo})
				return result
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgMoveTokenToSegment:Init Could not initialize signing client. Wallet is required.')
				}else{
					throw new Error('TxClient:MsgMoveTokenToSegment:Send Could not broadcast Tx: '+ e.message)
				}
			}
		},
		async sendMsgDeleteDocumentTokenMapper({ rootGetters }, { value, fee = [], memo = '' }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgDeleteDocumentTokenMapper(value)
				const result = await txClient.signAndBroadcast([msg], {fee: { amount: fee, 
	gas: "200000" }, memo})
				return result
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgDeleteDocumentTokenMapper:Init Could not initialize signing client. Wallet is required.')
				}else{
					throw new Error('TxClient:MsgDeleteDocumentTokenMapper:Send Could not broadcast Tx: '+ e.message)
				}
			}
		},
		async sendMsgUpdateWallet({ rootGetters }, { value, fee = [], memo = '' }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgUpdateWallet(value)
				const result = await txClient.signAndBroadcast([msg], {fee: { amount: fee, 
	gas: "200000" }, memo})
				return result
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgUpdateWallet:Init Could not initialize signing client. Wallet is required.')
				}else{
					throw new Error('TxClient:MsgUpdateWallet:Send Could not broadcast Tx: '+ e.message)
				}
			}
		},
		async sendMsgFetchGetWalletHistory({ rootGetters }, { value, fee = [], memo = '' }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgFetchGetWalletHistory(value)
				const result = await txClient.signAndBroadcast([msg], {fee: { amount: fee, 
	gas: "200000" }, memo})
				return result
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgFetchGetWalletHistory:Init Could not initialize signing client. Wallet is required.')
				}else{
					throw new Error('TxClient:MsgFetchGetWalletHistory:Send Could not broadcast Tx: '+ e.message)
				}
			}
		},
		async sendMsgActivateToken({ rootGetters }, { value, fee = [], memo = '' }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgActivateToken(value)
				const result = await txClient.signAndBroadcast([msg], {fee: { amount: fee, 
	gas: "200000" }, memo})
				return result
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgActivateToken:Init Could not initialize signing client. Wallet is required.')
				}else{
					throw new Error('TxClient:MsgActivateToken:Send Could not broadcast Tx: '+ e.message)
				}
			}
		},
		async sendMsgFetchAllWalletHistory({ rootGetters }, { value, fee = [], memo = '' }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgFetchAllWalletHistory(value)
				const result = await txClient.signAndBroadcast([msg], {fee: { amount: fee, 
	gas: "200000" }, memo})
				return result
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgFetchAllWalletHistory:Init Could not initialize signing client. Wallet is required.')
				}else{
					throw new Error('TxClient:MsgFetchAllWalletHistory:Send Could not broadcast Tx: '+ e.message)
				}
			}
		},
		async sendMsgCloneToken({ rootGetters }, { value, fee = [], memo = '' }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgCloneToken(value)
				const result = await txClient.signAndBroadcast([msg], {fee: { amount: fee, 
	gas: "200000" }, memo})
				return result
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgCloneToken:Init Could not initialize signing client. Wallet is required.')
				}else{
					throw new Error('TxClient:MsgCloneToken:Send Could not broadcast Tx: '+ e.message)
				}
			}
		},
		async sendMsgFetchSegmentHistory({ rootGetters }, { value, fee = [], memo = '' }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgFetchSegmentHistory(value)
				const result = await txClient.signAndBroadcast([msg], {fee: { amount: fee, 
	gas: "200000" }, memo})
				return result
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgFetchSegmentHistory:Init Could not initialize signing client. Wallet is required.')
				}else{
					throw new Error('TxClient:MsgFetchSegmentHistory:Send Could not broadcast Tx: '+ e.message)
				}
			}
		},
		async sendMsgFetchTokensByWalletId({ rootGetters }, { value, fee = [], memo = '' }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgFetchTokensByWalletId(value)
				const result = await txClient.signAndBroadcast([msg], {fee: { amount: fee, 
	gas: "200000" }, memo})
				return result
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgFetchTokensByWalletId:Init Could not initialize signing client. Wallet is required.')
				}else{
					throw new Error('TxClient:MsgFetchTokensByWalletId:Send Could not broadcast Tx: '+ e.message)
				}
			}
		},
		async sendMsgMoveTokenToWallet({ rootGetters }, { value, fee = [], memo = '' }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgMoveTokenToWallet(value)
				const result = await txClient.signAndBroadcast([msg], {fee: { amount: fee, 
	gas: "200000" }, memo})
				return result
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgMoveTokenToWallet:Init Could not initialize signing client. Wallet is required.')
				}else{
					throw new Error('TxClient:MsgMoveTokenToWallet:Send Could not broadcast Tx: '+ e.message)
				}
			}
		},
		async sendMsgFetchDocumentHashHistory({ rootGetters }, { value, fee = [], memo = '' }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgFetchDocumentHashHistory(value)
				const result = await txClient.signAndBroadcast([msg], {fee: { amount: fee, 
	gas: "200000" }, memo})
				return result
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgFetchDocumentHashHistory:Init Could not initialize signing client. Wallet is required.')
				}else{
					throw new Error('TxClient:MsgFetchDocumentHashHistory:Send Could not broadcast Tx: '+ e.message)
				}
			}
		},
		async sendMsgCreateTokenCopies({ rootGetters }, { value, fee = [], memo = '' }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgCreateTokenCopies(value)
				const result = await txClient.signAndBroadcast([msg], {fee: { amount: fee, 
	gas: "200000" }, memo})
				return result
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgCreateTokenCopies:Init Could not initialize signing client. Wallet is required.')
				}else{
					throw new Error('TxClient:MsgCreateTokenCopies:Send Could not broadcast Tx: '+ e.message)
				}
			}
		},
		async sendMsgUpdateSegment({ rootGetters }, { value, fee = [], memo = '' }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgUpdateSegment(value)
				const result = await txClient.signAndBroadcast([msg], {fee: { amount: fee, 
	gas: "200000" }, memo})
				return result
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgUpdateSegment:Init Could not initialize signing client. Wallet is required.')
				}else{
					throw new Error('TxClient:MsgUpdateSegment:Send Could not broadcast Tx: '+ e.message)
				}
			}
		},
		async sendMsgFetchTokensBySegmentId({ rootGetters }, { value, fee = [], memo = '' }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgFetchTokensBySegmentId(value)
				const result = await txClient.signAndBroadcast([msg], {fee: { amount: fee, 
	gas: "200000" }, memo})
				return result
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgFetchTokensBySegmentId:Init Could not initialize signing client. Wallet is required.')
				}else{
					throw new Error('TxClient:MsgFetchTokensBySegmentId:Send Could not broadcast Tx: '+ e.message)
				}
			}
		},
		async sendMsgCreateWallet({ rootGetters }, { value, fee = [], memo = '' }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgCreateWallet(value)
				const result = await txClient.signAndBroadcast([msg], {fee: { amount: fee, 
	gas: "200000" }, memo})
				return result
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgCreateWallet:Init Could not initialize signing client. Wallet is required.')
				}else{
					throw new Error('TxClient:MsgCreateWallet:Send Could not broadcast Tx: '+ e.message)
				}
			}
		},
		async sendMsgCreateDocumentTokenMapper({ rootGetters }, { value, fee = [], memo = '' }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgCreateDocumentTokenMapper(value)
				const result = await txClient.signAndBroadcast([msg], {fee: { amount: fee, 
	gas: "200000" }, memo})
				return result
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgCreateDocumentTokenMapper:Init Could not initialize signing client. Wallet is required.')
				}else{
					throw new Error('TxClient:MsgCreateDocumentTokenMapper:Send Could not broadcast Tx: '+ e.message)
				}
			}
		},
		async sendMsgUpdateDocumentTokenMapper({ rootGetters }, { value, fee = [], memo = '' }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgUpdateDocumentTokenMapper(value)
				const result = await txClient.signAndBroadcast([msg], {fee: { amount: fee, 
	gas: "200000" }, memo})
				return result
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgUpdateDocumentTokenMapper:Init Could not initialize signing client. Wallet is required.')
				}else{
					throw new Error('TxClient:MsgUpdateDocumentTokenMapper:Send Could not broadcast Tx: '+ e.message)
				}
			}
		},
		async sendMsgFetchAllSegment({ rootGetters }, { value, fee = [], memo = '' }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgFetchAllSegment(value)
				const result = await txClient.signAndBroadcast([msg], {fee: { amount: fee, 
	gas: "200000" }, memo})
				return result
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgFetchAllSegment:Init Could not initialize signing client. Wallet is required.')
				}else{
					throw new Error('TxClient:MsgFetchAllSegment:Send Could not broadcast Tx: '+ e.message)
				}
			}
		},
		async sendMsgUpdateTokenInformation({ rootGetters }, { value, fee = [], memo = '' }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgUpdateTokenInformation(value)
				const result = await txClient.signAndBroadcast([msg], {fee: { amount: fee, 
	gas: "200000" }, memo})
				return result
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgUpdateTokenInformation:Init Could not initialize signing client. Wallet is required.')
				}else{
					throw new Error('TxClient:MsgUpdateTokenInformation:Send Could not broadcast Tx: '+ e.message)
				}
			}
		},
		async sendMsgDeactivateToken({ rootGetters }, { value, fee = [], memo = '' }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgDeactivateToken(value)
				const result = await txClient.signAndBroadcast([msg], {fee: { amount: fee, 
	gas: "200000" }, memo})
				return result
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgDeactivateToken:Init Could not initialize signing client. Wallet is required.')
				}else{
					throw new Error('TxClient:MsgDeactivateToken:Send Could not broadcast Tx: '+ e.message)
				}
			}
		},
		async sendMsgFetchTokenHistory({ rootGetters }, { value, fee = [], memo = '' }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgFetchTokenHistory(value)
				const result = await txClient.signAndBroadcast([msg], {fee: { amount: fee, 
	gas: "200000" }, memo})
				return result
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgFetchTokenHistory:Init Could not initialize signing client. Wallet is required.')
				}else{
					throw new Error('TxClient:MsgFetchTokenHistory:Send Could not broadcast Tx: '+ e.message)
				}
			}
		},
		async sendMsgFetchAllTokenHistoryGlobal({ rootGetters }, { value, fee = [], memo = '' }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgFetchAllTokenHistoryGlobal(value)
				const result = await txClient.signAndBroadcast([msg], {fee: { amount: fee, 
	gas: "200000" }, memo})
				return result
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgFetchAllTokenHistoryGlobal:Init Could not initialize signing client. Wallet is required.')
				}else{
					throw new Error('TxClient:MsgFetchAllTokenHistoryGlobal:Send Could not broadcast Tx: '+ e.message)
				}
			}
		},
		async sendMsgFetchGetTokenHistoryGlobal({ rootGetters }, { value, fee = [], memo = '' }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgFetchGetTokenHistoryGlobal(value)
				const result = await txClient.signAndBroadcast([msg], {fee: { amount: fee, 
	gas: "200000" }, memo})
				return result
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgFetchGetTokenHistoryGlobal:Init Could not initialize signing client. Wallet is required.')
				}else{
					throw new Error('TxClient:MsgFetchGetTokenHistoryGlobal:Send Could not broadcast Tx: '+ e.message)
				}
			}
		},
		async sendMsgUpdateToken({ rootGetters }, { value, fee = [], memo = '' }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgUpdateToken(value)
				const result = await txClient.signAndBroadcast([msg], {fee: { amount: fee, 
	gas: "200000" }, memo})
				return result
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgUpdateToken:Init Could not initialize signing client. Wallet is required.')
				}else{
					throw new Error('TxClient:MsgUpdateToken:Send Could not broadcast Tx: '+ e.message)
				}
			}
		},
		async sendMsgFetchAllWallet({ rootGetters }, { value, fee = [], memo = '' }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgFetchAllWallet(value)
				const result = await txClient.signAndBroadcast([msg], {fee: { amount: fee, 
	gas: "200000" }, memo})
				return result
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgFetchAllWallet:Init Could not initialize signing client. Wallet is required.')
				}else{
					throw new Error('TxClient:MsgFetchAllWallet:Send Could not broadcast Tx: '+ e.message)
				}
			}
		},
		async sendMsgFetchDocumentHash({ rootGetters }, { value, fee = [], memo = '' }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgFetchDocumentHash(value)
				const result = await txClient.signAndBroadcast([msg], {fee: { amount: fee, 
	gas: "200000" }, memo})
				return result
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgFetchDocumentHash:Init Could not initialize signing client. Wallet is required.')
				}else{
					throw new Error('TxClient:MsgFetchDocumentHash:Send Could not broadcast Tx: '+ e.message)
				}
			}
		},
		
		async MsgFetchGetSegment({ rootGetters }, { value }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgFetchGetSegment(value)
				return msg
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgFetchGetSegment:Init Could not initialize signing client. Wallet is required.')
				} else{
					throw new Error('TxClient:MsgFetchGetSegment:Create Could not create message: ' + e.message)
				}
			}
		},
		async MsgUpdateTokenCopies({ rootGetters }, { value }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgUpdateTokenCopies(value)
				return msg
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgUpdateTokenCopies:Init Could not initialize signing client. Wallet is required.')
				} else{
					throw new Error('TxClient:MsgUpdateTokenCopies:Create Could not create message: ' + e.message)
				}
			}
		},
		async MsgCreateSegmentWithId({ rootGetters }, { value }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgCreateSegmentWithId(value)
				return msg
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgCreateSegmentWithId:Init Could not initialize signing client. Wallet is required.')
				} else{
					throw new Error('TxClient:MsgCreateSegmentWithId:Create Could not create message: ' + e.message)
				}
			}
		},
		async MsgCreateWalletWithId({ rootGetters }, { value }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgCreateWalletWithId(value)
				return msg
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgCreateWalletWithId:Init Could not initialize signing client. Wallet is required.')
				} else{
					throw new Error('TxClient:MsgCreateWalletWithId:Create Could not create message: ' + e.message)
				}
			}
		},
		async MsgDeleteTokenCopies({ rootGetters }, { value }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgDeleteTokenCopies(value)
				return msg
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgDeleteTokenCopies:Init Could not initialize signing client. Wallet is required.')
				} else{
					throw new Error('TxClient:MsgDeleteTokenCopies:Create Could not create message: ' + e.message)
				}
			}
		},
		async MsgRevertToGenesis({ rootGetters }, { value }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgRevertToGenesis(value)
				return msg
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgRevertToGenesis:Init Could not initialize signing client. Wallet is required.')
				} else{
					throw new Error('TxClient:MsgRevertToGenesis:Create Could not create message: ' + e.message)
				}
			}
		},
		async MsgFetchGetWallet({ rootGetters }, { value }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgFetchGetWallet(value)
				return msg
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgFetchGetWallet:Init Could not initialize signing client. Wallet is required.')
				} else{
					throw new Error('TxClient:MsgFetchGetWallet:Create Could not create message: ' + e.message)
				}
			}
		},
		async MsgFetchAllSegmentHistory({ rootGetters }, { value }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgFetchAllSegmentHistory(value)
				return msg
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgFetchAllSegmentHistory:Init Could not initialize signing client. Wallet is required.')
				} else{
					throw new Error('TxClient:MsgFetchAllSegmentHistory:Create Could not create message: ' + e.message)
				}
			}
		},
		async MsgFetchToken({ rootGetters }, { value }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgFetchToken(value)
				return msg
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgFetchToken:Init Could not initialize signing client. Wallet is required.')
				} else{
					throw new Error('TxClient:MsgFetchToken:Create Could not create message: ' + e.message)
				}
			}
		},
		async MsgCreateSegment({ rootGetters }, { value }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgCreateSegment(value)
				return msg
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgCreateSegment:Init Could not initialize signing client. Wallet is required.')
				} else{
					throw new Error('TxClient:MsgCreateSegment:Create Could not create message: ' + e.message)
				}
			}
		},
		async MsgCreateToken({ rootGetters }, { value }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgCreateToken(value)
				return msg
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgCreateToken:Init Could not initialize signing client. Wallet is required.')
				} else{
					throw new Error('TxClient:MsgCreateToken:Create Could not create message: ' + e.message)
				}
			}
		},
		async MsgCreateHashToken({ rootGetters }, { value }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgCreateHashToken(value)
				return msg
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgCreateHashToken:Init Could not initialize signing client. Wallet is required.')
				} else{
					throw new Error('TxClient:MsgCreateHashToken:Create Could not create message: ' + e.message)
				}
			}
		},
		async MsgMoveTokenToSegment({ rootGetters }, { value }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgMoveTokenToSegment(value)
				return msg
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgMoveTokenToSegment:Init Could not initialize signing client. Wallet is required.')
				} else{
					throw new Error('TxClient:MsgMoveTokenToSegment:Create Could not create message: ' + e.message)
				}
			}
		},
		async MsgDeleteDocumentTokenMapper({ rootGetters }, { value }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgDeleteDocumentTokenMapper(value)
				return msg
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgDeleteDocumentTokenMapper:Init Could not initialize signing client. Wallet is required.')
				} else{
					throw new Error('TxClient:MsgDeleteDocumentTokenMapper:Create Could not create message: ' + e.message)
				}
			}
		},
		async MsgUpdateWallet({ rootGetters }, { value }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgUpdateWallet(value)
				return msg
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgUpdateWallet:Init Could not initialize signing client. Wallet is required.')
				} else{
					throw new Error('TxClient:MsgUpdateWallet:Create Could not create message: ' + e.message)
				}
			}
		},
		async MsgFetchGetWalletHistory({ rootGetters }, { value }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgFetchGetWalletHistory(value)
				return msg
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgFetchGetWalletHistory:Init Could not initialize signing client. Wallet is required.')
				} else{
					throw new Error('TxClient:MsgFetchGetWalletHistory:Create Could not create message: ' + e.message)
				}
			}
		},
		async MsgActivateToken({ rootGetters }, { value }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgActivateToken(value)
				return msg
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgActivateToken:Init Could not initialize signing client. Wallet is required.')
				} else{
					throw new Error('TxClient:MsgActivateToken:Create Could not create message: ' + e.message)
				}
			}
		},
		async MsgFetchAllWalletHistory({ rootGetters }, { value }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgFetchAllWalletHistory(value)
				return msg
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgFetchAllWalletHistory:Init Could not initialize signing client. Wallet is required.')
				} else{
					throw new Error('TxClient:MsgFetchAllWalletHistory:Create Could not create message: ' + e.message)
				}
			}
		},
		async MsgCloneToken({ rootGetters }, { value }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgCloneToken(value)
				return msg
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgCloneToken:Init Could not initialize signing client. Wallet is required.')
				} else{
					throw new Error('TxClient:MsgCloneToken:Create Could not create message: ' + e.message)
				}
			}
		},
		async MsgFetchSegmentHistory({ rootGetters }, { value }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgFetchSegmentHistory(value)
				return msg
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgFetchSegmentHistory:Init Could not initialize signing client. Wallet is required.')
				} else{
					throw new Error('TxClient:MsgFetchSegmentHistory:Create Could not create message: ' + e.message)
				}
			}
		},
		async MsgFetchTokensByWalletId({ rootGetters }, { value }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgFetchTokensByWalletId(value)
				return msg
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgFetchTokensByWalletId:Init Could not initialize signing client. Wallet is required.')
				} else{
					throw new Error('TxClient:MsgFetchTokensByWalletId:Create Could not create message: ' + e.message)
				}
			}
		},
		async MsgMoveTokenToWallet({ rootGetters }, { value }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgMoveTokenToWallet(value)
				return msg
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgMoveTokenToWallet:Init Could not initialize signing client. Wallet is required.')
				} else{
					throw new Error('TxClient:MsgMoveTokenToWallet:Create Could not create message: ' + e.message)
				}
			}
		},
		async MsgFetchDocumentHashHistory({ rootGetters }, { value }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgFetchDocumentHashHistory(value)
				return msg
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgFetchDocumentHashHistory:Init Could not initialize signing client. Wallet is required.')
				} else{
					throw new Error('TxClient:MsgFetchDocumentHashHistory:Create Could not create message: ' + e.message)
				}
			}
		},
		async MsgCreateTokenCopies({ rootGetters }, { value }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgCreateTokenCopies(value)
				return msg
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgCreateTokenCopies:Init Could not initialize signing client. Wallet is required.')
				} else{
					throw new Error('TxClient:MsgCreateTokenCopies:Create Could not create message: ' + e.message)
				}
			}
		},
		async MsgUpdateSegment({ rootGetters }, { value }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgUpdateSegment(value)
				return msg
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgUpdateSegment:Init Could not initialize signing client. Wallet is required.')
				} else{
					throw new Error('TxClient:MsgUpdateSegment:Create Could not create message: ' + e.message)
				}
			}
		},
		async MsgFetchTokensBySegmentId({ rootGetters }, { value }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgFetchTokensBySegmentId(value)
				return msg
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgFetchTokensBySegmentId:Init Could not initialize signing client. Wallet is required.')
				} else{
					throw new Error('TxClient:MsgFetchTokensBySegmentId:Create Could not create message: ' + e.message)
				}
			}
		},
		async MsgCreateWallet({ rootGetters }, { value }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgCreateWallet(value)
				return msg
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgCreateWallet:Init Could not initialize signing client. Wallet is required.')
				} else{
					throw new Error('TxClient:MsgCreateWallet:Create Could not create message: ' + e.message)
				}
			}
		},
		async MsgCreateDocumentTokenMapper({ rootGetters }, { value }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgCreateDocumentTokenMapper(value)
				return msg
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgCreateDocumentTokenMapper:Init Could not initialize signing client. Wallet is required.')
				} else{
					throw new Error('TxClient:MsgCreateDocumentTokenMapper:Create Could not create message: ' + e.message)
				}
			}
		},
		async MsgUpdateDocumentTokenMapper({ rootGetters }, { value }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgUpdateDocumentTokenMapper(value)
				return msg
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgUpdateDocumentTokenMapper:Init Could not initialize signing client. Wallet is required.')
				} else{
					throw new Error('TxClient:MsgUpdateDocumentTokenMapper:Create Could not create message: ' + e.message)
				}
			}
		},
		async MsgFetchAllSegment({ rootGetters }, { value }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgFetchAllSegment(value)
				return msg
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgFetchAllSegment:Init Could not initialize signing client. Wallet is required.')
				} else{
					throw new Error('TxClient:MsgFetchAllSegment:Create Could not create message: ' + e.message)
				}
			}
		},
		async MsgUpdateTokenInformation({ rootGetters }, { value }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgUpdateTokenInformation(value)
				return msg
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgUpdateTokenInformation:Init Could not initialize signing client. Wallet is required.')
				} else{
					throw new Error('TxClient:MsgUpdateTokenInformation:Create Could not create message: ' + e.message)
				}
			}
		},
		async MsgDeactivateToken({ rootGetters }, { value }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgDeactivateToken(value)
				return msg
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgDeactivateToken:Init Could not initialize signing client. Wallet is required.')
				} else{
					throw new Error('TxClient:MsgDeactivateToken:Create Could not create message: ' + e.message)
				}
			}
		},
		async MsgFetchTokenHistory({ rootGetters }, { value }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgFetchTokenHistory(value)
				return msg
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgFetchTokenHistory:Init Could not initialize signing client. Wallet is required.')
				} else{
					throw new Error('TxClient:MsgFetchTokenHistory:Create Could not create message: ' + e.message)
				}
			}
		},
		async MsgFetchAllTokenHistoryGlobal({ rootGetters }, { value }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgFetchAllTokenHistoryGlobal(value)
				return msg
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgFetchAllTokenHistoryGlobal:Init Could not initialize signing client. Wallet is required.')
				} else{
					throw new Error('TxClient:MsgFetchAllTokenHistoryGlobal:Create Could not create message: ' + e.message)
				}
			}
		},
		async MsgFetchGetTokenHistoryGlobal({ rootGetters }, { value }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgFetchGetTokenHistoryGlobal(value)
				return msg
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgFetchGetTokenHistoryGlobal:Init Could not initialize signing client. Wallet is required.')
				} else{
					throw new Error('TxClient:MsgFetchGetTokenHistoryGlobal:Create Could not create message: ' + e.message)
				}
			}
		},
		async MsgUpdateToken({ rootGetters }, { value }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgUpdateToken(value)
				return msg
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgUpdateToken:Init Could not initialize signing client. Wallet is required.')
				} else{
					throw new Error('TxClient:MsgUpdateToken:Create Could not create message: ' + e.message)
				}
			}
		},
		async MsgFetchAllWallet({ rootGetters }, { value }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgFetchAllWallet(value)
				return msg
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgFetchAllWallet:Init Could not initialize signing client. Wallet is required.')
				} else{
					throw new Error('TxClient:MsgFetchAllWallet:Create Could not create message: ' + e.message)
				}
			}
		},
		async MsgFetchDocumentHash({ rootGetters }, { value }) {
			try {
				const txClient=await initTxClient(rootGetters)
				const msg = await txClient.msgFetchDocumentHash(value)
				return msg
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgFetchDocumentHash:Init Could not initialize signing client. Wallet is required.')
				} else{
					throw new Error('TxClient:MsgFetchDocumentHash:Create Could not create message: ' + e.message)
				}
			}
		},
		
	}
}
