// THIS FILE IS GENERATED AUTOMATICALLY. DO NOT MODIFY.

import { StdFee } from "@cosmjs/launchpad";
import { SigningStargateClient } from "@cosmjs/stargate";
import { Registry, OfflineSigner, EncodeObject, DirectSecp256k1HdWallet } from "@cosmjs/proto-signing";
import { Api } from "./rest";
import { MsgFetchGetSegment } from "./types/businesslogic/tx";
import { MsgUpdateTokenCopies } from "./types/businesslogic/tx";
import { MsgCreateSegmentWithId } from "./types/businesslogic/tx";
import { MsgCreateWalletWithId } from "./types/businesslogic/tx";
import { MsgDeleteTokenCopies } from "./types/businesslogic/tx";
import { MsgRevertToGenesis } from "./types/businesslogic/tx";
import { MsgFetchGetWallet } from "./types/businesslogic/tx";
import { MsgFetchAllSegmentHistory } from "./types/businesslogic/tx";
import { MsgFetchToken } from "./types/businesslogic/tx";
import { MsgCreateSegment } from "./types/businesslogic/tx";
import { MsgCreateToken } from "./types/businesslogic/tx";
import { MsgCreateHashToken } from "./types/businesslogic/tx";
import { MsgMoveTokenToSegment } from "./types/businesslogic/tx";
import { MsgDeleteDocumentTokenMapper } from "./types/businesslogic/tx";
import { MsgUpdateWallet } from "./types/businesslogic/tx";
import { MsgFetchGetWalletHistory } from "./types/businesslogic/tx";
import { MsgActivateToken } from "./types/businesslogic/tx";
import { MsgFetchAllWalletHistory } from "./types/businesslogic/tx";
import { MsgCloneToken } from "./types/businesslogic/tx";
import { MsgFetchSegmentHistory } from "./types/businesslogic/tx";
import { MsgFetchTokensByWalletId } from "./types/businesslogic/tx";
import { MsgMoveTokenToWallet } from "./types/businesslogic/tx";
import { MsgFetchDocumentHashHistory } from "./types/businesslogic/tx";
import { MsgCreateTokenCopies } from "./types/businesslogic/tx";
import { MsgUpdateSegment } from "./types/businesslogic/tx";
import { MsgFetchTokensBySegmentId } from "./types/businesslogic/tx";
import { MsgCreateWallet } from "./types/businesslogic/tx";
import { MsgCreateDocumentTokenMapper } from "./types/businesslogic/tx";
import { MsgUpdateDocumentTokenMapper } from "./types/businesslogic/tx";
import { MsgFetchAllSegment } from "./types/businesslogic/tx";
import { MsgUpdateTokenInformation } from "./types/businesslogic/tx";
import { MsgDeactivateToken } from "./types/businesslogic/tx";
import { MsgFetchTokenHistory } from "./types/businesslogic/tx";
import { MsgFetchAllTokenHistoryGlobal } from "./types/businesslogic/tx";
import { MsgFetchGetTokenHistoryGlobal } from "./types/businesslogic/tx";
import { MsgUpdateToken } from "./types/businesslogic/tx";
import { MsgFetchAllWallet } from "./types/businesslogic/tx";
import { MsgFetchDocumentHash } from "./types/businesslogic/tx";


const types = [
  ["/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgFetchGetSegment", MsgFetchGetSegment],
  ["/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgUpdateTokenCopies", MsgUpdateTokenCopies],
  ["/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgCreateSegmentWithId", MsgCreateSegmentWithId],
  ["/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgCreateWalletWithId", MsgCreateWalletWithId],
  ["/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgDeleteTokenCopies", MsgDeleteTokenCopies],
  ["/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgRevertToGenesis", MsgRevertToGenesis],
  ["/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgFetchGetWallet", MsgFetchGetWallet],
  ["/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgFetchAllSegmentHistory", MsgFetchAllSegmentHistory],
  ["/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgFetchToken", MsgFetchToken],
  ["/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgCreateSegment", MsgCreateSegment],
  ["/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgCreateToken", MsgCreateToken],
  ["/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgCreateHashToken", MsgCreateHashToken],
  ["/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgMoveTokenToSegment", MsgMoveTokenToSegment],
  ["/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgDeleteDocumentTokenMapper", MsgDeleteDocumentTokenMapper],
  ["/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgUpdateWallet", MsgUpdateWallet],
  ["/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgFetchGetWalletHistory", MsgFetchGetWalletHistory],
  ["/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgActivateToken", MsgActivateToken],
  ["/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgFetchAllWalletHistory", MsgFetchAllWalletHistory],
  ["/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgCloneToken", MsgCloneToken],
  ["/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgFetchSegmentHistory", MsgFetchSegmentHistory],
  ["/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgFetchTokensByWalletId", MsgFetchTokensByWalletId],
  ["/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgMoveTokenToWallet", MsgMoveTokenToWallet],
  ["/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgFetchDocumentHashHistory", MsgFetchDocumentHashHistory],
  ["/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgCreateTokenCopies", MsgCreateTokenCopies],
  ["/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgUpdateSegment", MsgUpdateSegment],
  ["/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgFetchTokensBySegmentId", MsgFetchTokensBySegmentId],
  ["/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgCreateWallet", MsgCreateWallet],
  ["/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgCreateDocumentTokenMapper", MsgCreateDocumentTokenMapper],
  ["/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgUpdateDocumentTokenMapper", MsgUpdateDocumentTokenMapper],
  ["/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgFetchAllSegment", MsgFetchAllSegment],
  ["/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgUpdateTokenInformation", MsgUpdateTokenInformation],
  ["/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgDeactivateToken", MsgDeactivateToken],
  ["/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgFetchTokenHistory", MsgFetchTokenHistory],
  ["/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgFetchAllTokenHistoryGlobal", MsgFetchAllTokenHistoryGlobal],
  ["/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgFetchGetTokenHistoryGlobal", MsgFetchGetTokenHistoryGlobal],
  ["/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgUpdateToken", MsgUpdateToken],
  ["/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgFetchAllWallet", MsgFetchAllWallet],
  ["/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgFetchDocumentHash", MsgFetchDocumentHash],
  
];
export const MissingWalletError = new Error("wallet is required");

export const registry = new Registry(<any>types);

const defaultFee = {
  amount: [],
  gas: "200000",
};

interface TxClientOptions {
  addr: string
}

interface SignAndBroadcastOptions {
  fee: StdFee,
  memo?: string
}

const txClient = async (wallet: OfflineSigner, { addr: addr }: TxClientOptions = { addr: "http://localhost:26657" }) => {
  if (!wallet) throw MissingWalletError;
  let client;
  if (addr) {
    client = await SigningStargateClient.connectWithSigner(addr, wallet, { registry });
  }else{
    // client = await SigningStargateClient.offline( wallet, { registry });
  }
  const { address } = (await wallet.getAccounts())[0];

  return {
    signAndBroadcast: (msgs: EncodeObject[], { fee, memo }: SignAndBroadcastOptions = {fee: defaultFee, memo: ""}) => client.signAndBroadcast(address, msgs, fee,memo),
    msgFetchGetSegment: (data: MsgFetchGetSegment): EncodeObject => ({ typeUrl: "/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgFetchGetSegment", value: MsgFetchGetSegment.fromPartial( data ) }),
    msgUpdateTokenCopies: (data: MsgUpdateTokenCopies): EncodeObject => ({ typeUrl: "/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgUpdateTokenCopies", value: MsgUpdateTokenCopies.fromPartial( data ) }),
    msgCreateSegmentWithId: (data: MsgCreateSegmentWithId): EncodeObject => ({ typeUrl: "/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgCreateSegmentWithId", value: MsgCreateSegmentWithId.fromPartial( data ) }),
    msgCreateWalletWithId: (data: MsgCreateWalletWithId): EncodeObject => ({ typeUrl: "/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgCreateWalletWithId", value: MsgCreateWalletWithId.fromPartial( data ) }),
    msgDeleteTokenCopies: (data: MsgDeleteTokenCopies): EncodeObject => ({ typeUrl: "/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgDeleteTokenCopies", value: MsgDeleteTokenCopies.fromPartial( data ) }),
    msgRevertToGenesis: (data: MsgRevertToGenesis): EncodeObject => ({ typeUrl: "/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgRevertToGenesis", value: MsgRevertToGenesis.fromPartial( data ) }),
    msgFetchGetWallet: (data: MsgFetchGetWallet): EncodeObject => ({ typeUrl: "/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgFetchGetWallet", value: MsgFetchGetWallet.fromPartial( data ) }),
    msgFetchAllSegmentHistory: (data: MsgFetchAllSegmentHistory): EncodeObject => ({ typeUrl: "/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgFetchAllSegmentHistory", value: MsgFetchAllSegmentHistory.fromPartial( data ) }),
    msgFetchToken: (data: MsgFetchToken): EncodeObject => ({ typeUrl: "/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgFetchToken", value: MsgFetchToken.fromPartial( data ) }),
    msgCreateSegment: (data: MsgCreateSegment): EncodeObject => ({ typeUrl: "/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgCreateSegment", value: MsgCreateSegment.fromPartial( data ) }),
    msgCreateToken: (data: MsgCreateToken): EncodeObject => ({ typeUrl: "/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgCreateToken", value: MsgCreateToken.fromPartial( data ) }),
    msgCreateHashToken: (data: MsgCreateHashToken): EncodeObject => ({ typeUrl: "/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgCreateHashToken", value: MsgCreateHashToken.fromPartial( data ) }),
    msgMoveTokenToSegment: (data: MsgMoveTokenToSegment): EncodeObject => ({ typeUrl: "/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgMoveTokenToSegment", value: MsgMoveTokenToSegment.fromPartial( data ) }),
    msgDeleteDocumentTokenMapper: (data: MsgDeleteDocumentTokenMapper): EncodeObject => ({ typeUrl: "/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgDeleteDocumentTokenMapper", value: MsgDeleteDocumentTokenMapper.fromPartial( data ) }),
    msgUpdateWallet: (data: MsgUpdateWallet): EncodeObject => ({ typeUrl: "/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgUpdateWallet", value: MsgUpdateWallet.fromPartial( data ) }),
    msgFetchGetWalletHistory: (data: MsgFetchGetWalletHistory): EncodeObject => ({ typeUrl: "/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgFetchGetWalletHistory", value: MsgFetchGetWalletHistory.fromPartial( data ) }),
    msgActivateToken: (data: MsgActivateToken): EncodeObject => ({ typeUrl: "/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgActivateToken", value: MsgActivateToken.fromPartial( data ) }),
    msgFetchAllWalletHistory: (data: MsgFetchAllWalletHistory): EncodeObject => ({ typeUrl: "/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgFetchAllWalletHistory", value: MsgFetchAllWalletHistory.fromPartial( data ) }),
    msgCloneToken: (data: MsgCloneToken): EncodeObject => ({ typeUrl: "/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgCloneToken", value: MsgCloneToken.fromPartial( data ) }),
    msgFetchSegmentHistory: (data: MsgFetchSegmentHistory): EncodeObject => ({ typeUrl: "/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgFetchSegmentHistory", value: MsgFetchSegmentHistory.fromPartial( data ) }),
    msgFetchTokensByWalletId: (data: MsgFetchTokensByWalletId): EncodeObject => ({ typeUrl: "/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgFetchTokensByWalletId", value: MsgFetchTokensByWalletId.fromPartial( data ) }),
    msgMoveTokenToWallet: (data: MsgMoveTokenToWallet): EncodeObject => ({ typeUrl: "/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgMoveTokenToWallet", value: MsgMoveTokenToWallet.fromPartial( data ) }),
    msgFetchDocumentHashHistory: (data: MsgFetchDocumentHashHistory): EncodeObject => ({ typeUrl: "/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgFetchDocumentHashHistory", value: MsgFetchDocumentHashHistory.fromPartial( data ) }),
    msgCreateTokenCopies: (data: MsgCreateTokenCopies): EncodeObject => ({ typeUrl: "/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgCreateTokenCopies", value: MsgCreateTokenCopies.fromPartial( data ) }),
    msgUpdateSegment: (data: MsgUpdateSegment): EncodeObject => ({ typeUrl: "/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgUpdateSegment", value: MsgUpdateSegment.fromPartial( data ) }),
    msgFetchTokensBySegmentId: (data: MsgFetchTokensBySegmentId): EncodeObject => ({ typeUrl: "/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgFetchTokensBySegmentId", value: MsgFetchTokensBySegmentId.fromPartial( data ) }),
    msgCreateWallet: (data: MsgCreateWallet): EncodeObject => ({ typeUrl: "/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgCreateWallet", value: MsgCreateWallet.fromPartial( data ) }),
    msgCreateDocumentTokenMapper: (data: MsgCreateDocumentTokenMapper): EncodeObject => ({ typeUrl: "/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgCreateDocumentTokenMapper", value: MsgCreateDocumentTokenMapper.fromPartial( data ) }),
    msgUpdateDocumentTokenMapper: (data: MsgUpdateDocumentTokenMapper): EncodeObject => ({ typeUrl: "/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgUpdateDocumentTokenMapper", value: MsgUpdateDocumentTokenMapper.fromPartial( data ) }),
    msgFetchAllSegment: (data: MsgFetchAllSegment): EncodeObject => ({ typeUrl: "/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgFetchAllSegment", value: MsgFetchAllSegment.fromPartial( data ) }),
    msgUpdateTokenInformation: (data: MsgUpdateTokenInformation): EncodeObject => ({ typeUrl: "/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgUpdateTokenInformation", value: MsgUpdateTokenInformation.fromPartial( data ) }),
    msgDeactivateToken: (data: MsgDeactivateToken): EncodeObject => ({ typeUrl: "/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgDeactivateToken", value: MsgDeactivateToken.fromPartial( data ) }),
    msgFetchTokenHistory: (data: MsgFetchTokenHistory): EncodeObject => ({ typeUrl: "/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgFetchTokenHistory", value: MsgFetchTokenHistory.fromPartial( data ) }),
    msgFetchAllTokenHistoryGlobal: (data: MsgFetchAllTokenHistoryGlobal): EncodeObject => ({ typeUrl: "/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgFetchAllTokenHistoryGlobal", value: MsgFetchAllTokenHistoryGlobal.fromPartial( data ) }),
    msgFetchGetTokenHistoryGlobal: (data: MsgFetchGetTokenHistoryGlobal): EncodeObject => ({ typeUrl: "/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgFetchGetTokenHistoryGlobal", value: MsgFetchGetTokenHistoryGlobal.fromPartial( data ) }),
    msgUpdateToken: (data: MsgUpdateToken): EncodeObject => ({ typeUrl: "/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgUpdateToken", value: MsgUpdateToken.fromPartial( data ) }),
    msgFetchAllWallet: (data: MsgFetchAllWallet): EncodeObject => ({ typeUrl: "/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgFetchAllWallet", value: MsgFetchAllWallet.fromPartial( data ) }),
    msgFetchDocumentHash: (data: MsgFetchDocumentHash): EncodeObject => ({ typeUrl: "/silicon_economy.base.blockchainbroker.tokenmanager.businesslogic.MsgFetchDocumentHash", value: MsgFetchDocumentHash.fromPartial( data ) }),
    
  };
};

interface QueryClientOptions {
  addr: string
}

const queryClient = async ({ addr: addr }: QueryClientOptions = { addr: "http://localhost:1317" }) => {
  return new Api({ baseUrl: addr });
};

export {
  txClient,
  queryClient,
};
