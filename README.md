<!--
Copyright Open Logistics Foundation

Licensed under the Open Logistics Foundation License 1.3.
For details on the licensing terms, see the LICENSE file.
SPDX-License-Identifier: OLFL-1.3
-->

# eCMR - Token Manager 

This Repository contains the token manager blockchain and a microservice serving as a light client for easy access to the blockchain.

In the eCMR project, the blockchain is being used to save a document's hash value on the blockchain. At a later point, a document's hash can be requested, which then can be compared to the document, verifying the document's integrity.

# Detailed descriptions

## Blockchain
The blockchain is using the [Cosmos SDK](https://github.com/cosmos/cosmos-sdk) and [Tendermint](https://github.com/tendermint/tendermint). It is being compiled using [Starport](https://github.com/tendermint/starport).


## Microservice
The microservice is a microservice built with [NestJS](https://nestjs.com/) using the [CosmJS](https://github.com/cosmos/cosmjs) packages for signing and broadcating transactions on the blockchain. The microservice's API provides three methods, one for writing a document's hash onto the blockchain and two for reading the document's hash, either with or without the document's history.

# Run
## With docker-compose
1. build images <br>
    `docker-compose build`
2. run  <br>
    `docker-compose up`
