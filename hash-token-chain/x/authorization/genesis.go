// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package authorization

import (
	sdk "github.com/cosmos/cosmos-sdk/types"

	"gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/TokenManager/x/authorization/keeper"
	"gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/TokenManager/x/authorization/types"
)

// InitGenesis initializes the capability module's state from a provided genesis state.
func InitGenesis(ctx sdk.Context, k keeper.Keeper, genState types.GenesisState) {
	k.InitGenesis(ctx, genState)
}

// ExportGenesis returns the capability module's exported genesis.
func ExportGenesis(ctx sdk.Context, k keeper.Keeper) *types.GenesisState {
	genesis := types.DefaultGenesis()

	// this line is used by starport scaffolding # genesis/module/export
	// Get all blockchainAccountState
	blockchainAccountStateList := k.GetAllBlockchainAccountState(ctx)
	for _, elem := range blockchainAccountStateList {
		elem := elem
		genesis.BlockchainAccountStateList = append(genesis.BlockchainAccountStateList, &elem)
	}

	// Get all applicationRoleState
	applicationRoleStateList := k.GetAllApplicationRoleState(ctx)
	for _, elem := range applicationRoleStateList {
		elem := elem
		genesis.ApplicationRoleStateList = append(genesis.ApplicationRoleStateList, &elem)
	}

	// Get all blockchainAccount
	blockchainAccountList := k.GetAllBlockchainAccount(ctx)
	for _, elem := range blockchainAccountList {
		elem := elem
		genesis.BlockchainAccountList = append(genesis.BlockchainAccountList, &elem)
	}

	// Get all applicationRole
	applicationRoleList := k.GetAllApplicationRole(ctx)
	for _, elem := range applicationRoleList {
		elem := elem
		genesis.ApplicationRoleList = append(genesis.ApplicationRoleList, &elem)
	}

	return genesis
}
