// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package types

const (
	// AuthorizationCreateAccount grants permission to create a new Account
	AuthorizationCreateAccount = "AUTHORIZATION-MODULE_CREATE-ACCOUNT"
	// AuthorizationGrantRole grants permission to grant a role to an user
	AuthorizationGrantRole = "AUTHORIZATION-MODULE_GRANT-ROLES"
	// AuthorizationRevokeRole grants permission to revoke an existing role of a user
	AuthorizationRevokeRole = "AUTHORIZATION-MODULE_REVOKE-ROLES"
	// AuthorizationBlockAccount grants permission to block an existing account
	AuthorizationBlockAccount = "AUTHORIZATION-MODULE_BLOCK-ACCOUNT"
	// AuthorizationCreateApplicationRole grants permission to create a new application role
	AuthorizationCreateApplicationRole = "AUTHORIZATION-MODULE_CREATE-APPLICATION-ROLE"
	// AuthorizationUpdateApplicationRole grants permission to update an existing application role
	AuthorizationUpdateApplicationRole = "AUTHORIZATION-MODULE_UPDATE-APPLICATION-ROLE"
	// AuthorizationReadApplicationRole grants permission to read existing application role
	AuthorizationReadApplicationRole = "AUTHORIZATION-MODULE_READ-APPLICATION-ROLE"
	// AuthorizationDeactivateApplicationRole grants permission to read existing application role
	AuthorizationDeactivateApplicationRole = "AUTHORIZATION-MODULE_DEACTIVATE-APPLICATION-ROLE"
	// AuthorizationUpdateConfiguration grants permission to update the module configuration state
	AuthorizationUpdateConfiguration = "AUTHORIZATION-NODULE_UPDATE_CONFIGURATION"

	AuthorizationFetchAllApplicationRole   = "AUTHORIZATION-MODULE_FETCH-ALL-APPLICATION-ROLE"
	AuthorizationFetchApplicationRole      = "AUTHORIZATION-MODULE_FETCH-APPLICATION-ROLE"
	AuthorizationFetchAllBlockchainAccount = "AUTHORIZATION-MODULE_FETCH-ALL-BLOCKCHAIN-ACCOUNT"
	AuthorizationFetchBlockchainAccount    = "AUTHORIZATION-MODULE_FETCH-BLOCKCHAIN-ACCOUNT"

	// ApplicationRoles for businesslogic transactions
	BusinessLogicCreateToken            = "BUSINESSLOGIC-MODULE_CREATE-TOKEN"
	BusinessLogicUpdateToken            = "BUSINESSLOGIC-MODULE_UPDATE-TOKEN"
	BusinessLogicMoveTokenToWallet      = "BUSINESSLOGIC-MODULE_MOVE-TOKEN-TO-WALLET"
	BusinessLogicSetTokenValidStatus    = "BUSINESSLOGIC-MODULE_SET-TOKEN-VALID-STATUS"
	BusinessLogicActivateToken          = "BUSINESSLOGIC-MODULE_ACTIVATE-TOKEN"
	BusinessLogicDeactivateToken        = "BUSINESSLOGIC-MODULE_DEACTIVATE-TOKEN"
	BusinessLogicStoreDocumentHash      = "BUSINESSLOGIC-MODULE_STORE-DOCUMENT-HASH"
	BusinessLogicCloneToken             = "BUSINESSLOGIC-MODULE_CLONE-TOKEN"
	BusinessLogicRevertModulesToGenesis = "BUSINESSLOGIC-MODULE_REVERT_MODULES_TO_GENESIS"

	// ApplicationRoles for businesslogc query transactions
	BusinessLogicGetDocumentHash      = "BUSINESSLOGIC-MODULE_GET-DOCUMENT-HASH"
	BusinessLogicGetTokensByWalletId  = "BUSINESSLOGIC-MODULE_GET-TOKENS-BY-TOKEN-WALLET-ID"
	BusinessLogicGetTokensBySegmentId = "BUSINESSLOGIC-MODULE_GET-TOKENS-BY-SEGMENT-ID"

	// ApplicationRoles for hashtoken transactions
	HashTokenCreateToken            = "HASHTOKEN-MODULE_CREATE-TOKEN"
	HashTokenUpdateToken            = "HASHTOKEN-MODULE_UPDATE-TOKEN"
	HashTokenDeactivateToken        = "HASHTOKEN-MODULE_DEACTIVATE-TOKEN"
	HashTokenActivateToken          = "HASHTOKEN-MODULE_ACTIVATE-TOKEN"
	HashTokenUpdateTokenInformation = "HASHTOKEN-MODULE_UPDATE-TOKEN-INFORMATION"
	HashTokenGetToken               = "HASHTOKEN-MODULE_GET-TOKEN"
	HashTokenGetAllToken            = "HASHTOKEN-MODULE_GET-ALL-TOKEN"
	HashTokenGetTokenHistory        = "HASHTOKEN-MODULE_GET-TOKEN-HISTORY"
	HashTokenGetAllTokenHistory     = "HASHTOKEN-MODULE_GET-ALL-TOKEN-HISTORY"

	// ApplicationRoles for token transactions
	TokenCreateToken            = "TOKEN-MODULE_CREATE-TOKEN"
	TokenUpdateToken            = "TOKEN-MODULE_UPDATE-TOKEN"
	TokenDeactivateToken        = "TOKEN-MODULE_DEACTIVATE-TOKEN"
	TokenActivateToken          = "TOKEN-MODULE_ACTIVATE-TOKEN"
	TokenUpdateTokenInformation = "TOKEN-MODULE_UPDATE-TOKEN-INFORMATION"
	TokenGetToken               = "TOKEN-MODULE_GET-TOKEN"
	TokenGetAllToken            = "TOKEN-MODULE_GET-ALL-TOKEN"
	TokenGetTokenHistory        = "TOKEN-MODULE_GET-TOKEN-HISTORY"
	TokenGetAllTokenHistory     = "TOKEN-MODULE_GET-ALL-TOKEN-HISTORY"

	// ApplicationRoles for tokenwallet transactions
	WalletCreateSegment               = "WALLET-MODULE_CREATE-SEGMENT"
	WalletUpdateSegment               = "WALLET-MODULE_UPDATE-SEGMENT"
	WalletCreateSegmentHistory        = "WALLET-MODULE_CREATE-SEGMENT-HISTORY"
	WalletUpdateSegmentHistory        = "WALLET-MODULE_UPDATE-SEGMENT-HISTORY"
	WalletCreateWallet                = "WALLET-MODULE_CREATE-WALLET"
	WalletUpdateWallet                = "WALLET-MODULE_UPDATE-WALLET"
	WalletCreateWalletHistory         = "WALLET-MODULE_CREATE-WALLET-HISTORY"
	WalletUpdateWalletHistory         = "WALLET-MODULE_UPDATE-WALLET-HISTORY"
	WalletAssignCosmosAddressToWallet = "WALLET-MODULE_ASSIGN-COSMOSADRESS-TO-WALLET"
	WalletMoveTokenToSegment          = "WALLET-MODULE_MOVE-TOKEN-TO-SEGMENT"
	WalletMoveTokenToWallet           = "WALLET-MODULE_MOVE-TOKEN-TO-WALLET"
	WalletRemoveTokenRefFromSegment   = "WALLET-MODULE_REMOVE-TOKENREF-FROM-SEGMENT"

	WalletQuerySegmentAll            = "WALLET-MODULE_QUERY-SEGMENT-ALL"
	WalletQuerySegment               = "WALLET-MODULE_QUERY-SEGMENT"
	WalletQueryWalletAll             = "WALLET-MODULE_QUERY-WALLET-ALL"
	WalletQueryWallet                = "WALLET-MODULE_QUERY-WALLET"
	WalletQueryTokenHistoryGlobalAll = "WALLET-MODULE_QUERY-TOKEN-HISTORY-GLOBAL-ALL"
	WalletQueryTokenHistoryGlobal    = "WALLET-MODULE_QUERY-TOKEN-HISTORY-GLOBAL"
	WalletQueryWalletHistoryAll      = "WALLET-MODULE_QUERY-TOKEN-WALLET-HISTORY-ALL"
	WalletQueryWalletHistory         = "WALLET-MODULE_QUERY-TOKEN-WALLET-HISTORY"
	WalletQuerySegmentHistoryAll     = "WALLET-MODULE_QUERY-SEGMENT-HISTORY-ALL"
	WalletQuerySegmentHistory        = "WALLET-MODULE_QUERY-SEGMENT-HISTORY"
)
