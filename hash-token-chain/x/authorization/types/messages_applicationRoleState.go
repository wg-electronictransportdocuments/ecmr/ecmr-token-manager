// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package types

import (
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
)

var _ sdk.Msg = &MsgCreateApplicationRoleState{}

func NewMsgCreateApplicationRoleState(creator string, applicationRoleID string, description string, timeStamp string) *MsgCreateApplicationRoleState {
	return &MsgCreateApplicationRoleState{
		Creator:           creator,
		ApplicationRoleID: applicationRoleID,
		Description:       description,
		Valid:             true,
		TimeStamp:         timeStamp,
	}
}

func (msg *MsgCreateApplicationRoleState) Route() string {
	return RouterKey
}

func (msg *MsgCreateApplicationRoleState) Type() string {
	return "CreateApplicationRoleState"
}

func (msg *MsgCreateApplicationRoleState) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgCreateApplicationRoleState) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgCreateApplicationRoleState) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgUpdateApplicationRoleState{}

func NewMsgUpdateApplicationRoleState(creator string, id string, applicationRoleID string, description string, timeStamp string) *MsgUpdateApplicationRoleState {
	return &MsgUpdateApplicationRoleState{
		Id:                id,
		Creator:           creator,
		ApplicationRoleID: applicationRoleID,
		Description:       description,
		Valid:             true,
		TimeStamp:         timeStamp,
	}
}

func (msg *MsgUpdateApplicationRoleState) Route() string {
	return RouterKey
}

func (msg *MsgUpdateApplicationRoleState) Type() string {
	return "UpdateApplicationRoleState"
}

func (msg *MsgUpdateApplicationRoleState) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgUpdateApplicationRoleState) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgUpdateApplicationRoleState) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgCreateApplicationRoleState{}

func NewMsgDeleteApplicationRoleState(creator string, id string) *MsgDeleteApplicationRoleState {
	return &MsgDeleteApplicationRoleState{
		Id:      id,
		Creator: creator,
	}
}
func (msg *MsgDeleteApplicationRoleState) Route() string {
	return RouterKey
}

func (msg *MsgDeleteApplicationRoleState) Type() string {
	return "DeleteApplicationRoleState"
}

func (msg *MsgDeleteApplicationRoleState) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgDeleteApplicationRoleState) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgDeleteApplicationRoleState) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}
