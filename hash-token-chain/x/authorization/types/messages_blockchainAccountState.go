// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package types

import (
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
)

var _ sdk.Msg = &MsgCreateBlockchainAccountState{}

func NewMsgCreateBlockchainAccountState(creator string, accountID string, timeStamp string) *MsgCreateBlockchainAccountState {
	return &MsgCreateBlockchainAccountState{
		Creator:            creator,
		AccountID:          accountID,
		TimeStamp:          timeStamp,
		ApplicationRoleIDs: nil,
		Valid:              true,
	}
}

func (msg *MsgCreateBlockchainAccountState) Route() string {
	return RouterKey
}

func (msg *MsgCreateBlockchainAccountState) Type() string {
	return "CreateBlockchainAccountState"
}

func (msg *MsgCreateBlockchainAccountState) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgCreateBlockchainAccountState) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgCreateBlockchainAccountState) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgUpdateBlockchainAccountState{}

func NewMsgUpdateBlockchainAccountState(creator string, id string, accountID string, timeStamp string) *MsgUpdateBlockchainAccountState {
	return &MsgUpdateBlockchainAccountState{
		Id:                 id,
		Creator:            creator,
		AccountID:          accountID,
		TimeStamp:          timeStamp,
		ApplicationRoleIDs: nil,
		Valid:              true,
	}
}

func (msg *MsgUpdateBlockchainAccountState) Route() string {
	return RouterKey
}

func (msg *MsgUpdateBlockchainAccountState) Type() string {
	return "UpdateBlockchainAccountState"
}

func (msg *MsgUpdateBlockchainAccountState) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgUpdateBlockchainAccountState) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgUpdateBlockchainAccountState) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgCreateBlockchainAccountState{}

func NewMsgDeleteBlockchainAccountState(creator string, id string) *MsgDeleteBlockchainAccountState {
	return &MsgDeleteBlockchainAccountState{
		Id:      id,
		Creator: creator,
	}
}
func (msg *MsgDeleteBlockchainAccountState) Route() string {
	return RouterKey
}

func (msg *MsgDeleteBlockchainAccountState) Type() string {
	return "DeleteBlockchainAccountState"
}

func (msg *MsgDeleteBlockchainAccountState) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgDeleteBlockchainAccountState) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgDeleteBlockchainAccountState) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}
