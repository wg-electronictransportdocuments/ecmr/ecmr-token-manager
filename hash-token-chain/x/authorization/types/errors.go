// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package types

// DONTCOVER

import (
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
)

// x/authorization module sentinel errors

var (
	ErrApplicationRoleAlreadyExists   = sdkerrors.Register(ModuleName, 2, "application role already exists")
	ErrNoPermission                   = sdkerrors.Register(ModuleName, 4, "You do not have the permission for this transaction")
	ErrBlockchainAccountAlreadyExists = sdkerrors.Register(ModuleName, 5, "blockchain account already exists")
)
