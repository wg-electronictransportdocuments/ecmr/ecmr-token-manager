// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package types

import (
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
)

var _ sdk.Msg = &MsgCreateApplicationRole{}

func NewMsgCreateApplicationRole(creator string, id string, description string) *MsgCreateApplicationRole {
	return &MsgCreateApplicationRole{
		Creator:     creator,
		Id:          id,
		Description: description,
	}
}

func (msg *MsgCreateApplicationRole) Route() string {
	return RouterKey
}

func (msg *MsgCreateApplicationRole) Type() string {
	return "CreateApplicationRole"
}

func (msg *MsgCreateApplicationRole) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgCreateApplicationRole) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgCreateApplicationRole) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgUpdateApplicationRole{}

func NewMsgUpdateApplicationRole(creator string, id string, description string) *MsgUpdateApplicationRole {
	return &MsgUpdateApplicationRole{
		Id:          id,
		Creator:     creator,
		Description: description,
	}
}

func (msg *MsgUpdateApplicationRole) Route() string {
	return RouterKey
}

func (msg *MsgUpdateApplicationRole) Type() string {
	return "UpdateApplicationRole"
}

func (msg *MsgUpdateApplicationRole) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgUpdateApplicationRole) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgUpdateApplicationRole) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgCreateApplicationRole{}

func NewMsgDeleteApplicationRole(creator string, id string) *MsgDeleteApplicationRole {
	return &MsgDeleteApplicationRole{
		Id:      id,
		Creator: creator,
	}
}
func (msg *MsgDeleteApplicationRole) Route() string {
	return RouterKey
}

func (msg *MsgDeleteApplicationRole) Type() string {
	return "DeleteApplicationRole"
}

func (msg *MsgDeleteApplicationRole) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgDeleteApplicationRole) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgDeleteApplicationRole) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgDeactivateApplicationRole{}

func NewMsgDeactivateApplicationRole(creator string, id string) *MsgDeactivateApplicationRole {
	return &MsgDeactivateApplicationRole{
		Creator: creator,
		Id:      id,
	}
}
func (msg *MsgDeactivateApplicationRole) Route() string {
	return RouterKey
}

func (msg *MsgDeactivateApplicationRole) Type() string {
	return "DeactivateApplicationRole"
}

func (msg *MsgDeactivateApplicationRole) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgDeactivateApplicationRole) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgDeactivateApplicationRole) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}
