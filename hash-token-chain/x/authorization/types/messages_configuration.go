// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package types

import (
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
)

var _ sdk.Msg = &MsgCreateConfiguration{}

func NewMsgCreateConfiguration(creator string, PermissionCheck bool) *MsgCreateConfiguration {
	return &MsgCreateConfiguration{
		Creator:         creator,
		PermissionCheck: PermissionCheck,
	}
}

func (msg *MsgCreateConfiguration) Route() string {
	return RouterKey
}

func (msg *MsgCreateConfiguration) Type() string {
	return "CreateConfiguration"
}

func (msg *MsgCreateConfiguration) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgCreateConfiguration) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgCreateConfiguration) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgUpdateConfiguration{}

func NewMsgUpdateConfiguration(creator string, id uint64, PermissionCheck bool) *MsgUpdateConfiguration {
	return &MsgUpdateConfiguration{
		Id:              id,
		Creator:         creator,
		PermissionCheck: PermissionCheck,
	}
}

func (msg *MsgUpdateConfiguration) Route() string {
	return RouterKey
}

func (msg *MsgUpdateConfiguration) Type() string {
	return "UpdateConfiguration"
}

func (msg *MsgUpdateConfiguration) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgUpdateConfiguration) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgUpdateConfiguration) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgDeleteConfiguration{}

func NewMsgDeleteConfiguration(creator string, id uint64) *MsgDeleteConfiguration {
	return &MsgDeleteConfiguration{
		Id:      id,
		Creator: creator,
	}
}
func (msg *MsgDeleteConfiguration) Route() string {
	return RouterKey
}

func (msg *MsgDeleteConfiguration) Type() string {
	return "DeleteConfiguration"
}

func (msg *MsgDeleteConfiguration) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgDeleteConfiguration) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgDeleteConfiguration) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}
