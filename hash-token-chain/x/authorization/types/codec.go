// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package types

import (
	"github.com/cosmos/cosmos-sdk/codec"
	cdctypes "github.com/cosmos/cosmos-sdk/codec/types"
	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/cosmos/cosmos-sdk/types/msgservice"
)

func RegisterCodec(cdc *codec.LegacyAmino) {
	// this line is used by starport scaffolding # 2
	cdc.RegisterConcrete(&MsgCreateConfiguration{}, "authorization/CreateConfiguration", nil)
	cdc.RegisterConcrete(&MsgUpdateConfiguration{}, "authorization/UpdateConfiguration", nil)
	cdc.RegisterConcrete(&MsgDeleteConfiguration{}, "authorization/DeleteConfiguration", nil)

	cdc.RegisterConcrete(&MsgCreateBlockchainAccountState{}, "authorization/CreateBlockchainAccountState", nil)
	cdc.RegisterConcrete(&MsgUpdateBlockchainAccountState{}, "authorization/UpdateBlockchainAccountState", nil)
	cdc.RegisterConcrete(&MsgDeleteBlockchainAccountState{}, "authorization/DeleteBlockchainAccountState", nil)

	cdc.RegisterConcrete(&MsgCreateApplicationRoleState{}, "authorization/CreateApplicationRoleState", nil)
	cdc.RegisterConcrete(&MsgUpdateApplicationRoleState{}, "authorization/UpdateApplicationRoleState", nil)
	cdc.RegisterConcrete(&MsgDeleteApplicationRoleState{}, "authorization/DeleteApplicationRoleState", nil)

	cdc.RegisterConcrete(&MsgCreateBlockchainAccount{}, "authorization/CreateBlockchainAccount", nil)
	cdc.RegisterConcrete(&MsgDeactivateBlockchainAccount{}, "authorization/DeactivateBlockchainAccount", nil)
	cdc.RegisterConcrete(&MsgFetchAllBlockchainAccount{}, "authorization/FetchAllBlockchainAccount", nil)
	cdc.RegisterConcrete(&MsgFetchBlockchainAccount{}, "authorization/FetchBlockchainAccount", nil)

	cdc.RegisterConcrete(&MsgCreateApplicationRole{}, "authorization/CreateApplicationRole", nil)
	cdc.RegisterConcrete(&MsgUpdateApplicationRole{}, "authorization/UpdateApplicationRole", nil)
	cdc.RegisterConcrete(&MsgDeleteApplicationRole{}, "authorization/DeleteApplicationRole", nil)
	cdc.RegisterConcrete(&MsgDeactivateApplicationRole{}, "authorization/DeactivateApplicationRole", nil)
	cdc.RegisterConcrete(&MsgGrantAppRoleToBlockchainAccount{}, "authorization/GrantAppRoleToBlockchainAccount", nil)
	cdc.RegisterConcrete(&MsgRevokeAppRoleFromBlockchainAccount{}, "authorization/RevokeAppRoleFromBlockchainAccount", nil)
	cdc.RegisterConcrete(&MsgFetchAllApplicationRole{}, "authorization/FetchAllApplicationRole", nil)
	cdc.RegisterConcrete(&MsgFetchApplicationRole{}, "authorization/FetchApplicationRole", nil)

}

func RegisterInterfaces(registry cdctypes.InterfaceRegistry) {
	// this line is used by starport scaffolding # 3
	registry.RegisterImplementations((*sdk.Msg)(nil),
		&MsgCreateConfiguration{},
		&MsgUpdateConfiguration{},
		&MsgDeleteConfiguration{},
	)
	registry.RegisterImplementations((*sdk.Msg)(nil),
		&MsgCreateBlockchainAccountState{},
		&MsgUpdateBlockchainAccountState{},
		&MsgDeleteBlockchainAccountState{},
	)
	registry.RegisterImplementations((*sdk.Msg)(nil),
		&MsgCreateApplicationRoleState{},
		&MsgUpdateApplicationRoleState{},
		&MsgDeleteApplicationRoleState{},
	)
	registry.RegisterImplementations((*sdk.Msg)(nil),
		&MsgCreateBlockchainAccount{},
		&MsgDeactivateBlockchainAccount{},
		&MsgGrantAppRoleToBlockchainAccount{},
		&MsgRevokeAppRoleFromBlockchainAccount{},
		&MsgFetchAllBlockchainAccount{},
		&MsgFetchBlockchainAccount{},
	)
	registry.RegisterImplementations((*sdk.Msg)(nil),
		&MsgCreateApplicationRole{},
		&MsgUpdateApplicationRole{},
		&MsgDeleteApplicationRole{},
		&MsgDeactivateApplicationRole{},
		&MsgFetchAllApplicationRole{},
		&MsgFetchApplicationRole{},
	)

	msgservice.RegisterMsgServiceDesc(registry, &_Msg_serviceDesc)
}

var (
	amino     = codec.NewLegacyAmino()
	ModuleCdc = codec.NewAminoCodec(amino)
)
