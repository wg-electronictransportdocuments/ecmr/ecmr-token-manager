// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"
	"fmt"

	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkErrors "github.com/cosmos/cosmos-sdk/types/errors"

	"gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/TokenManager/x/authorization/types"
)

func (k Keeper) CreateBlockchainAccount(goCtx context.Context, msg *types.MsgCreateBlockchainAccount) (*types.MsgCreateBlockchainAccountResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	// Permission check
	check, err := k.HasRole(ctx, msg.Creator, types.AuthorizationCreateAccount)
	if err != nil || !check {
		return nil, sdkErrors.Wrap(types.ErrNoPermission, types.AuthorizationCreateAccount)
	}

	if k.HasBlockchainAccount(ctx, msg.Id) {
		return nil, sdkErrors.Wrap(types.ErrBlockchainAccountAlreadyExists, msg.Id)
	}

	id, err := k.AppendBlockchainAccount(
		ctx,
		msg.Creator,
		msg.Id,
	)
	return &types.MsgCreateBlockchainAccountResponse{
		Id: id,
	}, err
}

func (k Keeper) GrantAppRoleToBlockchainAccount(goCtx context.Context, msg *types.MsgGrantAppRoleToBlockchainAccount) (*types.MsgGrantAppRoleToBlockchainAccountResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	// Permission check
	check, err := k.HasRole(ctx, msg.Creator, types.AuthorizationGrantRole)
	if err != nil || !check {
		return nil, sdkErrors.Wrap(types.ErrNoPermission, types.AuthorizationGrantRole)
	}

	if !k.HasApplicationRole(ctx, msg.RoleID) {
		return nil, sdkErrors.ErrKeyNotFound
	}

	err = k.GrantApplicationPermission(ctx, msg.User, msg.RoleID, msg.Creator)
	if err != nil {
		return nil, err
	}
	return &types.MsgGrantAppRoleToBlockchainAccountResponse{}, nil
}

func (k Keeper) RevokeAppRoleFromBlockchainAccount(goCtx context.Context, msg *types.MsgRevokeAppRoleFromBlockchainAccount) (*types.MsgRevokeAppRoleFromBlockchainAccountResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	// Permission check
	check, checkErr := k.HasRole(ctx, msg.Creator, types.AuthorizationRevokeRole)
	if checkErr != nil || !check {
		return nil, sdkErrors.Wrap(types.ErrNoPermission, types.AuthorizationRevokeRole)
	}

	if !k.HasApplicationRole(ctx, msg.RoleID) {
		return nil, sdkErrors.ErrKeyNotFound
	}

	err := k.RevokeApplicationPermission(ctx, msg.Account, msg.RoleID, msg.Creator)
	if err != nil {
		return nil, err
	}

	return &types.MsgRevokeAppRoleFromBlockchainAccountResponse{}, nil
}

func (k Keeper) DeactivateBlockchainAccount(goCtx context.Context, msg *types.MsgDeactivateBlockchainAccount) (*types.MsgDeactivateBlockchainAccountResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	// Permission check
	check, err := k.HasRole(ctx, msg.Creator, types.AuthorizationBlockAccount)
	if err != nil || !check {
		return nil, sdkErrors.Wrap(types.ErrNoPermission, types.AuthorizationBlockAccount)
	}

	if !k.HasApplicationRole(ctx, msg.Id) {
		return nil, sdkErrors.Wrap(sdkErrors.ErrKeyNotFound, fmt.Sprintf("key %s doesn't exist", msg.Id))
	}

	err = k.SetBlockchainAccountInvalid(ctx, msg.Creator, msg.Id)

	return &types.MsgDeactivateBlockchainAccountResponse{}, err
}
