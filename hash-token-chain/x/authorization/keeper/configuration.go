// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"encoding/binary"
	"strconv"

	"github.com/cosmos/cosmos-sdk/store/prefix"
	sdk "github.com/cosmos/cosmos-sdk/types"

	"gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/TokenManager/x/authorization/types"
)

// GetConfigurationCount gets the total number of configuration
func (k Keeper) GetConfigurationCount(ctx sdk.Context) uint64 {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.ConfigurationCountKey))
	byteKey := types.KeyPrefix(types.ConfigurationCountKey)
	bz := store.Get(byteKey)

	// Count doesn't exist: no element
	if bz == nil {
		return 0
	}

	// Parse bytes
	count, err := strconv.ParseUint(string(bz), 10, 64)
	if err != nil {
		// Panic because the count should be always formattable to iint64
		panic("cannot decode count")
	}

	return count
}

// SetConfigurationCount sets the total number of configuration
func (k Keeper) SetConfigurationCount(ctx sdk.Context, count uint64) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.ConfigurationCountKey))
	byteKey := types.KeyPrefix(types.ConfigurationCountKey)
	bz := []byte(strconv.FormatUint(count, 10))
	store.Set(byteKey, bz)
}

// AppendConfiguration appends a configuration in the store with a new id and updates the count
func (k Keeper) AppendConfiguration(
	ctx sdk.Context,
	configuration types.Configuration,
) uint64 {
	// Create the configuration
	count := k.GetConfigurationCount(ctx)

	// Set the ID of the appended value
	configuration.Id = count

	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.ConfigurationKey))
	appendedValue := k.cdc.MustMarshal(&configuration)
	store.Set(GetConfigurationIDBytes(configuration.Id), appendedValue)

	// Update configuration count
	k.SetConfigurationCount(ctx, count+1)

	return count
}

// SetConfiguration sets a specific configuration in the store
func (k Keeper) SetConfiguration(ctx sdk.Context, configuration types.Configuration) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.ConfigurationKey))
	b := k.cdc.MustMarshal(&configuration)
	store.Set(GetConfigurationIDBytes(configuration.Id), b)
}

// GetConfiguration returns a configuration from its id
func (k Keeper) GetConfiguration(ctx sdk.Context, id uint64) types.Configuration {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.ConfigurationKey))
	var configuration types.Configuration
	k.cdc.MustUnmarshal(store.Get(GetConfigurationIDBytes(id)), &configuration)
	return configuration
}

// HasConfiguration checks if a configuration exists in the store
func (k Keeper) HasConfiguration(ctx sdk.Context, id uint64) bool {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.ConfigurationKey))
	return store.Has(GetConfigurationIDBytes(id))
}

// GetConfigurationIDBytes returns the byte representation of an ID
func GetConfigurationIDBytes(id uint64) []byte {
	bz := make([]byte, 8)
	binary.BigEndian.PutUint64(bz, id)
	return bz
}

// GetConfigurationIDFromBytes returns an ID in uint64 format from a byte array
func GetConfigurationIDFromBytes(bz []byte) uint64 {
	return binary.BigEndian.Uint64(bz)
}
