// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper_test

import (
	"time"

	sdkErrors "github.com/cosmos/cosmos-sdk/types/errors"

	"gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/TokenManager/app"
	"gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/TokenManager/x/authorization/types"
)

const (
	id1    = "id1"
	id2    = "id2"
	roleID = "roleID"
)

func (suite *AuthorizationTestSuite) TestConfiguration() {
	suite.SetupApplicationRoles()
	suite.SetupAdminAccount()

	suite.keeper.AppendConfiguration(suite.ctx, types.Configuration{Creator: "A", Id: 0, PermissionCheck: true})
	suite.keeper.HasConfiguration(suite.ctx, 0)
	suite.keeper.SetConfigurationCount(suite.ctx, 1)
	count := suite.keeper.GetConfigurationCount(suite.ctx)
	config := suite.keeper.GetConfiguration(suite.ctx, 0)

	suite.Require().Equal("A", config.Creator)
	suite.Require().Equal(uint64(0), config.Id)
	suite.Require().Equal(true, config.PermissionCheck)
	suite.Require().Equal(uint64(1), count)
}

func (suite *AuthorizationTestSuite) TestAppendApplicationRole() {
	suite.SetupApplicationRoles()
	suite.SetupAdminAccount()

	addr := []string{suite.addr1.String(), suite.addr2.String()}
	setupAuthorized := []types.MsgCreateApplicationRole{
		{
			Creator:     addr[0],
			Id:          id1,
			Description: app.Description,
		},
		{
			Creator:     addr[0],
			Id:          id2,
			Description: app.Description,
		},
	}
	setupUnauthorized := []types.MsgCreateApplicationRole{
		{
			Creator:     addr[1],
			Id:          id1,
			Description: app.Description,
		},
		{
			Creator:     addr[1],
			Id:          id2,
			Description: app.Description,
		},
	}

	for _, element := range setupAuthorized {
		var lengthAppRoles = len(suite.keeper.GetAllApplicationRole(suite.ctx))

		id, _ := suite.keeper.AppendApplicationRole(suite.ctx, element.Creator, element.Id, element.Description)

		exists := suite.keeper.HasApplicationRole(suite.ctx, id)
		suite.Require().True(exists)

		applicationRole, _ := suite.keeper.GetApplicationRole(suite.ctx, id)
		suite.Require().Equal(element.Id, applicationRole.Id)
		suite.Require().Equal(element.Description, applicationRole.ApplicationRoleStates[len(applicationRole.ApplicationRoleStates)-1].Description)

		allApplicationRoles := suite.keeper.GetAllApplicationRole(suite.ctx)
		suite.Require().NotNil(allApplicationRoles)
		suite.Require().Equal(lengthAppRoles+1, len(allApplicationRoles))

		owner := suite.keeper.GetApplicationRoleOwner(suite.ctx, id)
		suite.Require().Equal(element.Creator, owner)
	}

	for _, element := range setupUnauthorized {
		_, err := suite.keeper.AppendApplicationRole(suite.ctx, element.Creator, element.Id, element.Description)
		suite.Require().NoError(err)
	}
}

func (suite *AuthorizationTestSuite) TestSetApplicationRole() {
	addr := []string{suite.addr1.String(), suite.addr2.String()}
	ids := []string{id1, id2}
	setup := []types.ApplicationRole{
		{
			Creator: addr[0],
			Id:      id1,
			ApplicationRoleStates: []*types.ApplicationRoleState{
				{
					Creator:           addr[0],
					Id:                id1,
					ApplicationRoleID: id1,
					Description:       app.Description,
					Valid:             true,
					TimeStamp:         time.Now().UTC().Format(time.RFC3339),
				},
			},
		},
		{
			Creator: addr[1],
			Id:      id2,
			ApplicationRoleStates: []*types.ApplicationRoleState{
				{
					Creator:           addr[1],
					Id:                id2,
					ApplicationRoleID: id2,
					Description:       app.Description,
					Valid:             true,
					TimeStamp:         time.Now().UTC().Format(time.RFC3339),
				},
			},
		},
	}

	for index, element := range setup {
		suite.keeper.SetApplicationRole(suite.ctx, element)
		role, _ := suite.keeper.GetApplicationRole(suite.ctx, ids[index])
		suite.Require().Equal(role.Id, ids[index])
		suite.Require().Equal(role.Creator, addr[index])

		suite.Require().Equal(role.ApplicationRoleStates[len(role.ApplicationRoleStates)-1].ApplicationRoleID, ids[index])
		suite.Require().Equal(role.ApplicationRoleStates[len(role.ApplicationRoleStates)-1].Creator, addr[index])
		suite.Require().Equal(role.ApplicationRoleStates[len(role.ApplicationRoleStates)-1].Description, app.Description)
		suite.Require().Equal(role.ApplicationRoleStates[len(role.ApplicationRoleStates)-1].Id, ids[index])
		suite.Require().Equal(role.ApplicationRoleStates[len(role.ApplicationRoleStates)-1].Valid, true)
	}
}

func (suite *AuthorizationTestSuite) TestSetApplicationRoleInvalid() {
	suite.SetupApplicationRoles()
	suite.SetupAdminAccount()

	addr := []string{suite.addr1.String(), suite.addr2.String()}
	setup := []types.MsgCreateApplicationRole{
		{
			Creator:     addr[0],
			Id:          "id3",
			Description: app.Description,
		},
		{
			Creator:     addr[0],
			Id:          "id4",
			Description: app.Description,
		},
	}

	for _, element := range setup {
		id, errAppend := suite.keeper.AppendApplicationRole(suite.ctx, element.Creator, element.Id, element.Description)
		suite.Require().NoError(errAppend)
		appRole, _ := suite.keeper.GetApplicationRole(suite.ctx, id)
		suite.Require().True(appRole.ApplicationRoleStates[len(appRole.ApplicationRoleStates)-1].Valid)

		err := suite.keeper.SetApplicationRoleInvalid(suite.ctx, addr[1], id)
		suite.Require().NoError(err)

		suite.keeper.SetApplicationRoleInvalid(suite.ctx, addr[0], id)
		appRole, _ = suite.keeper.GetApplicationRole(suite.ctx, id)

		suite.Require().False(appRole.ApplicationRoleStates[len(appRole.ApplicationRoleStates)-1].Valid)
	}
}

func (suite *AuthorizationTestSuite) TestSetApplicationRoleDescription() {
	suite.SetupApplicationRoles()
	suite.SetupAdminAccount()

	addr := []string{suite.addr1.String(), suite.addr2.String()}
	setup := []types.MsgCreateApplicationRole{
		{
			Creator:     addr[0],
			Id:          "id5",
			Description: app.Description,
		},
		{
			Creator:     addr[0],
			Id:          "id6",
			Description: app.Description,
		},
	}

	var newDescription = "newDescription"

	for _, element := range setup {
		id, err := suite.keeper.AppendApplicationRole(suite.ctx, element.Creator, element.Id, element.Description)
		suite.Require().NoError(err)

		appRole, _ := suite.keeper.GetApplicationRole(suite.ctx, id)
		suite.Require().True(appRole.ApplicationRoleStates[len(appRole.ApplicationRoleStates)-1].Valid)

		suite.keeper.SetApplicationRoleDescription(suite.ctx, id, newDescription)
		appRole, _ = suite.keeper.GetApplicationRole(suite.ctx, id)

		suite.Require().Equal(appRole.ApplicationRoleStates[len(appRole.ApplicationRoleStates)-1].Description, newDescription)
	}
}

func (suite *AuthorizationTestSuite) TestAppendApplicationRoleState() {
	addr := []string{suite.addr1.String(), suite.addr2.String()}
	setup := []types.ApplicationRoleState{
		{
			Creator:           addr[0],
			Id:                "0",
			ApplicationRoleID: id1,
			Description:       app.Description,
			Valid:             true,
			TimeStamp:         time.Now().UTC().Format(time.RFC3339),
		},
		{
			Creator:           addr[0],
			Id:                "1",
			ApplicationRoleID: id2,
			Description:       app.Description,
			Valid:             true,
			TimeStamp:         time.Now().UTC().Format(time.RFC3339),
		},
	}

	for _, element := range setup {
		id := suite.keeper.AppendApplicationRoleState(suite.ctx, element.Creator, element.ApplicationRoleID, element.Description, element.Valid, element.TimeStamp)
		applicationRoleState := suite.keeper.GetApplicationRoleState(suite.ctx, id)
		suite.Require().Equal(applicationRoleState.Creator, element.Creator)
		suite.Require().Equal(applicationRoleState.Id, element.Id)
		suite.Require().Equal(applicationRoleState.ApplicationRoleID, element.ApplicationRoleID)
		suite.Require().Equal(applicationRoleState.Description, element.Description)
		suite.Require().Equal(applicationRoleState.Valid, element.Valid)
		suite.Require().Equal(applicationRoleState.TimeStamp, element.TimeStamp)
	}
}

func (suite *AuthorizationTestSuite) TestSetApplicationRoleState() {
	addr := []string{suite.addr1.String(), suite.addr2.String()}
	setup := []types.ApplicationRoleState{
		{
			Creator:           addr[0],
			Id:                "0",
			ApplicationRoleID: id1,
			Description:       app.Description,
			Valid:             true,
			TimeStamp:         time.Now().UTC().Format(time.RFC3339),
		},
		{
			Creator:           addr[0],
			Id:                "1",
			ApplicationRoleID: id2,
			Description:       app.Description,
			Valid:             true,
			TimeStamp:         time.Now().UTC().Format(time.RFC3339),
		},
	}

	for _, element := range setup {
		suite.keeper.SetApplicationRoleState(suite.ctx, element)
		applicationRoleState := suite.keeper.GetApplicationRoleState(suite.ctx, element.Id)
		suite.Require().True(suite.keeper.HasApplicationRoleState(suite.ctx, applicationRoleState.Id))
		suite.Require().Equal(applicationRoleState.Creator, element.Creator)
		suite.Require().Equal(applicationRoleState.Id, element.Id)
		suite.Require().Equal(applicationRoleState.ApplicationRoleID, element.ApplicationRoleID)
		suite.Require().Equal(applicationRoleState.Description, element.Description)
		suite.Require().Equal(applicationRoleState.Valid, element.Valid)
		suite.Require().Equal(applicationRoleState.TimeStamp, element.TimeStamp)
		var states = suite.keeper.GetAllApplicationRoleState(suite.ctx)
		suite.Require().Equal(states[len(states)-1], applicationRoleState)
	}
}

func (suite *AuthorizationTestSuite) TestAppendBlockchainAccount() {
	suite.SetupApplicationRoles()
	suite.SetupAdminAccount()

	addr := []string{suite.addr1.String(), suite.addr2.String()}
	setup := []types.MsgCreateBlockchainAccount{
		{
			Creator: addr[0],
			Id:      id1,
		},
		{
			Creator: addr[0],
			Id:      id2,
		},
	}

	for _, element := range setup {
		var lengthAppRoles = len(suite.keeper.GetAllBlockchainAccount(suite.ctx))

		_, err := suite.keeper.AppendBlockchainAccount(suite.ctx, addr[1], element.Id)
		suite.NoError(err)

		id, _ := suite.keeper.AppendBlockchainAccount(suite.ctx, element.Creator, element.Id)

		exists := suite.keeper.HasBlockchainAccount(suite.ctx, id)
		suite.Require().True(exists)

		out := suite.keeper.GetAllBlockchainAccount(suite.ctx)
		suite.Require().NotNil(out)
		suite.Require().Equal(element.Id, out[len(out)-1].Id)
		suite.Require().Equal(lengthAppRoles+1, len(out))
	}
}

func (suite *AuthorizationTestSuite) TestSetBlockchainAccount() {
	addr := []string{suite.addr1.String(), suite.addr2.String()}
	ids := []string{id1, id2}
	appRoleIds := [][]string{{types.AuthorizationReadApplicationRole}, {types.AuthorizationCreateAccount}}
	setup := []types.BlockchainAccount{
		{
			Creator: addr[0],
			Id:      id1,
			BlockchainAccountStates: []*types.BlockchainAccountState{
				{
					Creator:            addr[0],
					Id:                 id1,
					AccountID:          id1,
					TimeStamp:          time.Now().UTC().Format(time.RFC3339),
					ApplicationRoleIDs: []string{types.AuthorizationReadApplicationRole},
					Valid:              true,
				},
			},
		},
		{
			Creator: addr[1],
			Id:      id2,
			BlockchainAccountStates: []*types.BlockchainAccountState{
				{
					Creator:            addr[1],
					Id:                 id2,
					AccountID:          id2,
					TimeStamp:          time.Now().UTC().Format(time.RFC3339),
					ApplicationRoleIDs: []string{types.AuthorizationCreateAccount},
					Valid:              true,
				},
			},
		},
	}

	for index, element := range setup {
		suite.keeper.SetBlockchainAccount(suite.ctx, element)
		account := suite.keeper.GetBlockchainAccount(suite.ctx, ids[index])
		suite.Require().Equal(account.Id, ids[index])
		suite.Require().Equal(account.Creator, addr[index])

		suite.Require().Equal(account.BlockchainAccountStates[len(account.BlockchainAccountStates)-1].AccountID, ids[index])
		suite.Require().Equal(account.BlockchainAccountStates[len(account.BlockchainAccountStates)-1].Creator, addr[index])
		suite.Require().Equal(account.BlockchainAccountStates[len(account.BlockchainAccountStates)-1].ApplicationRoleIDs, appRoleIds[index])
		suite.Require().Equal(account.BlockchainAccountStates[len(account.BlockchainAccountStates)-1].Id, ids[index])
		suite.Require().Equal(account.BlockchainAccountStates[len(account.BlockchainAccountStates)-1].Valid, true)
		suite.Require().NotNil(account.BlockchainAccountStates[len(account.BlockchainAccountStates)-1].TimeStamp)
	}
}

func (suite *AuthorizationTestSuite) TestGrantApplicationPermission() {
	suite.SetupApplicationRoles()
	suite.SetupAdminAccount()
	suite.SetupAccount()

	addr := []string{suite.addr1.String(), suite.addr2.String()}
	setupAuthorized := []types.MsgGrantAppRoleToBlockchainAccount{
		{
			Creator: addr[0],
			User:    addr[1],
			RoleID:  types.AuthorizationCreateAccount,
		},
		{
			Creator: addr[0],
			User:    addr[1],
			RoleID:  types.AuthorizationDeactivateApplicationRole,
		},
	}

	setupUnauthorized := []types.MsgGrantAppRoleToBlockchainAccount{
		{
			Creator: addr[1],
			User:    addr[1],
			RoleID:  types.AuthorizationCreateAccount,
		},
		{
			Creator: addr[0],
			User:    addr[0],
			RoleID:  types.AuthorizationCreateAccount,
		},
	}

	for _, element := range setupAuthorized {
		err := suite.keeper.GrantApplicationPermission(suite.ctx, element.User, element.RoleID, addr[1])
		suite.Require().NoError(err)

		suite.keeper.GrantApplicationPermission(suite.ctx, element.User, element.RoleID, element.Creator)

		exists := suite.keeper.HasBlockchainAccount(suite.ctx, addr[1])
		suite.Require().True(exists)
		account := suite.keeper.GetBlockchainAccount(suite.ctx, addr[1])
		states := account.BlockchainAccountStates[len(account.BlockchainAccountStates)-1]
		appIDs := states.ApplicationRoleIDs[len(states.ApplicationRoleIDs)-1]
		suite.Require().Equal(appIDs, element.RoleID)
	}

	for _, element := range setupUnauthorized {
		err := suite.keeper.GrantApplicationPermission(suite.ctx, element.User, element.RoleID, element.Creator)
		suite.Error(err, sdkErrors.ErrInvalidRequest)
	}
}

func (suite *AuthorizationTestSuite) TestRevokeApplicationPermission() {
	suite.SetupApplicationRoles()
	suite.SetupAdminAccount()
	suite.SetupAccount()

	addr := []string{suite.addr1.String(), suite.addr2.String()}
	setupAuthorized := []types.MsgRevokeAppRoleFromBlockchainAccount{
		{
			Creator: addr[0],
			Account: addr[1],
			RoleID:  types.AuthorizationCreateAccount,
		},
		{
			Creator: addr[0],
			Account: addr[1],
			RoleID:  types.AuthorizationDeactivateApplicationRole,
		},
	}
	setupUnauthorized := []types.MsgRevokeAppRoleFromBlockchainAccount{
		{
			Creator: addr[1],
			Account: addr[1],
			RoleID:  types.AuthorizationCreateAccount,
		},
		{
			Creator: addr[0],
			Account: addr[1],
			RoleID:  roleID,
		},
		{
			Creator: addr[0],
			Account: addr[1],
			RoleID:  types.AuthorizationCreateAccount,
		},
	}

	for _, element := range setupAuthorized {
		suite.keeper.GrantApplicationPermission(suite.ctx, element.Account, element.RoleID, element.Creator)
		exists := suite.keeper.HasBlockchainAccount(suite.ctx, addr[1])
		suite.Require().True(exists)

		err := suite.keeper.RevokeApplicationPermission(suite.ctx, element.Account, element.RoleID, addr[1])
		suite.Require().NoError(err)

		suite.keeper.RevokeApplicationPermission(suite.ctx, element.Account, element.RoleID, element.Creator)
		account := suite.keeper.GetBlockchainAccount(suite.ctx, addr[1])
		states := account.BlockchainAccountStates[len(account.BlockchainAccountStates)-1]
		appIDs := states.ApplicationRoleIDs
		suite.Require().NotContains(appIDs, element.RoleID)
	}

	for _, element := range setupUnauthorized {
		err := suite.keeper.RevokeApplicationPermission(suite.ctx, element.Account, element.RoleID, element.Creator)
		suite.NoError(err)
	}
}

func (suite *AuthorizationTestSuite) TestAppendBlockchainAccountState() {
	addr := []string{suite.addr1.String(), suite.addr2.String()}
	setup := []types.BlockchainAccountState{
		{
			Creator:            addr[0],
			Id:                 "0",
			AccountID:          id1,
			ApplicationRoleIDs: []string{types.AuthorizationReadApplicationRole},
			Valid:              true,
			TimeStamp:          time.Now().UTC().Format(time.RFC3339),
		},
		{
			Creator:            addr[0],
			Id:                 "1",
			AccountID:          id2,
			ApplicationRoleIDs: []string{types.AuthorizationCreateAccount},
			Valid:              true,
			TimeStamp:          time.Now().UTC().Format(time.RFC3339),
		},
	}

	for _, element := range setup {
		id := suite.keeper.AppendBlockchainAccountState(suite.ctx, element.Creator, element.AccountID, element.TimeStamp, element.ApplicationRoleIDs, element.Valid)
		blockchainAccountState := suite.keeper.GetBlockchainAccountState(suite.ctx, id)
		suite.Require().Equal(blockchainAccountState.Creator, element.Creator)
		suite.Require().Equal(blockchainAccountState.Id, element.Id)
		suite.Require().Equal(blockchainAccountState.AccountID, element.AccountID)
		suite.Require().Equal(blockchainAccountState.ApplicationRoleIDs, element.ApplicationRoleIDs)
		suite.Require().Equal(blockchainAccountState.Valid, element.Valid)
		suite.Require().Equal(blockchainAccountState.TimeStamp, element.TimeStamp)

		owner := suite.keeper.GetBlockchainAccountStateOwner(suite.ctx, id)
		suite.Require().Equal(element.Creator, owner)
	}
}

func (suite *AuthorizationTestSuite) TestSetBlockchainAccountState() {
	addr := []string{suite.addr1.String(), suite.addr2.String()}
	setup := []types.BlockchainAccountState{
		{
			Creator:            addr[0],
			Id:                 "0",
			AccountID:          id1,
			ApplicationRoleIDs: []string{types.AuthorizationReadApplicationRole},
			Valid:              true,
			TimeStamp:          time.Now().UTC().Format(time.RFC3339),
		},
		{
			Creator:            addr[0],
			Id:                 "1",
			AccountID:          id2,
			ApplicationRoleIDs: []string{types.AuthorizationCreateAccount},
			Valid:              true,
			TimeStamp:          time.Now().UTC().Format(time.RFC3339),
		},
	}

	for _, element := range setup {
		suite.keeper.SetBlockchainAccountState(suite.ctx, element)
		blockchainAccountState := suite.keeper.GetBlockchainAccountState(suite.ctx, element.Id)
		suite.Require().True(suite.keeper.HasBlockchainAccountState(suite.ctx, blockchainAccountState.Id))
		owner := suite.keeper.GetBlockchainAccountStateOwner(suite.ctx, blockchainAccountState.Id)
		suite.Require().Equal(element.Creator, owner)
		suite.Require().Equal(element.Creator, blockchainAccountState.Creator)
		suite.Require().Equal(blockchainAccountState.Id, element.Id)
		suite.Require().Equal(blockchainAccountState.AccountID, element.AccountID)
		suite.Require().Equal(blockchainAccountState.ApplicationRoleIDs, element.ApplicationRoleIDs)
		suite.Require().Equal(blockchainAccountState.Valid, element.Valid)
		suite.Require().Equal(blockchainAccountState.TimeStamp, element.TimeStamp)
		var states = suite.keeper.GetAllBlockchainAccountState(suite.ctx)
		suite.Require().Equal(states[len(states)-1], blockchainAccountState)
	}
}
