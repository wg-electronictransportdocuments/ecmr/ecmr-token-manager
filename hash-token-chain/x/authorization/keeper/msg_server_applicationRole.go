// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"
	"fmt"

	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkErrors "github.com/cosmos/cosmos-sdk/types/errors"
	"github.com/google/uuid"

	"gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/TokenManager/x/authorization/types"
)

func (k Keeper) CreateApplicationRole(goCtx context.Context, msg *types.MsgCreateApplicationRole) (*types.MsgCreateApplicationRoleResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	// Permission check
	check, err := k.HasRole(ctx, msg.Creator, types.AuthorizationCreateApplicationRole)
	if err != nil || !check {
		return nil, sdkErrors.Wrap(types.ErrNoPermission, types.AuthorizationCreateApplicationRole)
	}

	if k.HasApplicationRole(ctx, msg.Id) {
		return nil, sdkErrors.Wrap(types.ErrApplicationRoleAlreadyExists, msg.Id)
	}

	id, err := k.AppendApplicationRole(
		ctx,
		msg.Creator,
		msg.Id,
		msg.Description,
	)

	return &types.MsgCreateApplicationRoleResponse{
		Id: id,
	}, err
}

func (k Keeper) UpdateApplicationRole(goCtx context.Context, msg *types.MsgUpdateApplicationRole) (*types.MsgUpdateApplicationRoleResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	// Permission check
	check, err := k.HasRole(ctx, msg.Creator, types.AuthorizationCreateApplicationRole)
	if err != nil || !check {
		return nil, sdkErrors.Wrap(types.ErrNoPermission, types.AuthorizationCreateApplicationRole)
	}

	role, err := k.GetApplicationRole(ctx, msg.Id)
	if err != nil {
		return nil, sdkErrors.Wrap(sdkErrors.ErrKeyNotFound, fmt.Sprintf("key %s doesn't exist", msg.Id))
	}

	role.ApplicationRoleStates = append(role.ApplicationRoleStates, &types.ApplicationRoleState{
		Creator:           msg.Creator,
		Id:                uuid.New().String(),
		ApplicationRoleID: msg.Id,
		Description:       msg.Description,
		Valid:             role.ApplicationRoleStates[len(role.ApplicationRoleStates)-1].Valid,
		TimeStamp:         GetTimestamp(),
	})

	k.SetApplicationRole(ctx, role)
	return &types.MsgUpdateApplicationRoleResponse{}, nil
}

func (k Keeper) DeactivateApplicationRole(goCtx context.Context, msg *types.MsgDeactivateApplicationRole) (*types.MsgDeactivateApplicationRoleResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	// Permission check
	check, err := k.HasRole(ctx, msg.Creator, types.AuthorizationDeactivateApplicationRole)
	if err != nil || !check {
		return nil, sdkErrors.Wrap(types.ErrNoPermission, types.AuthorizationDeactivateApplicationRole)
	}

	if !k.HasApplicationRole(ctx, msg.Id) {
		return nil, sdkErrors.Wrap(sdkErrors.ErrKeyNotFound, fmt.Sprintf("key %s doesn't exist", msg.Id))
	}

	err = k.SetApplicationRoleInvalid(ctx, msg.Creator, msg.Id)

	return &types.MsgDeactivateApplicationRoleResponse{}, err
}
