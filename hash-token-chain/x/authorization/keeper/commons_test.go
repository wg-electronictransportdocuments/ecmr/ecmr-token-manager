// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper_test

import (
	"testing"
	"time"

	"github.com/cosmos/cosmos-sdk/baseapp"
	"github.com/cosmos/cosmos-sdk/codec"
	codecTypes "github.com/cosmos/cosmos-sdk/codec/types"
	"github.com/cosmos/cosmos-sdk/store"
	storeTypes "github.com/cosmos/cosmos-sdk/store/types"
	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"github.com/tendermint/tendermint/libs/log"
	tendermintTypes "github.com/tendermint/tendermint/proto/tendermint/types"
	tendermintTmDb "github.com/tendermint/tm-db"

	"gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/TokenManager/app"
	authorizationKeeper "gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/TokenManager/x/authorization/keeper"
	"gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/TokenManager/x/authorization/types"
)

type AuthorizationTestSuite struct {
	suite.Suite
	keeper      authorizationKeeper.Keeper
	app         *app.App
	ctx         sdk.Context
	queryClient types.QueryClient
	addr1       sdk.AccAddress
	addr2       sdk.AccAddress
}

const (
	appRoleStateID  = "5700c184-aa20-4b7b-9e88-ec958f63df48"
	appRoleID       = "AUTHORIZATION-MODULE_BLOCK-ACCOUNT"
	blockAccStateID = "fb4594c8-e0ca-4190-ab72-0a0ed7d46de8"
)

func (suite *AuthorizationTestSuite) SetupTest() {
	testableApp, ctx := app.CreateTestInput()

	suite.keeper = testableApp.AuthorizationKeeper
	suite.app = testableApp
	suite.ctx = ctx

	creator1, _ := sdk.AccAddressFromBech32(app.CreatorA)
	creator2, _ := sdk.AccAddressFromBech32("cosmos13ewe06vu6255fqrmhz2gqyhqjvh6rxw837sc30")

	suite.addr1 = creator1
	suite.addr2 = creator2

	querier := authorizationKeeper.Querier{Keeper: testableApp.AuthorizationKeeper}
	queryHelper := baseapp.NewQueryServerTestHelper(ctx, testableApp.InterfaceRegistry())
	types.RegisterQueryServer(queryHelper, querier)

	suite.queryClient = types.NewQueryClient(queryHelper)
}

func (suite *AuthorizationTestSuite) SetupApplicationRoles() {
	roles := []string{
		types.AuthorizationCreateAccount,
		types.AuthorizationGrantRole,
		types.AuthorizationRevokeRole,
		types.AuthorizationBlockAccount,
		types.AuthorizationCreateApplicationRole,
		types.AuthorizationUpdateApplicationRole,
		types.AuthorizationReadApplicationRole,
		types.AuthorizationDeactivateApplicationRole,
		types.AuthorizationFetchAllApplicationRole,
		types.AuthorizationFetchApplicationRole,
		types.AuthorizationFetchAllBlockchainAccount,
		types.AuthorizationFetchBlockchainAccount}

	for _, element := range roles {
		suite.keeper.AppendApplicationRole(suite.ctx, suite.addr1.String(), element, app.Description)
	}
}

func (suite *AuthorizationTestSuite) SetupAdminAccount() {

	var blockchainAccountStatesAdmin []*types.BlockchainAccountState
	applicationRoleIDsAdmin := []string{
		types.AuthorizationCreateAccount,
		types.AuthorizationGrantRole,
		types.AuthorizationRevokeRole,
		types.AuthorizationBlockAccount,
		types.AuthorizationCreateApplicationRole,
		types.AuthorizationUpdateApplicationRole,
		types.AuthorizationReadApplicationRole,
		types.AuthorizationDeactivateApplicationRole,
		types.AuthorizationFetchAllApplicationRole,
		types.AuthorizationFetchApplicationRole,
		types.AuthorizationFetchAllBlockchainAccount,
		types.AuthorizationFetchBlockchainAccount}

	var BlockchainAccountStateAdmin = types.BlockchainAccountState{
		Creator:            suite.addr1.String(),
		Id:                 uuid.New().String(),
		ApplicationRoleIDs: applicationRoleIDsAdmin,
		AccountID:          suite.addr1.String(),
		Valid:              true,
		TimeStamp:          time.Now().UTC().Format(time.RFC3339),
	}

	blockchainAccountStatesAdmin = append(blockchainAccountStatesAdmin, &BlockchainAccountStateAdmin)

	var adminAccount = types.BlockchainAccount{
		Creator:                 suite.addr1.String(),
		Id:                      suite.addr1.String(),
		BlockchainAccountStates: blockchainAccountStatesAdmin,
	}

	suite.keeper.AppendBlockchainAccount(suite.ctx, adminAccount.Creator, adminAccount.Id)
	suite.keeper.SetBlockchainAccount(suite.ctx, adminAccount)
}

func (suite *AuthorizationTestSuite) SetupAccount() {

	var blockchainAccountStatesAdmin []*types.BlockchainAccountState

	var BlockchainAccountStateAdmin = types.BlockchainAccountState{
		Creator:            suite.addr2.String(),
		Id:                 uuid.New().String(),
		ApplicationRoleIDs: []string{},
		AccountID:          suite.addr2.String(),
		Valid:              true,
		TimeStamp:          time.Now().UTC().Format(time.RFC3339),
	}

	blockchainAccountStatesAdmin = append(blockchainAccountStatesAdmin, &BlockchainAccountStateAdmin)

	var account = types.BlockchainAccount{
		Creator:                 suite.addr2.String(),
		Id:                      suite.addr2.String(),
		BlockchainAccountStates: blockchainAccountStatesAdmin,
	}

	suite.keeper.SetBlockchainAccount(suite.ctx, account)
}

func (suite *AuthorizationTestSuite) SetupApplicationRoleState() {

	var state = types.ApplicationRoleState{
		Creator:           suite.addr1.String(),
		Id:                appRoleStateID,
		ApplicationRoleID: appRoleID,
		Description:       app.Description,
		Valid:             true,
		TimeStamp:         app.Timestamp,
	}

	suite.keeper.SetApplicationRoleState(suite.ctx, state)
}

func (suite *AuthorizationTestSuite) SetupBlockchainAccountState() {

	var state = types.BlockchainAccountState{
		Creator:            suite.addr1.String(),
		Id:                 blockAccStateID,
		AccountID:          app.CreatorA,
		ApplicationRoleIDs: nil,
		Valid:              true,
		TimeStamp:          app.Timestamp,
	}

	suite.keeper.SetBlockchainAccountState(suite.ctx, state)
}

func setupKeeper(t testing.TB) (*authorizationKeeper.Keeper, sdk.Context) {
	storeKey := sdk.NewKVStoreKey(types.StoreKey)
	memStoreKey := storeTypes.NewMemoryStoreKey(types.MemStoreKey)

	db := tendermintTmDb.NewMemDB()
	stateStore := store.NewCommitMultiStore(db)
	stateStore.MountStoreWithDB(storeKey, sdk.StoreTypeIAVL, db)
	stateStore.MountStoreWithDB(memStoreKey, sdk.StoreTypeMemory, nil)
	require.NoError(t, stateStore.LoadLatestVersion())

	registry := codecTypes.NewInterfaceRegistry()
	keeper := authorizationKeeper.NewKeeper(
		codec.NewProtoCodec(registry),
		storeKey,
		memStoreKey,
	)

	roleIds := []string{
		types.AuthorizationCreateAccount,
		types.AuthorizationGrantRole,
		types.AuthorizationRevokeRole,
		types.AuthorizationBlockAccount,
		types.AuthorizationCreateApplicationRole,
		types.AuthorizationUpdateApplicationRole,
		types.AuthorizationReadApplicationRole,
		types.AuthorizationDeactivateApplicationRole,
		types.AuthorizationFetchAllApplicationRole,
		types.AuthorizationFetchApplicationRole,
		types.AuthorizationFetchAllBlockchainAccount,
		types.AuthorizationFetchBlockchainAccount,
	}
	ctx := sdk.NewContext(stateStore, tendermintTypes.Header{}, false, log.NewNopLogger())
	for _, v := range roleIds {
		keeper.AppendApplicationRole(ctx, "", v, "")
	}
	keeper.SetBlockchainAccount(ctx, types.BlockchainAccount{
		Creator: app.CreatorA,
		Id:      app.CreatorA,
		BlockchainAccountStates: []*types.BlockchainAccountState{{
			Creator:            app.CreatorA,
			Id:                 uuid.New().String(),
			AccountID:          app.CreatorA,
			TimeStamp:          time.Now().UTC().Format(time.RFC3339),
			ApplicationRoleIDs: roleIds,
			Valid:              true,
		}},
	})
	keeper.SetConfiguration(ctx, types.Configuration{Creator: "", Id: 0, PermissionCheck: true})

	return keeper, ctx
}
func TestAuthorizationTestSuite(t *testing.T) {
	suite.Run(t, new(AuthorizationTestSuite))
}
