// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	"github.com/cosmos/cosmos-sdk/store/prefix"
	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/cosmos/cosmos-sdk/types/query"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/TokenManager/x/authorization/types"
)

func (k Keeper) ApplicationRoleAll(c context.Context, req *types.QueryAllApplicationRoleRequest) (*types.QueryAllApplicationRoleResponse, error) {
	if req == nil {
		return nil, status.Error(codes.InvalidArgument, "invalid request")
	}

	var applicationRoles []*types.ApplicationRole
	ctx := sdk.UnwrapSDKContext(c)

	store := ctx.KVStore(k.storeKey)
	applicationRoleStore := prefix.NewStore(store, types.KeyPrefix(types.ApplicationRoleKey))

	pageRes, err := query.Paginate(applicationRoleStore, req.Pagination, func(key []byte, value []byte) error {
		var applicationRole types.ApplicationRole
		if err := k.cdc.Unmarshal(value, &applicationRole); err != nil {
			return err
		}

		applicationRoles = append(applicationRoles, &applicationRole)
		return nil
	})

	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &types.QueryAllApplicationRoleResponse{ApplicationRole: applicationRoles, Pagination: pageRes}, nil
}

func (k Keeper) ApplicationRole(c context.Context, req *types.QueryGetApplicationRoleRequest) (*types.QueryGetApplicationRoleResponse, error) {
	ctx := sdk.UnwrapSDKContext(c)
	applicationRole, err := k.GetApplicationRole(ctx, req.Id)

	return &types.QueryGetApplicationRoleResponse{ApplicationRole: &applicationRole}, err
}
