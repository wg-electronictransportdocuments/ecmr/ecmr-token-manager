// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"
	"fmt"

	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkErrors "github.com/cosmos/cosmos-sdk/types/errors"

	"gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/TokenManager/x/authorization/types"
)

func (k Keeper) UpdateConfiguration(goCtx context.Context, msg *types.MsgUpdateConfiguration) (*types.MsgUpdateConfigurationResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	// Permission check
	check, err := k.HasRole(ctx, msg.Creator, types.AuthorizationUpdateConfiguration)
	if err != nil || !check {
		return nil, sdkErrors.Wrap(types.ErrNoPermission, types.AuthorizationUpdateConfiguration)
	}

	// Checks that the element exists
	if !k.HasConfiguration(ctx, msg.Id) {
		return nil, sdkErrors.Wrap(sdkErrors.ErrKeyNotFound, fmt.Sprintf("key %d doesn't exist", msg.Id))
	}

	var configuration = types.Configuration{
		Creator:         msg.Creator,
		Id:              msg.Id,
		PermissionCheck: msg.PermissionCheck,
	}
	k.SetConfiguration(ctx, configuration)

	return &types.MsgUpdateConfigurationResponse{}, nil
}
