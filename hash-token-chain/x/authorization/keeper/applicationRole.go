// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"strconv"
	"time"

	"github.com/cosmos/cosmos-sdk/store/prefix"
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkErrors "github.com/cosmos/cosmos-sdk/types/errors"
	"github.com/google/uuid"

	"gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/TokenManager/x/authorization/types"
)

// GetApplicationRoleCount gets the total number of applicationRole
func (k Keeper) GetApplicationRoleCount(ctx sdk.Context) int64 {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.ApplicationRoleCountKey))
	byteKey := types.KeyPrefix(types.ApplicationRoleCountKey)
	bz := store.Get(byteKey)

	// Count doesn't exist: no element
	if bz == nil {
		return 0
	}

	// Parse bytes
	count, err := strconv.ParseInt(string(bz), 10, 64)
	if err != nil {
		// Panic because the count should be always formattable to int64
		panic("cannot decode count")
	}

	return count
}

// SetApplicationRoleCount sets the total number of applicationRole
func (k Keeper) SetApplicationRoleCount(ctx sdk.Context, count int64) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.ApplicationRoleCountKey))
	byteKey := types.KeyPrefix(types.ApplicationRoleCountKey)
	bz := []byte(strconv.FormatInt(count, 10))
	store.Set(byteKey, bz)
}

// AppendApplicationRole appends a applicationRole in the store with a new id and updates the count
func (k Keeper) AppendApplicationRole(
	ctx sdk.Context,
	creator string,
	id string,
	description string,
) (string, error) {
	// Create new state
	var applicationRoleState = types.ApplicationRoleState{
		Creator:           creator,
		Id:                uuid.New().String(),
		ApplicationRoleID: id,
		Description:       description,
		Valid:             true,
		TimeStamp:         time.Now().UTC().Format(time.RFC3339),
	}

	// Append new state
	var applicationRoleStates []*types.ApplicationRoleState
	applicationRoleStates = append(applicationRoleStates, &applicationRoleState)
	var applicationRole = types.ApplicationRole{
		Creator:               creator,
		Id:                    id,
		ApplicationRoleStates: applicationRoleStates,
	}

	// Access KVStore and append
	k.SetApplicationRole(ctx, applicationRole)

	// Update applicationRole count
	count := k.GetApplicationRoleCount(ctx)
	k.SetApplicationRoleCount(ctx, count+1)

	return applicationRole.Id, nil
}

// SetApplicationRole sets a specific applicationRole in the store
func (k Keeper) SetApplicationRole(ctx sdk.Context, applicationRole types.ApplicationRole) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.ApplicationRoleKey))
	b := k.cdc.MustMarshal(&applicationRole)
	store.Set(types.KeyPrefix(types.ApplicationRoleKey+applicationRole.Id), b)
}

// GetApplicationRole returns a applicationRole from its id
func (k Keeper) GetApplicationRole(ctx sdk.Context, key string) (types.ApplicationRole, error) {
	if !k.HasApplicationRole(ctx, key) {
		return types.ApplicationRole{}, sdkErrors.ErrKeyNotFound
	}
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.ApplicationRoleKey))
	var applicationRole types.ApplicationRole
	k.cdc.MustUnmarshal(store.Get(types.KeyPrefix(types.ApplicationRoleKey+key)), &applicationRole)
	return applicationRole, nil
}

// GetLatestApplicationRoleState returns the latest state of an applicationRole
func (k Keeper) GetLatestApplicationRoleState(ctx sdk.Context, key string) (types.ApplicationRoleState, error) {
	role, err := k.GetApplicationRole(ctx, key)
	if err != nil {
		return types.ApplicationRoleState{}, err
	}
	if len(role.ApplicationRoleStates) > 0 {
		return *role.ApplicationRoleStates[len(role.ApplicationRoleStates)-1], nil
	} else {
		return types.ApplicationRoleState{}, sdkErrors.Error{}
	}
}

// AppendNewApplicationRoleState appends a new state to an existing applicationRole
func (k Keeper) AppendNewApplicationRoleState(ctx sdk.Context, key string, state types.ApplicationRoleState) error {
	state.TimeStamp = time.Now().UTC().Format(time.RFC3339)
	state.Id = uuid.New().String()

	role, err := k.GetApplicationRole(ctx, key)
	if err != nil {
		return err
	}
	role.ApplicationRoleStates[len(role.ApplicationRoleStates)-1].Valid = false
	role.ApplicationRoleStates = append(role.ApplicationRoleStates, &state)
	k.SetApplicationRole(ctx, role)
	return nil
}

// SetApplicationRoleDescription updates the description if an applicationRole
func (k Keeper) SetApplicationRoleDescription(ctx sdk.Context, key string, description string) error {
	state, err := k.GetLatestApplicationRoleState(ctx, key)
	if err != nil {
		return err
	}
	state.Description = description
	return k.AppendNewApplicationRoleState(ctx, key, state)
}

// SetApplicationRoleInvalid invalidates an application role
func (k Keeper) SetApplicationRoleInvalid(ctx sdk.Context, creator string, key string) error {
	state, err := k.GetLatestApplicationRoleState(ctx, key)
	if err != nil {
		return err
	}
	state.Valid = false
	state.Creator = creator

	return k.AppendNewApplicationRoleState(ctx, key, state)
}

// HasApplicationRole checks if an applicationRole exists in the store
func (k Keeper) HasApplicationRole(ctx sdk.Context, id string) bool {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.ApplicationRoleKey))
	return store.Has(types.KeyPrefix(types.ApplicationRoleKey + id))
}

// GetApplicationRoleOwner returns the creator of an applicationRole
func (k Keeper) GetApplicationRoleOwner(ctx sdk.Context, key string) string {
	var applicationRoleOwner, _ = k.GetApplicationRole(ctx, key)
	return applicationRoleOwner.Creator
}

// GetAllApplicationRole returns all applicationRole
func (k Keeper) GetAllApplicationRole(ctx sdk.Context) (msgs []types.ApplicationRole) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.ApplicationRoleKey))
	iterator := sdk.KVStorePrefixIterator(store, types.KeyPrefix(types.ApplicationRoleKey))

	defer iterator.Close()

	for ; iterator.Valid(); iterator.Next() {
		var msg types.ApplicationRole
		k.cdc.MustUnmarshal(iterator.Value(), &msg)
		msgs = append(msgs, msg)
	}

	return
}
