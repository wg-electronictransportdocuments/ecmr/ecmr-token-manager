// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkErrors "github.com/cosmos/cosmos-sdk/types/errors"
	"github.com/cosmos/cosmos-sdk/types/query"

	"gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/TokenManager/x/authorization/types"
	authorizationTypes "gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/TokenManager/x/authorization/types"
)

func (k Keeper) FetchAllApplicationRole(goCtx context.Context, msg *types.MsgFetchAllApplicationRole) (*types.MsgFetchAllApplicationRoleResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)
	if k.GetConfiguration(ctx, 0).PermissionCheck {
		check, err := k.HasRole(ctx, msg.Creator, authorizationTypes.AuthorizationFetchAllApplicationRole)
		if err != nil || !check {
			return &types.MsgFetchAllApplicationRoleResponse{}, sdkErrors.Wrap(authorizationTypes.ErrNoPermission, authorizationTypes.AuthorizationFetchAllApplicationRole)
		}
	}
	res, queryErr := k.ApplicationRoleAll(goCtx, &types.QueryAllApplicationRoleRequest{Pagination: &query.PageRequest{}})
	if queryErr != nil {
		return &types.MsgFetchAllApplicationRoleResponse{}, sdkErrors.Wrap(authorizationTypes.ErrNoPermission, authorizationTypes.AuthorizationFetchAllApplicationRole)
	}
	return &types.MsgFetchAllApplicationRoleResponse{Role: res.ApplicationRole}, nil
}
