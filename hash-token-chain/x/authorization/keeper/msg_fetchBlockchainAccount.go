// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkErrors "github.com/cosmos/cosmos-sdk/types/errors"

	"gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/TokenManager/x/authorization/types"
	authorizationTypes "gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/TokenManager/x/authorization/types"
)

func (k Keeper) FetchBlockchainAccount(goCtx context.Context, msg *types.MsgFetchBlockchainAccount) (*types.MsgFetchBlockchainAccountResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)
	if k.GetConfiguration(ctx, 0).PermissionCheck {
		check, err := k.HasRole(ctx, msg.Creator, authorizationTypes.AuthorizationFetchBlockchainAccount)
		if err != nil || !check {
			return &types.MsgFetchBlockchainAccountResponse{}, sdkErrors.Wrap(authorizationTypes.ErrNoPermission, authorizationTypes.AuthorizationFetchBlockchainAccount)
		}
	}
	res, _ := k.BlockchainAccount(goCtx, &types.QueryGetBlockchainAccountRequest{Id: msg.Id})
	return &types.MsgFetchBlockchainAccountResponse{Account: res.BlockchainAccount}, nil
}
