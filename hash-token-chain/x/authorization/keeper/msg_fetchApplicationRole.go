// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkErrors "github.com/cosmos/cosmos-sdk/types/errors"

	"gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/TokenManager/x/authorization/types"
	authorizationTypes "gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/TokenManager/x/authorization/types"
)

func (k Keeper) FetchApplicationRole(goCtx context.Context, msg *types.MsgFetchApplicationRole) (*types.MsgFetchApplicationRoleResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)
	if k.GetConfiguration(ctx, 0).PermissionCheck {
		check, err := k.HasRole(ctx, msg.Creator, authorizationTypes.AuthorizationFetchApplicationRole)
		if err != nil || !check {
			return &types.MsgFetchApplicationRoleResponse{}, sdkErrors.Wrap(authorizationTypes.ErrNoPermission, authorizationTypes.AuthorizationFetchApplicationRole)
		}
	}
	res, _ := k.ApplicationRole(goCtx, &types.QueryGetApplicationRoleRequest{Id: msg.Id})
	return &types.MsgFetchApplicationRoleResponse{Role: res.ApplicationRole}, nil
}
