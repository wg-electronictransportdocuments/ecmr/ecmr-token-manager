// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package cli

import (
	"fmt"

	"github.com/cosmos/cosmos-sdk/client"
	"github.com/spf13/cobra"

	"gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/TokenManager/x/authorization/types"
)

// GetTxCmd returns the transaction commands for this module
func GetTxCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:                        types.ModuleName,
		Short:                      fmt.Sprintf("%s transactions subcommands", types.ModuleName),
		DisableFlagParsing:         true,
		SuggestionsMinimumDistance: 2,
		RunE:                       client.ValidateCmd,
	}

	// this line is used by starport scaffolding # 1

	/*
		cmd.AddCommand(CmdCreateBlockchainAccount())
		cmd.AddCommand(CmdDeactivateBlockchainAccount())
		cmd.AddCommand(CmdGrantApplicationRoleToBlockchainAccount())
		cmd.AddCommand(CmdRevokeApplicationRoleFromBlockchainAccount())

		cmd.AddCommand(CmdCreateApplicationRole())
		cmd.AddCommand(CmdUpdateApplicationRole())
		cmd.AddCommand(CmdDeleteApplicationRole())
		cmd.AddCommand(CmdDeactivateApplicationRole())

		cmd.AddCommand(CmdUpdateConfiguration())
	*/

	return cmd
}
