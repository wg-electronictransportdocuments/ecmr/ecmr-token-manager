// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package types

// DONTCOVER

import (
	sdkErrors "github.com/cosmos/cosmos-sdk/types/errors"
)

// x/tokenManager module sentinel errors

var (
	ErrNoMapping = sdkErrors.Register(ModuleName, 1, "document token mapping does not exist")
)
