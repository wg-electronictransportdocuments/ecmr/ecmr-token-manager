// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package types

import (
	"fmt"
)

// DefaultIndex is the default capability global index
const DefaultIndex uint64 = 1

// DefaultGenesis returns the default Capability genesis state
func DefaultGenesis() *GenesisState {
	return &GenesisState{
		// this line is used by starport scaffolding # genesis/types/default
		TokenCopiesList:         []*TokenCopies{},
		DocumentTokenMapperList: []*DocumentTokenMapper{},
	}
}

// Validate performs basic genesis state validation returning an error upon any
// failure.
func (gs GenesisState) Validate() error {
	// this line is used by starport scaffolding # genesis/types/validate
	// Check for duplicated index in tokenCopies
	tokenCopiesIndexMap := make(map[string]bool)

	for _, elem := range gs.TokenCopiesList {
		if _, ok := tokenCopiesIndexMap[elem.Index]; ok {
			return fmt.Errorf("duplicated index for tokenCopies")
		}
		tokenCopiesIndexMap[elem.Index] = true
	}
	// Check for duplicated index in documentTokenMapper
	documentTokenMapperIndexMap := make(map[string]bool)

	for _, elem := range gs.DocumentTokenMapperList {
		if _, ok := documentTokenMapperIndexMap[elem.DocumentId]; ok {
			return fmt.Errorf("duplicated index for documentTokenMapper")
		}
		documentTokenMapperIndexMap[elem.DocumentId] = true
	}

	return nil
}
