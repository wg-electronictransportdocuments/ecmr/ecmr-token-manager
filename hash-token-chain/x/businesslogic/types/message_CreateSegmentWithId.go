// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package types

import (
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkErrors "github.com/cosmos/cosmos-sdk/types/errors"
)

var _ sdk.Msg = &MsgCreateSegmentWithId{}

func NewMsgCreateSegmentWithId(creator string, id string, name string, info string, walletId string) *MsgCreateSegmentWithId {
	return &MsgCreateSegmentWithId{
		Creator:  creator,
		Id:       id,
		Name:     name,
		Info:     info,
		WalletId: walletId,
	}
}

func (msg *MsgCreateSegmentWithId) Route() string {
	return RouterKey
}

func (msg *MsgCreateSegmentWithId) Type() string {
	return "CreateSegmentWithId"
}

func (msg *MsgCreateSegmentWithId) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgCreateSegmentWithId) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgCreateSegmentWithId) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkErrors.Wrapf(sdkErrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}
