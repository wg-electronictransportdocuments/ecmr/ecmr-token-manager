// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkErrors "github.com/cosmos/cosmos-sdk/types/errors"

	authorizationTypes "gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/TokenManager/x/authorization/types"
	"gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/TokenManager/x/businesslogic/types"
	tokenwalletTypes "gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/TokenManager/x/tokenwallet/types"
)

func (k msgServer) CreateWalletWithId(goCtx context.Context, msg *types.MsgCreateWalletWithId) (*tokenwalletTypes.MsgIdResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	// Permission check
	check, Role := k.authorizationKeeper.HasRole(ctx, msg.Creator, authorizationTypes.WalletCreateWallet)
	if Role != nil || !check {
		return &tokenwalletTypes.MsgIdResponse{}, sdkErrors.Wrap(authorizationTypes.ErrNoPermission, authorizationTypes.WalletCreateWallet)
	}

	res, errWallet := k.walletKeeper.CreateWalletWithId(goCtx, &tokenwalletTypes.MsgCreateWalletWithId{
		Creator: msg.Creator,
		Name:    msg.Name,
		Id:      msg.Id,
	})

	if errWallet != nil {
		return &tokenwalletTypes.MsgIdResponse{}, errWallet
	}

	return res, nil
}
