// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkErrors "github.com/cosmos/cosmos-sdk/types/errors"

	authorizationTypes "gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/TokenManager/x/authorization/types"
	"gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/TokenManager/x/businesslogic/types"
	tokenwalletTypes "gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/TokenManager/x/tokenwallet/types"
)

func (k msgServer) UpdateSegment(goCtx context.Context, msg *types.MsgUpdateSegment) (*tokenwalletTypes.MsgEmptyResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	// Permission check
	check, errRole := k.authorizationKeeper.HasRole(ctx, msg.Creator, authorizationTypes.WalletUpdateSegment)
	if errRole != nil || !check {
		return &tokenwalletTypes.MsgEmptyResponse{}, sdkErrors.Wrap(authorizationTypes.ErrNoPermission, authorizationTypes.WalletUpdateSegment)
	}

	res, errSegment := k.walletKeeper.UpdateSegment(goCtx, &tokenwalletTypes.MsgUpdateSegment{
		Creator: msg.Creator,
		Id:      msg.Id,
		Name:    msg.Name,
		Info:    msg.Info,
	})
	if errSegment != nil {
		return &tokenwalletTypes.MsgEmptyResponse{}, errSegment
	}

	return res, nil
}
