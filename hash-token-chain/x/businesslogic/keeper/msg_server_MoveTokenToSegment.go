// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkErrors "github.com/cosmos/cosmos-sdk/types/errors"

	authorizationTypes "gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/TokenManager/x/authorization/types"
	"gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/TokenManager/x/businesslogic/types"
	tokenwalletTypes "gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/TokenManager/x/tokenwallet/types"
)

func (k msgServer) MoveTokenToSegment(goCtx context.Context, msg *types.MsgMoveTokenToSegment) (*tokenwalletTypes.MsgEmptyResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	// Permission check
	check, errRole := k.authorizationKeeper.HasRole(ctx, msg.Creator, authorizationTypes.WalletMoveTokenToSegment)
	if errRole != nil || !check {
		return &tokenwalletTypes.MsgEmptyResponse{}, sdkErrors.Wrap(authorizationTypes.ErrNoPermission, authorizationTypes.WalletMoveTokenToSegment)
	}

	res, errMoving := k.walletKeeper.MoveTokenToSegment(goCtx, &tokenwalletTypes.MsgMoveTokenToSegment{
		Creator:         msg.Creator,
		TokenRefId:      msg.TokenRefId,
		SourceSegmentId: msg.SourceSegmentId,
		TargetSegmentId: msg.TargetSegmentId,
	})
	if errMoving != nil {
		return &tokenwalletTypes.MsgEmptyResponse{}, errMoving
	}

	return res, nil
}
