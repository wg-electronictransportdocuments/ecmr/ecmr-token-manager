// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	"gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/TokenManager/x/businesslogic/types"
	tokenTypes "gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/TokenManager/x/token/types"
)

func (k Keeper) FetchTokenHistory(goCtx context.Context, msg *types.MsgFetchTokenHistory) (*types.MsgFetchTokenHistoryResponse, error) {
	res, _ := k.tokenKeeper.FetchTokenHistory(goCtx, &tokenTypes.MsgFetchTokenHistory{
		Creator: msg.Creator,
		Id:      msg.Id,
	})
	return &types.MsgFetchTokenHistoryResponse{TokenHistory: res.TokenHistory}, nil
}
