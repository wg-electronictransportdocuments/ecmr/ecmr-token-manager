// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkErrors "github.com/cosmos/cosmos-sdk/types/errors"

	authorizationTypes "gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/TokenManager/x/authorization/types"
	"gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/TokenManager/x/businesslogic/types"
	tokenwalletTypes "gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/TokenManager/x/tokenwallet/types"
)

func (k msgServer) FetchAllWallet(goCtx context.Context, msg *types.MsgFetchAllWallet) (*tokenwalletTypes.MsgFetchAllWalletResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	// Permission check
	check, errRole := k.authorizationKeeper.HasRole(ctx, msg.Creator, authorizationTypes.WalletQueryWalletAll)
	if errRole != nil || !check {
		return &tokenwalletTypes.MsgFetchAllWalletResponse{}, sdkErrors.Wrap(authorizationTypes.ErrNoPermission, authorizationTypes.WalletQueryWalletAll)
	}

	res, errWallets := k.walletKeeper.FetchWalletAll(goCtx, &tokenwalletTypes.MsgFetchAllWallet{
		Creator: msg.Creator,
	})
	if errWallets != nil {
		return &tokenwalletTypes.MsgFetchAllWalletResponse{}, errWallets
	}

	return res, nil
}
