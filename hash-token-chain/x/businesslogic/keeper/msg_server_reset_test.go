// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"
	"testing"

	sdk "github.com/cosmos/cosmos-sdk/types"

	authorizationTypes "gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/TokenManager/x/authorization/types"
	"gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/TokenManager/x/businesslogic/types"
	tokenwalletTypes "gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/TokenManager/x/tokenwallet/types"
)

func Test_msgServer_RevertModulesToGenesis(t *testing.T) {
	keeper, ctx := setupKeeper(t)
	goCtx := sdk.WrapSDKContext(ctx)
	type fields struct {
		Keeper Keeper
	}
	type args struct {
		goCtx context.Context
		msg   *types.MsgRevertToGenesis
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *types.MsgIdResponse
		wantErr bool
	}{
		// The test for denied transaction has to be first, since the successful transaction resets the configuration
		// and disables the permission check
		{
			name:    permissionDenied,
			fields:  fields{Keeper: *keeper},
			args:    args{goCtx: goCtx, msg: &types.MsgRevertToGenesis{Creator: creatorB}},
			wantErr: true,
		},
		{
			name:    successfulTx,
			fields:  fields{Keeper: *keeper},
			args:    args{goCtx: goCtx, msg: &types.MsgRevertToGenesis{Creator: creatorA}},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			k := msgServer{
				Keeper: tt.fields.Keeper,
			}
			k.walletKeeper.CreateWallet(sdk.WrapSDKContext(ctx), tokenwalletTypes.NewMsgCreateWallet(creatorA, "newWallet"))
			k.authorizationKeeper.SetApplicationRole(ctx, authorizationTypes.ApplicationRole{
				Creator: creatorA,
				Id:      authorizationTypes.BusinessLogicRevertModulesToGenesis,
				ApplicationRoleStates: []*authorizationTypes.ApplicationRoleState{
					{
						Creator:           creatorA,
						Id:                authorizationTypes.BusinessLogicRevertModulesToGenesis,
						ApplicationRoleID: authorizationTypes.BusinessLogicRevertModulesToGenesis,
						Description:       "",
						Valid:             true,
						TimeStamp:         "",
					},
				},
			})
			k.authorizationKeeper.SetBlockchainAccount(ctx, authorizationTypes.BlockchainAccount{
				Creator: creatorA,
				Id:      creatorA,
				BlockchainAccountStates: []*authorizationTypes.BlockchainAccountState{
					{
						Creator:            creatorA,
						Id:                 creatorA,
						AccountID:          creatorA,
						TimeStamp:          "",
						Valid:              true,
						ApplicationRoleIDs: []string{authorizationTypes.BusinessLogicRevertModulesToGenesis},
					},
				},
			},
			)
			got, err := k.RevertModulesToGenesis(tt.args.goCtx, tt.args.msg)
			if (err != nil) != tt.wantErr {
				t.Errorf("msgServer.StoreDocumentHash() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got == nil {
				t.Errorf("msgServer.StoreDocumentHash() error = %v, wantErr %v", got, "id not nil")
				return
			}
		})
	}
}
