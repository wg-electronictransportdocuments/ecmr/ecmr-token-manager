// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkErrors "github.com/cosmos/cosmos-sdk/types/errors"

	"gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/TokenManager/x/businesslogic/types"
	hashtokenTypes "gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/TokenManager/x/hashtoken/types"
)

func (k Keeper) DocumentHash(c context.Context, req *types.QueryGetDocumentHashRequest) (*types.QueryGetDocumentHashResponse, error) {
	ctx := sdk.UnwrapSDKContext(c)
	hashTokenKeeper := k.hashtokenKeeper

	id, exist := k.GetDocumentTokenMapper(ctx, req.Id)
	if !exist {
		return &types.QueryGetDocumentHashResponse{}, sdkErrors.Wrap(types.ErrNoMapping, "no mapping exist")
	}

	response, err := hashTokenKeeper.FetchToken(c, &hashtokenTypes.MsgFetchToken{Id: id.TokenId})
	if err != nil {
		return &types.QueryGetDocumentHashResponse{}, err
	}

	var token = *response.Token
	return &types.QueryGetDocumentHashResponse{
		Hash:         token.Info.Hash,
		HashFunction: token.Info.HashFunction,
		Metadata:     token.Info.Metadata,
		Creator:      token.Creator,
		Timestamp:    token.Timestamp,
	}, nil
}

func (k Keeper) DocumentHashHistory(c context.Context, req *types.QueryGetDocumentHashHistoryRequest) (*types.QueryGetDocumentHashHistoryResponse, error) {
	ctx := sdk.UnwrapSDKContext(c)
	id, exist := k.GetDocumentTokenMapper(ctx, req.Id)
	if !exist {
		return &types.QueryGetDocumentHashHistoryResponse{}, sdkErrors.Wrap(types.ErrNoMapping, "no mapping exists")
	}

	response, errToken := k.hashtokenKeeper.FetchToken(c, &hashtokenTypes.MsgFetchToken{Id: id.TokenId})
	if errToken != nil {
		return &types.QueryGetDocumentHashHistoryResponse{}, errToken
	}
	var token = *response.Token
	out := types.QueryGetDocumentHashHistoryResponse{
		History: []*types.QueryGetDocumentHashResponse{
			{
				Hash:         token.Info.Hash,
				HashFunction: token.Info.HashFunction,
				Metadata:     token.Info.Metadata,
				Creator:      token.Creator,
				Timestamp:    token.Timestamp,
			},
		},
	}

	tokenHistory, errTokenHistory := k.hashtokenKeeper.FetchTokenHistory(c, &hashtokenTypes.MsgFetchTokenHistory{Id: id.TokenId})
	if errTokenHistory != nil {
		return &types.QueryGetDocumentHashHistoryResponse{}, errToken
	}

	if tokenHistory.TokenHistory.History == nil {
		return &out, nil
	}

	for i := len(tokenHistory.TokenHistory.History) - 1; i >= 0; i-- {
		token := tokenHistory.TokenHistory.History[i]

		out.History = append(out.History, &types.QueryGetDocumentHashResponse{
			Hash:         token.Info.Hash,
			HashFunction: token.Info.HashFunction,
			Metadata:     token.Info.Metadata,
			Creator:      token.Creator,
			Timestamp:    token.Timestamp,
		})
	}

	return &out, nil
}
