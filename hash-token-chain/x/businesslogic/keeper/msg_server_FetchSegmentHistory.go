// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkErrors "github.com/cosmos/cosmos-sdk/types/errors"

	authorizationTypes "gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/TokenManager/x/authorization/types"
	"gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/TokenManager/x/businesslogic/types"
	tokenwalletTypes "gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/TokenManager/x/tokenwallet/types"
)

func (k msgServer) FetchSegmentHistory(goCtx context.Context, msg *types.MsgFetchSegmentHistory) (*tokenwalletTypes.MsgFetchGetSegmentHistoryResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	// Permission check
	check, errRole := k.authorizationKeeper.HasRole(ctx, msg.Creator, authorizationTypes.WalletQuerySegmentHistory)
	if errRole != nil || !check {
		return &tokenwalletTypes.MsgFetchGetSegmentHistoryResponse{}, sdkErrors.Wrap(authorizationTypes.ErrNoPermission, authorizationTypes.WalletQuerySegmentHistory)
	}

	res, errSegmentHistory := k.walletKeeper.FetchSegmentHistory(goCtx, &tokenwalletTypes.MsgFetchGetSegmentHistory{
		Creator: msg.Creator,
		Id:      msg.Id,
	})
	if errSegmentHistory != nil {
		return &tokenwalletTypes.MsgFetchGetSegmentHistoryResponse{}, errSegmentHistory
	}

	return res, nil
}
