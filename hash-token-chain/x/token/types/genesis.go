// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package types

import (
	"fmt"
)

// DefaultIndex is the default capability global index
const DefaultIndex uint64 = 1

// DefaultGenesis returns the default Capability genesis state
func DefaultGenesis() *GenesisState {
	return &GenesisState{
		// this line is used by starport scaffolding # genesis/types/default
		TokenHistoryList: []*TokenHistory{},
		TokenList:        []*Token{},
	}
}

// Validate performs basic genesis state validation returning an error upon any failure.
func (gs GenesisState) Validate() error {
	// this line is used by starport scaffolding # genesis/types/validate
	// Check for duplicated ID in tokenHistory
	tokenHistoryIdMap := make(map[string]bool)

	for _, elem := range gs.TokenHistoryList {
		if _, ok := tokenHistoryIdMap[elem.Id]; ok {
			return fmt.Errorf("duplicated id for tokenHistory")
		}
		tokenHistoryIdMap[elem.Id] = true
	}

	// Check for duplicated ID in token
	tokenIdMap := make(map[string]bool)

	for _, elem := range gs.TokenList {
		if _, ok := tokenIdMap[elem.Id]; ok {
			return fmt.Errorf("duplicated id for token")
		}
		tokenIdMap[elem.Id] = true
	}

	return nil
}
