// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package types

import (
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkErrors "github.com/cosmos/cosmos-sdk/types/errors"
)

var _ sdk.Msg = &MsgCreateTokenHistory{}

func NewMsgCreateTokenHistory(creator string, id string, history []*Token) *MsgCreateTokenHistory {
	return &MsgCreateTokenHistory{
		Creator: creator,
		Id:      id,
		History: history,
	}
}

func (msg *MsgCreateTokenHistory) Route() string {
	return RouterKey
}

func (msg *MsgCreateTokenHistory) Type() string {
	return "CreateTokenHistory"
}

func (msg *MsgCreateTokenHistory) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgCreateTokenHistory) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgCreateTokenHistory) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkErrors.Wrapf(sdkErrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgUpdateTokenHistory{}

func NewMsgUpdateTokenHistory(creator string, id string) *MsgUpdateTokenHistory {
	return &MsgUpdateTokenHistory{
		Id:      id,
		Creator: creator,
	}
}

func (msg *MsgUpdateTokenHistory) Route() string {
	return RouterKey
}

func (msg *MsgUpdateTokenHistory) Type() string {
	return "UpdateTokenHistory"
}

func (msg *MsgUpdateTokenHistory) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgUpdateTokenHistory) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgUpdateTokenHistory) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkErrors.Wrapf(sdkErrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}
