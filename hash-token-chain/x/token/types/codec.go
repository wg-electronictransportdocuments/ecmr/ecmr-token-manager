// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package types

import (
	"github.com/cosmos/cosmos-sdk/codec"
	codecTypes "github.com/cosmos/cosmos-sdk/codec/types"
	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/cosmos/cosmos-sdk/types/msgservice"
)

func RegisterCodec(cdc *codec.LegacyAmino) {
	// this line is used by starport scaffolding # 2
	cdc.RegisterConcrete(&MsgFetchAllToken{}, "token/FetchAllToken", nil)

	cdc.RegisterConcrete(&MsgFetchAllTokenHistory{}, "token/FetchAllTokenHistory", nil)
	cdc.RegisterConcrete(&MsgFetchTokenHistory{}, "token/FetchTokenHistory", nil)

	cdc.RegisterConcrete(&MsgFetchToken{}, "token/FetchToken", nil)

	cdc.RegisterConcrete(&MsgCreateTokenHistory{}, "Token/CreateTokenHistory", nil)
	cdc.RegisterConcrete(&MsgUpdateTokenHistory{}, "Token/UpdateTokenHistory", nil)

	cdc.RegisterConcrete(&MsgCreateToken{}, "Token/CreateToken", nil)
	cdc.RegisterConcrete(&MsgUpdateToken{}, "Token/UpdateToken", nil)

	cdc.RegisterConcrete(&MsgActivateToken{}, "Token/ActivateToken", nil)
	cdc.RegisterConcrete(&MsgDeactivateToken{}, "Token/DeactivateToken", nil)

	cdc.RegisterConcrete(&MsgUpdateTokenInformation{}, "Token/UpdateTokenInformation", nil)
}

func RegisterInterfaces(registry codecTypes.InterfaceRegistry) {
	// this line is used by starport scaffolding # 3
	registry.RegisterImplementations((*sdk.Msg)(nil),
		&MsgFetchAllToken{},
	)

	registry.RegisterImplementations((*sdk.Msg)(nil),
		&MsgFetchAllTokenHistory{},
	)

	registry.RegisterImplementations((*sdk.Msg)(nil),
		&MsgFetchTokenHistory{},
	)

	registry.RegisterImplementations((*sdk.Msg)(nil),
		&MsgFetchToken{},
	)

	registry.RegisterImplementations((*sdk.Msg)(nil),
		&MsgCreateTokenHistory{},
		&MsgUpdateTokenHistory{},
	)

	registry.RegisterImplementations((*sdk.Msg)(nil),
		&MsgCreateToken{},
		&MsgUpdateToken{},
	)

	registry.RegisterImplementations((*sdk.Msg)(nil),
		&MsgActivateToken{},
		&MsgDeactivateToken{},
	)

	registry.RegisterImplementations((*sdk.Msg)(nil),
		&MsgUpdateTokenInformation{},
	)

	msgservice.RegisterMsgServiceDesc(registry, &_Msg_serviceDesc)
}

var (
	amino     = codec.NewLegacyAmino()
	ModuleCdc = codec.NewAminoCodec(amino)
)
