// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper_test

import (
	"context"
	"reflect"
	"testing"

	sdk "github.com/cosmos/cosmos-sdk/types"

	"gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/TokenManager/app"
	"gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/TokenManager/x/hashtoken/keeper"
	"gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/TokenManager/x/hashtoken/types"
)

func Test_msgServer_FetchAllToken(t *testing.T) {
	newKeeper, ctx := setupKeeper(t)
	goCtx := sdk.WrapSDKContext(ctx)
	msgServer := keeper.NewMsgServerImpl(*newKeeper)

	type fields struct {
		Keeper keeper.Keeper
	}

	type args struct {
		goCtx context.Context
		msg   *types.MsgFetchAllToken
	}

	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *types.MsgFetchAllTokenResponse
		wantErr bool
	}{
		{
			name:    app.SuccessfulTx,
			fields:  fields{Keeper: *newKeeper},
			args:    args{goCtx: goCtx, msg: &types.MsgFetchAllToken{Creator: app.CreatorA}},
			want:    &types.MsgFetchAllTokenResponse{},
			wantErr: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := msgServer.FetchAllToken(tt.args.goCtx, tt.args.msg)
			if (err != nil) != tt.wantErr {
				t.Errorf("msgServer.FetchAllToken() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("msgServer.FetchAllToken() = %v, want %v", got, tt.want)
			}
		})
	}
}
