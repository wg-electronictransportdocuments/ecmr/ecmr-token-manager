// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper_test

import (
	"context"
	"reflect"
	"testing"

	sdk "github.com/cosmos/cosmos-sdk/types"

	"gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/TokenManager/app"
	"gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/TokenManager/x/hashtoken/keeper"
	"gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/TokenManager/x/hashtoken/types"
)

func Test_msgServer_createToken(t *testing.T) {
	newKeeper, ctx := setupKeeper(t)
	goCtx := sdk.WrapSDKContext(ctx)
	msgServer := keeper.NewMsgServerImpl(*newKeeper)

	type fields struct {
		Keeper keeper.Keeper
	}

	type args struct {
		goCtx context.Context
		msg   *types.MsgCreateToken
	}

	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *types.MsgIdResponse
		wantErr bool
	}{
		{
			name:   app.SuccessfulTx,
			fields: fields{Keeper: *newKeeper},
			args: args{goCtx: goCtx, msg: &types.MsgCreateToken{
				Creator:       app.CreatorA,
				TokenType:     app.TokenType,
				ChangeMessage: app.Changed,
				SegmentId:     app.SegmentId1,
			}},
			want:    &types.MsgIdResponse{},
			wantErr: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := msgServer.CreateToken(tt.args.goCtx, tt.args.msg)
			if (err != nil) != tt.wantErr {
				t.Errorf("msgServer.CreateToken() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("msgServer.CreateToken() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_msgServer_updateToken(t *testing.T) {
	newKeeper, ctx := setupKeeper(t)
	newKeeper.AppendToken(ctx, app.CreatorA, app.TokenId1, app.Timestamp, app.TokenType, app.Changed, app.SegmentId1, types.Info{})
	goCtx := sdk.WrapSDKContext(ctx)
	msgServer := keeper.NewMsgServerImpl(*newKeeper)

	type fields struct {
		Keeper keeper.Keeper
	}

	type args struct {
		goCtx context.Context
		msg   *types.MsgUpdateToken
	}

	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *types.MsgEmptyResponse
		wantErr bool
	}{
		{
			name:   app.SuccessfulTx,
			fields: fields{Keeper: *newKeeper},
			args: args{goCtx: goCtx, msg: &types.MsgUpdateToken{
				Creator:       app.CreatorA,
				Id:            app.TokenId1,
				TokenType:     app.TokenType,
				ChangeMessage: app.Changed,
				SegmentId:     app.SegmentId1,
			}},
			want:    &types.MsgEmptyResponse{},
			wantErr: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := msgServer.UpdateToken(tt.args.goCtx, tt.args.msg)
			if (err != nil) != tt.wantErr {
				t.Errorf("msgServer.UpdateToken() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("msgServer.UpdateToken() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_msgServer_activateToken(t *testing.T) {
	newKeeper, ctx := setupKeeper(t)
	newKeeper.AppendToken(ctx, app.CreatorA, app.TokenId1, app.Timestamp, app.TokenType, app.Changed, app.SegmentId1, types.Info{})
	goCtx := sdk.WrapSDKContext(ctx)
	msgServer := keeper.NewMsgServerImpl(*newKeeper)

	type fields struct {
		Keeper keeper.Keeper
	}

	type args struct {
		goCtx context.Context
		msg   *types.MsgActivateToken
	}

	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *types.MsgEmptyResponse
		wantErr bool
	}{
		{
			name:   app.SuccessfulTx,
			fields: fields{Keeper: *newKeeper},
			args: args{goCtx: goCtx, msg: &types.MsgActivateToken{
				Creator: app.CreatorA,
				Id:      app.TokenId1,
			}},
			want:    &types.MsgEmptyResponse{},
			wantErr: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := msgServer.ActivateToken(tt.args.goCtx, tt.args.msg)
			if (err != nil) != tt.wantErr {
				t.Errorf("msgServer.ActivateToken() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("msgServer.ActivateToken() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_msgServer_DeactivateToken(t *testing.T) {
	newKeeper, ctx := setupKeeper(t)
	newKeeper.AppendToken(ctx, app.CreatorA, app.TokenId1, app.Timestamp, app.TokenType, app.Changed, app.SegmentId1, types.Info{})
	goCtx := sdk.WrapSDKContext(ctx)
	msgServer := keeper.NewMsgServerImpl(*newKeeper)

	type fields struct {
		Keeper keeper.Keeper
	}

	type args struct {
		goCtx context.Context
		msg   *types.MsgDeactivateToken
	}

	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *types.MsgEmptyResponse
		wantErr bool
	}{
		{
			name:   app.SuccessfulTx,
			fields: fields{Keeper: *newKeeper},
			args: args{goCtx: goCtx, msg: &types.MsgDeactivateToken{
				Creator: app.CreatorA,
				Id:      app.TokenId1,
			}},
			want:    &types.MsgEmptyResponse{},
			wantErr: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := msgServer.DeactivateToken(tt.args.goCtx, tt.args.msg)
			if (err != nil) != tt.wantErr {
				t.Errorf("msgServer.DeactivateToken() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("msgServer.DeactivateToken() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_msgServer_updateTokenInformation(t *testing.T) {
	newKeeper, ctx := setupKeeper(t)
	newKeeper.AppendToken(ctx, app.CreatorA, app.TokenId1, app.Timestamp, app.TokenType, app.Changed, app.SegmentId1, types.Info{})
	goCtx := sdk.WrapSDKContext(ctx)
	msgServer := keeper.NewMsgServerImpl(*newKeeper)

	type fields struct {
		Keeper keeper.Keeper
	}

	type args struct {
		goCtx context.Context
		msg   *types.MsgUpdateTokenInformation
	}

	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *types.MsgEmptyResponse
		wantErr bool
	}{
		{
			name:   app.SuccessfulTx,
			fields: fields{Keeper: *newKeeper},
			args: args{goCtx: goCtx, msg: &types.MsgUpdateTokenInformation{
				Creator:       app.CreatorA,
				TokenId:       app.TokenId1,
				ChangeMessage: app.Changed,
				Document:      app.Document,
				Hash:          app.Hash,
				HashFunction:  app.HashFunction,
				Metadata:      app.Metadata,
			}},
			want:    &types.MsgEmptyResponse{},
			wantErr: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := msgServer.UpdateTokenInformation(tt.args.goCtx, tt.args.msg)
			if (err != nil) != tt.wantErr {
				t.Errorf("msgServer.UpdateTokenInformation() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("msgServer.UpdateTokenInformation() = %v, want %v", got, tt.want)
			}
		})
	}
}
