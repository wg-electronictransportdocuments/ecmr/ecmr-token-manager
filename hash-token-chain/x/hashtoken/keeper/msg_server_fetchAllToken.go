// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	"github.com/cosmos/cosmos-sdk/types/query"

	"gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/TokenManager/x/hashtoken/types"
)

func (k Keeper) FetchAllToken(goCtx context.Context, msg *types.MsgFetchAllToken) (*types.MsgFetchAllTokenResponse, error) {
	res, _ := k.TokenAll(goCtx, &types.QueryAllTokenRequest{Pagination: &query.PageRequest{}})
	return &types.MsgFetchAllTokenResponse{Token: res.Token}, nil
}
