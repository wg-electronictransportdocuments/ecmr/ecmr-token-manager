// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	"gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/TokenManager/x/hashtoken/types"
)

func (k Keeper) FetchToken(goCtx context.Context, msg *types.MsgFetchToken) (*types.MsgFetchTokenResponse, error) {
	res, _ := k.Token(goCtx, &types.QueryGetTokenRequest{Id: msg.Id})
	return &types.MsgFetchTokenResponse{Token: res.Token}, nil
}
