// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package Wallet

import (
	"fmt"

	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkErrors "github.com/cosmos/cosmos-sdk/types/errors"

	"gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/TokenManager/x/tokenwallet/keeper"
	"gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/TokenManager/x/tokenwallet/types"
)

// NewHandler ...
func NewHandler(k keeper.Keeper) sdk.Handler {
	// msgServer := keeper.NewMsgServerImpl(k)

	return func(ctx sdk.Context, msg sdk.Msg) (*sdk.Result, error) {
		ctx = ctx.WithEventManager(sdk.NewEventManager())

		switch msg := msg.(type) {
		// this line is used by starport scaffolding # 1

		/*
			case *types.MsgMoveTokenToSegment:
				res, err := msgServer.MoveTokenToSegment(sdk.WrapSDKContext(ctx), msg)
				return sdk.WrapServiceResult(ctx, res, err)

			case *types.MsgCreateSegment:
				res, err := msgServer.CreateSegment(sdk.WrapSDKContext(ctx), msg)
				return sdk.WrapServiceResult(ctx, res, err)

			case *types.MsgCreateSegmentWithId:
				res, err := msgServer.CreateSegmentWithId(sdk.WrapSDKContext(ctx), msg)
				return sdk.WrapServiceResult(ctx, res, err)

			case *types.MsgUpdateSegment:
				res, err := msgServer.UpdateSegment(sdk.WrapSDKContext(ctx), msg)
				return sdk.WrapServiceResult(ctx, res, err)

			case *types.MsgCreateWallet:
				res, err := msgServer.CreateWallet(sdk.WrapSDKContext(ctx), msg)
				return sdk.WrapServiceResult(ctx, res, err)

			case *types.MsgCreateWalletWithId:
				res, err := msgServer.CreateWalletWithId(sdk.WrapSDKContext(ctx), msg)
				return sdk.WrapServiceResult(ctx, res, err)

			case *types.MsgUpdateWallet:
				res, err := msgServer.UpdateWallet(sdk.WrapSDKContext(ctx), msg)
				return sdk.WrapServiceResult(ctx, res, err)

			case *types.MsgAssignCosmosAddressToWallet:
				res, err := msgServer.AssignCosmosAddressToWallet(sdk.WrapSDKContext(ctx), msg)
				return sdk.WrapServiceResult(ctx, res, err)

			case *types.MsgCreateTokenRef:
				res, err := msgServer.CreateTokenRef(sdk.WrapSDKContext(ctx), msg)
				return sdk.WrapServiceResult(ctx, res, err)

			case *types.MsgRemoveTokenRefFromSegment:
				res, err := msgServer.RemoveTokenRefFromSegment(sdk.WrapSDKContext(ctx), msg)
				return sdk.WrapServiceResult(ctx, res, err)

			case *types.MsgFetchGetTokenHistoryGlobal:
				res, err := msgServer.FetchTokenHistoryGlobal(sdk.WrapSDKContext(ctx), msg)
				return sdk.WrapServiceResult(ctx, res, err)

			case *types.MsgFetchAllTokenHistoryGlobal:
				res, err := msgServer.FetchTokenHistoryGlobalAll(sdk.WrapSDKContext(ctx), msg)
				return sdk.WrapServiceResult(ctx, res, err)

			case *types.MsgFetchGetSegmentHistory:
				res, err := msgServer.FetchSegmentHistory(sdk.WrapSDKContext(ctx), msg)
				return sdk.WrapServiceResult(ctx, res, err)

			case *types.MsgFetchAllSegmentHistory:
				res, err := msgServer.FetchSegmentHistoryAll(sdk.WrapSDKContext(ctx), msg)
				return sdk.WrapServiceResult(ctx, res, err)

			case *types.MsgFetchGetSegment:
				res, err := msgServer.FetchSegment(sdk.WrapSDKContext(ctx), msg)
				return sdk.WrapServiceResult(ctx, res, err)

			case *types.MsgFetchAllSegment:
				res, err := msgServer.FetchSegmentAll(sdk.WrapSDKContext(ctx), msg)
				return sdk.WrapServiceResult(ctx, res, err)

			case *types.MsgFetchGetWalletHistory:
				res, err := msgServer.FetchWalletHistory(sdk.WrapSDKContext(ctx), msg)
				return sdk.WrapServiceResult(ctx, res, err)

			case *types.MsgFetchAllWalletHistory:
				res, err := msgServer.FetchWalletHistoryAll(sdk.WrapSDKContext(ctx), msg)
				return sdk.WrapServiceResult(ctx, res, err)

			case *types.MsgFetchGetWallet:
				res, err := msgServer.FetchWallet(sdk.WrapSDKContext(ctx), msg)
				return sdk.WrapServiceResult(ctx, res, err)

			case *types.MsgFetchAllWallet:
				res, err := msgServer.FetchWalletAll(sdk.WrapSDKContext(ctx), msg)
				return sdk.WrapServiceResult(ctx, res, err)
		*/

		default:
			errMsg := fmt.Sprintf("unrecognized %s message type: %T", types.ModuleName, msg)
			return nil, sdkErrors.Wrap(sdkErrors.ErrUnknownRequest, errMsg)
		}
	}
}
