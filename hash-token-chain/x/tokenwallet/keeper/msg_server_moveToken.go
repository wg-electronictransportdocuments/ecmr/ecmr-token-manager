// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkErrors "github.com/cosmos/cosmos-sdk/types/errors"

	"gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/TokenManager/x/tokenwallet/types"
)

func (k Keeper) MoveTokenToSegment(goCtx context.Context, msg *types.MsgMoveTokenToSegment) (*types.MsgEmptyResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	if !k.HasSegment(ctx, msg.SourceSegmentId) || !k.HasSegment(ctx, msg.TargetSegmentId) {
		return nil, sdkErrors.ErrKeyNotFound
	}

	k.MoveToken(ctx, *msg)
	return &types.MsgEmptyResponse{}, nil
}

func (k Keeper) MoveTokenToWallet(goCtx context.Context, msg *types.MsgMoveTokenToWallet) (*types.MsgEmptyResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	if !k.HasWallet(ctx, msg.TargetWalletId) || !k.HasSegment(ctx, msg.SourceSegmentId) {
		return nil, sdkErrors.ErrKeyNotFound
	}

	segments := k.GetWallet(ctx, msg.TargetWalletId)
	inboxId := segments.SegmentIds[0]
	k.MoveToken(ctx, types.MsgMoveTokenToSegment{Creator: msg.Creator, TokenRefId: msg.TokenRefId, SourceSegmentId: msg.SourceSegmentId, TargetSegmentId: inboxId})

	sourceSegment := k.GetSegment(ctx, msg.SourceSegmentId)
	k.CreateTokenHistoryGlobalEntry(ctx, msg.Creator, msg.TokenRefId, sourceSegment.WalletId)
	return &types.MsgEmptyResponse{}, nil
}

func (k Keeper) RemoveTokenRefFromSegment(goCtx context.Context, msg *types.MsgRemoveTokenRefFromSegment) (*types.MsgEmptyResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	if !k.HasSegment(ctx, msg.SegmentId) {
		return nil, sdkErrors.ErrKeyNotFound
	}

	k.RemoveToken(ctx, *msg)
	return &types.MsgEmptyResponse{}, nil
}
