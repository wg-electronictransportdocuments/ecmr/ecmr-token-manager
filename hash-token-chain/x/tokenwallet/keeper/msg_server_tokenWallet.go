// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"
	"fmt"

	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkErrors "github.com/cosmos/cosmos-sdk/types/errors"

	"gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/TokenManager/x/tokenwallet/types"
)

func (k Keeper) CreateWallet(goCtx context.Context, msg *types.MsgCreateWallet) (*types.MsgIdResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	id := k.AppendWallet(ctx, *msg)

	return &types.MsgIdResponse{
		Id: id,
	}, nil
}

func (k Keeper) CreateWalletWithId(goCtx context.Context, msg *types.MsgCreateWalletWithId) (*types.MsgIdResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	id := k.AppendWalletWithId(ctx, *msg)

	return &types.MsgIdResponse{
		Id: id,
	}, nil
}

func (k Keeper) UpdateWallet(goCtx context.Context, msg *types.MsgUpdateWallet) (*types.MsgEmptyResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	oldWallet := k.GetWallet(ctx, msg.Id)
	var wallet = types.Wallet{
		Creator:        msg.Creator,
		Id:             msg.Id,
		Name:           msg.Name,
		Timestamp:      GetTimestamp(),
		SegmentIds:     oldWallet.SegmentIds,
		WalletAccounts: oldWallet.WalletAccounts,
	}

	// Checks that the element exists
	if !k.HasWallet(ctx, msg.Id) {
		return nil, sdkErrors.Wrap(sdkErrors.ErrKeyNotFound, fmt.Sprintf("key %s doesn't exist", msg.Id))
	}

	// Checks if the msg sender is the same as the current owner
	if msg.Creator != k.GetWalletOwner(ctx, msg.Id) {
		return nil, sdkErrors.Wrap(sdkErrors.ErrUnauthorized, "incorrect owner")
	}

	k.SetWallet(ctx, wallet)

	return &types.MsgEmptyResponse{}, nil
}

func (k Keeper) AssignCosmosAddressToWallet(goCtx context.Context, msg *types.MsgAssignCosmosAddressToWallet) (*types.MsgEmptyResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	// Checks that the element exists
	if !k.HasWallet(ctx, msg.WalletId) {
		return nil, sdkErrors.Wrap(sdkErrors.ErrKeyNotFound, fmt.Sprintf("key %s doesn't exist", msg.WalletId))
	}

	// Checks if the msg sender is the same as the current owner
	if msg.Creator != k.GetWalletOwner(ctx, msg.WalletId) {
		return nil, sdkErrors.Wrap(sdkErrors.ErrUnauthorized, "incorrect owner")
	}

	var account = types.WalletAccount{
		Address: msg.CosmosAddress,
		Active:  true,
	}

	if !k.HasWallet(ctx, msg.WalletId) {
		return nil, sdkErrors.ErrKeyNotFound
	}

	wallet := k.GetWallet(ctx, msg.WalletId)
	wallet.WalletAccounts = append(wallet.WalletAccounts, &account)
	k.SetWallet(ctx, wallet)

	return &types.MsgEmptyResponse{}, nil
}
