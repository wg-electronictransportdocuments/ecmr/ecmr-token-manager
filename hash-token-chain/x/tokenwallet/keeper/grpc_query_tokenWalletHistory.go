// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	"github.com/cosmos/cosmos-sdk/store/prefix"
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkErrors "github.com/cosmos/cosmos-sdk/types/errors"
	"github.com/cosmos/cosmos-sdk/types/query"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/TokenManager/x/tokenwallet/types"
)

func (k Keeper) WalletHistoryAll(c context.Context, req *types.QueryAllWalletHistoryRequest) (*types.QueryAllWalletHistoryResponse, error) {
	ctx := sdk.UnwrapSDKContext(c)
	var walletHistories []*types.WalletHistory
	store := ctx.KVStore(k.storeKey)
	walletHistoryStore := prefix.NewStore(store, types.KeyPrefix(types.WalletHistoryKey))

	pageRes, err := query.Paginate(walletHistoryStore, req.Pagination, func(key []byte, value []byte) error {
		var walletHistory types.WalletHistory
		if err := k.cdc.Unmarshal(value, &walletHistory); err != nil {
			return err
		}

		walletHistories = append(walletHistories, &walletHistory)
		return nil
	})

	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &types.QueryAllWalletHistoryResponse{WalletHistory: walletHistories, Pagination: pageRes}, nil
}

func (k Keeper) WalletHistory(c context.Context, req *types.QueryGetWalletHistoryRequest) (*types.QueryGetWalletHistoryResponse, error) {
	ctx := sdk.UnwrapSDKContext(c)

	if !k.HasWalletHistory(ctx, req.Id) {
		return nil, sdkErrors.ErrKeyNotFound
	}

	walletHistory := k.GetWalletHistory(ctx, req.Id)
	return &types.QueryGetWalletHistoryResponse{WalletHistory: &walletHistory}, nil
}
