// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"strconv"

	"github.com/cosmos/cosmos-sdk/store/prefix"
	sdk "github.com/cosmos/cosmos-sdk/types"

	"gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/TokenManager/x/tokenwallet/types"
)

// GetWalletHistoryCount gets the total number of walletHistory
func (k Keeper) GetWalletHistoryCount(ctx sdk.Context) int64 {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.WalletHistoryCountKey))
	byteKey := types.KeyPrefix(types.WalletHistoryCountKey)
	bz := store.Get(byteKey)

	// Count doesn't exist: no element
	if bz == nil {
		return 0
	}

	// Parse bytes
	count, err := strconv.ParseInt(string(bz), 10, 64)
	if err != nil {
		// Panic because the count should be always formattable to int64
		panic("cannot decode count")
	}

	return count
}

// SetWalletHistoryCount sets the total number of walletHistory
func (k Keeper) SetWalletHistoryCount(ctx sdk.Context, count int64) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.WalletHistoryCountKey))
	byteKey := types.KeyPrefix(types.WalletHistoryCountKey)
	bz := []byte(strconv.FormatInt(count, 10))
	store.Set(byteKey, bz)
}

// AppendWalletHistory appends a walletHistory in the store with a new id and updates the count
func (k Keeper) AppendWalletHistory(ctx sdk.Context, msg types.MsgCreateWalletHistory) string {
	// Create the walletHistory
	var walletHistory = types.WalletHistory{
		Creator: msg.Creator,
		Id:      msg.Id,
		History: msg.History,
	}

	k.SetWalletHistory(ctx, walletHistory)

	// Update walletHistory count
	count := k.GetWalletHistoryCount(ctx)
	k.SetWalletHistoryCount(ctx, count+1)

	return msg.Id
}

// SetWalletHistory sets a specific walletHistory in the store
func (k Keeper) SetWalletHistory(ctx sdk.Context, walletHistory types.WalletHistory) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.WalletHistoryKey))
	b := k.cdc.MustMarshal(&walletHistory)
	store.Set(types.KeyPrefix(types.WalletHistoryKey+walletHistory.Id), b)
}

// GetWalletHistory returns a walletHistory from its id
func (k Keeper) GetWalletHistory(ctx sdk.Context, key string) types.WalletHistory {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.WalletHistoryKey))
	var walletHistory types.WalletHistory
	k.cdc.MustUnmarshal(store.Get(types.KeyPrefix(types.WalletHistoryKey+key)), &walletHistory)
	return walletHistory
}

// HasWalletHistory checks if the walletHistory exists in the store
func (k Keeper) HasWalletHistory(ctx sdk.Context, id string) bool {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.WalletHistoryKey))
	return store.Has(types.KeyPrefix(types.WalletHistoryKey + id))
}

// GetWalletHistoryOwner returns the creator of the walletHistory
func (k Keeper) GetWalletHistoryOwner(ctx sdk.Context, key string) string {
	return k.GetWalletHistory(ctx, key).Creator
}

// GetAllWalletHistory returns all walletHistory
func (k Keeper) GetAllWalletHistory(ctx sdk.Context) (msgs []types.WalletHistory) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.WalletHistoryKey))
	iterator := sdk.KVStorePrefixIterator(store, types.KeyPrefix(types.WalletHistoryKey))

	defer iterator.Close()

	for ; iterator.Valid(); iterator.Next() {
		var msg types.WalletHistory
		k.cdc.MustUnmarshal(iterator.Value(), &msg)
		msgs = append(msgs, msg)
	}

	return
}
