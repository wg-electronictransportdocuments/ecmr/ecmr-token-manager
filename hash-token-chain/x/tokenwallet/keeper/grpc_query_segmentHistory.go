// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	"github.com/cosmos/cosmos-sdk/store/prefix"
	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/cosmos/cosmos-sdk/types/query"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/TokenManager/x/tokenwallet/types"
)

func (k Keeper) SegmentHistoryAll(c context.Context, req *types.QueryAllSegmentHistoryRequest) (*types.QueryAllSegmentHistoryResponse, error) {
	ctx := sdk.UnwrapSDKContext(c)
	var segmentHistories []*types.SegmentHistory
	store := ctx.KVStore(k.storeKey)
	segmentHistoryStore := prefix.NewStore(store, types.KeyPrefix(types.SegmentHistoryKey))

	pageRes, err := query.Paginate(segmentHistoryStore, req.Pagination, func(key []byte, value []byte) error {
		var segmentHistory types.SegmentHistory
		if err := k.cdc.Unmarshal(value, &segmentHistory); err != nil {
			return err
		}

		segmentHistories = append(segmentHistories, &segmentHistory)
		return nil
	})

	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &types.QueryAllSegmentHistoryResponse{SegmentHistory: segmentHistories, Pagination: pageRes}, nil
}

func (k Keeper) SegmentHistory(c context.Context, req *types.QueryGetSegmentHistoryRequest) (*types.QueryGetSegmentHistoryResponse, error) {
	ctx := sdk.UnwrapSDKContext(c)
	var segmentHistory types.SegmentHistory
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.SegmentHistoryKey))
	k.cdc.MustUnmarshal(store.Get(types.KeyPrefix(types.SegmentHistoryKey+req.Id)), &segmentHistory)

	return &types.QueryGetSegmentHistoryResponse{SegmentHistory: &segmentHistory}, nil
}
