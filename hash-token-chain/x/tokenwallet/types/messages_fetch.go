// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package types

import (
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkErrors "github.com/cosmos/cosmos-sdk/types/errors"
)

var _ sdk.Msg = &MsgFetchGetTokenHistoryGlobal{}

func NewMsgFetchGetTokenHistoryGlobal(creator string, id string) *MsgFetchGetTokenHistoryGlobal {
	return &MsgFetchGetTokenHistoryGlobal{
		Creator: creator,
		Id:      id,
	}
}

func (msg *MsgFetchGetTokenHistoryGlobal) Route() string {
	return RouterKey
}

func (msg *MsgFetchGetTokenHistoryGlobal) Type() string {
	return "FetchGetTokenHistoryGlobal"
}

func (msg *MsgFetchGetTokenHistoryGlobal) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgFetchGetTokenHistoryGlobal) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgFetchGetTokenHistoryGlobal) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkErrors.Wrapf(sdkErrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgFetchAllTokenHistoryGlobal{}

func NewMsgFetchAllTokenHistoryGlobal(creator string) *MsgFetchAllTokenHistoryGlobal {
	return &MsgFetchAllTokenHistoryGlobal{
		Creator: creator,
	}
}

func (msg *MsgFetchAllTokenHistoryGlobal) Route() string {
	return RouterKey
}

func (msg *MsgFetchAllTokenHistoryGlobal) Type() string {
	return "FetchAllTokenHistoryGlobal"
}

func (msg *MsgFetchAllTokenHistoryGlobal) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgFetchAllTokenHistoryGlobal) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgFetchAllTokenHistoryGlobal) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkErrors.Wrapf(sdkErrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgFetchGetSegmentHistory{}

func NewMsgFetchGetSegmentHistory(creator string, id string) *MsgFetchGetSegmentHistory {
	return &MsgFetchGetSegmentHistory{
		Creator: creator,
		Id:      id,
	}
}

func (msg *MsgFetchGetSegmentHistory) Route() string {
	return RouterKey
}

func (msg *MsgFetchGetSegmentHistory) Type() string {
	return "FetchGetSegmentHistory"
}

func (msg *MsgFetchGetSegmentHistory) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgFetchGetSegmentHistory) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgFetchGetSegmentHistory) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkErrors.Wrapf(sdkErrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgFetchAllSegmentHistory{}

func NewMsgFetchAllSegmentHistory(creator string) *MsgFetchAllSegmentHistory {
	return &MsgFetchAllSegmentHistory{
		Creator: creator,
	}
}

func (msg *MsgFetchAllSegmentHistory) Route() string {
	return RouterKey
}

func (msg *MsgFetchAllSegmentHistory) Type() string {
	return "FetchAllSegmentHistory"
}

func (msg *MsgFetchAllSegmentHistory) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgFetchAllSegmentHistory) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgFetchAllSegmentHistory) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkErrors.Wrapf(sdkErrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgFetchGetSegment{}

func NewMsgFetchGetSegment(creator string, id string) *MsgFetchGetSegment {
	return &MsgFetchGetSegment{
		Creator: creator,
		Id:      id,
	}
}

func (msg *MsgFetchGetSegment) Route() string {
	return RouterKey
}

func (msg *MsgFetchGetSegment) Type() string {
	return "FetchGetSegment"
}

func (msg *MsgFetchGetSegment) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgFetchGetSegment) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgFetchGetSegment) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkErrors.Wrapf(sdkErrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgFetchAllSegment{}

func NewMsgFetchAllSegment(creator string) *MsgFetchAllSegment {
	return &MsgFetchAllSegment{
		Creator: creator,
	}
}

func (msg *MsgFetchAllSegment) Route() string {
	return RouterKey
}

func (msg *MsgFetchAllSegment) Type() string {
	return "FetchAllSegment"
}

func (msg *MsgFetchAllSegment) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgFetchAllSegment) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgFetchAllSegment) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkErrors.Wrapf(sdkErrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgFetchGetWalletHistory{}

func NewMsgFetchGetWalletHistory(creator string, id string) *MsgFetchGetWalletHistory {
	return &MsgFetchGetWalletHistory{
		Creator: creator,
		Id:      id,
	}
}

func (msg *MsgFetchGetWalletHistory) Route() string {
	return RouterKey
}

func (msg *MsgFetchGetWalletHistory) Type() string {
	return "FetchGetWalletHistory"
}

func (msg *MsgFetchGetWalletHistory) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgFetchGetWalletHistory) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgFetchGetWalletHistory) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkErrors.Wrapf(sdkErrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgFetchAllWalletHistory{}

func NewMsgFetchAllWalletHistory(creator string) *MsgFetchAllWalletHistory {
	return &MsgFetchAllWalletHistory{
		Creator: creator,
	}
}

func (msg *MsgFetchAllWalletHistory) Route() string {
	return RouterKey
}

func (msg *MsgFetchAllWalletHistory) Type() string {
	return "FetchAllWalletHistory"
}

func (msg *MsgFetchAllWalletHistory) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgFetchAllWalletHistory) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgFetchAllWalletHistory) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkErrors.Wrapf(sdkErrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgFetchGetWallet{}

func NewMsgFetchGetWallet(creator string, id string) *MsgFetchGetWallet {
	return &MsgFetchGetWallet{
		Creator: creator,
		Id:      id,
	}
}

func (msg *MsgFetchGetWallet) Route() string {
	return RouterKey
}

func (msg *MsgFetchGetWallet) Type() string {
	return "FetchGetWallet"
}

func (msg *MsgFetchGetWallet) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgFetchGetWallet) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgFetchGetWallet) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkErrors.Wrapf(sdkErrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgFetchAllWallet{}

func NewMsgFetchAllWallet(creator string) *MsgFetchAllWallet {
	return &MsgFetchAllWallet{
		Creator: creator,
	}
}

func (msg *MsgFetchAllWallet) Route() string {
	return RouterKey
}

func (msg *MsgFetchAllWallet) Type() string {
	return "FetchAllWallet"
}

func (msg *MsgFetchAllWallet) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgFetchAllWallet) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgFetchAllWallet) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkErrors.Wrapf(sdkErrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}
