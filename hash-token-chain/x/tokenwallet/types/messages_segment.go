// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package types

import (
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkErrors "github.com/cosmos/cosmos-sdk/types/errors"
)

var _ sdk.Msg = &MsgCreateSegment{}

func NewMsgCreateSegment(creator string, name string, info string, walletId string) *MsgCreateSegment {
	return &MsgCreateSegment{
		Creator:  creator,
		Name:     name,
		Info:     info,
		WalletId: walletId,
	}
}

func (msg *MsgCreateSegment) Route() string {
	return RouterKey
}

func (msg *MsgCreateSegment) Type() string {
	return "CreateSegment"
}

func (msg *MsgCreateSegment) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgCreateSegment) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgCreateSegment) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkErrors.Wrapf(sdkErrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgCreateSegmentWithId{}

func NewMsgCreateSegmentWithId(creator string, id string, name string, info string, walletId string) *MsgCreateSegmentWithId {
	return &MsgCreateSegmentWithId{
		Creator:  creator,
		Id:       id,
		Name:     name,
		Info:     info,
		WalletId: walletId,
	}
}

func (msg *MsgCreateSegmentWithId) Route() string {
	return RouterKey
}

func (msg *MsgCreateSegmentWithId) Type() string {
	return "CreateSegmentWithId"
}

func (msg *MsgCreateSegmentWithId) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgCreateSegmentWithId) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgCreateSegmentWithId) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkErrors.Wrapf(sdkErrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgUpdateSegment{}

func NewMsgUpdateSegment(creator string, id string, name string, info string) *MsgUpdateSegment {
	return &MsgUpdateSegment{
		Id:      id,
		Creator: creator,
		Name:    name,
		Info:    info,
	}
}

func (msg *MsgUpdateSegment) Route() string {
	return RouterKey
}

func (msg *MsgUpdateSegment) Type() string {
	return "UpdateSegment"
}

func (msg *MsgUpdateSegment) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgUpdateSegment) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgUpdateSegment) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkErrors.Wrapf(sdkErrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgCreateTokenRef{}

func NewMsgCreateTokenRef(creator string, id string, moduleRef string, segmentId string) *MsgCreateTokenRef {
	return &MsgCreateTokenRef{
		Creator:   creator,
		Id:        id,
		ModuleRef: moduleRef,
		SegmentId: segmentId,
	}
}

func (msg *MsgCreateTokenRef) Route() string {
	return RouterKey
}

func (msg *MsgCreateTokenRef) Type() string {
	return "CreateTokenRef"
}

func (msg *MsgCreateTokenRef) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgCreateTokenRef) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgCreateTokenRef) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkErrors.Wrapf(sdkErrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgMoveTokenToSegment{}

func NewMsgMoveTokenToSegment(creator string, tokenRefId string, sourceSegmentId string, targetSegmentId string) *MsgMoveTokenToSegment {
	return &MsgMoveTokenToSegment{
		Creator:         creator,
		TokenRefId:      tokenRefId,
		SourceSegmentId: sourceSegmentId,
		TargetSegmentId: targetSegmentId,
	}
}

func (msg *MsgMoveTokenToSegment) Route() string {
	return RouterKey
}

func (msg *MsgMoveTokenToSegment) Type() string {
	return "MoveTokenToSegment"
}

func (msg *MsgMoveTokenToSegment) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgMoveTokenToSegment) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgMoveTokenToSegment) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkErrors.Wrapf(sdkErrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgMoveTokenToWallet{}

func NewMsgMoveTokenToWallet(creator string, tokenRefId string, sourceSegmentId string, targetWalletId string) *MsgMoveTokenToWallet {
	return &MsgMoveTokenToWallet{
		Creator:         creator,
		TokenRefId:      tokenRefId,
		SourceSegmentId: sourceSegmentId,
		TargetWalletId:  targetWalletId,
	}
}

func (msg *MsgMoveTokenToWallet) Route() string {
	return RouterKey
}

func (msg *MsgMoveTokenToWallet) Type() string {
	return "MoveTokenToWallet"
}

func (msg *MsgMoveTokenToWallet) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgMoveTokenToWallet) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgMoveTokenToWallet) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkErrors.Wrapf(sdkErrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgRemoveTokenRefFromSegment{}

func NewMsgRemoveTokenRefFromSegment(creator string, tokenRefId string, segmentId string) *MsgRemoveTokenRefFromSegment {
	return &MsgRemoveTokenRefFromSegment{
		Creator:    creator,
		TokenRefId: tokenRefId,
		SegmentId:  segmentId,
	}
}

func (msg *MsgRemoveTokenRefFromSegment) Route() string {
	return RouterKey
}

func (msg *MsgRemoveTokenRefFromSegment) Type() string {
	return "RemoveTokenRefFromSegment"
}

func (msg *MsgRemoveTokenRefFromSegment) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgRemoveTokenRefFromSegment) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgRemoveTokenRefFromSegment) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkErrors.Wrapf(sdkErrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}
