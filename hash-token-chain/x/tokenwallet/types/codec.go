// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package types

import (
	"github.com/cosmos/cosmos-sdk/codec"
	codecTypes "github.com/cosmos/cosmos-sdk/codec/types"
	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/cosmos/cosmos-sdk/types/msgservice"
)

func RegisterCodec(cdc *codec.LegacyAmino) {
	// this line is used by starport scaffolding # 2
	cdc.RegisterConcrete(&MsgCreateTokenHistoryGlobal{}, "Wallet/CreateTokenHistoryGlobal", nil)
	cdc.RegisterConcrete(&MsgUpdateTokenHistoryGlobal{}, "Wallet/UpdateTokenHistoryGlobal", nil)

	cdc.RegisterConcrete(&MsgCreateSegmentHistory{}, "Wallet/CreateSegmentHistory", nil)
	cdc.RegisterConcrete(&MsgUpdateSegmentHistory{}, "Wallet/UpdateSegmentHistory", nil)

	cdc.RegisterConcrete(&MsgCreateWalletHistory{}, "Wallet/CreateWalletHistory", nil)
	cdc.RegisterConcrete(&MsgUpdateWalletHistory{}, "Wallet/UpdateWalletHistory", nil)

	cdc.RegisterConcrete(&MsgCreateSegment{}, "Wallet/CreateSegment", nil)
	cdc.RegisterConcrete(&MsgUpdateSegment{}, "Wallet/UpdateSegment", nil)

	cdc.RegisterConcrete(&MsgCreateWallet{}, "Wallet/CreateWallet", nil)
	cdc.RegisterConcrete(&MsgUpdateWallet{}, "Wallet/UpdateWallet", nil)

	cdc.RegisterConcrete(&MsgAssignCosmosAddressToWallet{}, "Wallet/CreateWalletAccount", nil)
	cdc.RegisterConcrete(&MsgCreateTokenRef{}, "Wallet/CreateTokenRef", nil)
	cdc.RegisterConcrete(&MsgMoveTokenToSegment{}, "Wallet/MoveTokenToSegment", nil)
	cdc.RegisterConcrete(&MsgRemoveTokenRefFromSegment{}, "Wallet/RemoveTokenRefFromSegment", nil)

	cdc.RegisterConcrete(&MsgFetchGetTokenHistoryGlobal{}, "Wallet/FetchGetTokenHistoryGlobal", nil)
	cdc.RegisterConcrete(&MsgFetchAllTokenHistoryGlobal{}, "Wallet/FetchAllTokenHistoryGlobal", nil)
	cdc.RegisterConcrete(&MsgFetchGetSegmentHistory{}, "Wallet/FetchGetSegmentHistory", nil)
	cdc.RegisterConcrete(&MsgFetchAllSegmentHistory{}, "Wallet/FetchAllSegmentHistory", nil)
	cdc.RegisterConcrete(&MsgFetchGetSegment{}, "Wallet/FetchGetSegment", nil)
	cdc.RegisterConcrete(&MsgFetchAllSegment{}, "Wallet/FetchAllSegment", nil)
	cdc.RegisterConcrete(&MsgFetchGetWalletHistory{}, "Wallet/FetchGetWalletHistory", nil)
	cdc.RegisterConcrete(&MsgFetchAllWalletHistory{}, "Wallet/FetchAllWalletHistory", nil)
	cdc.RegisterConcrete(&MsgFetchGetWallet{}, "Wallet/FetchGetWallet", nil)
	cdc.RegisterConcrete(&MsgFetchAllWallet{}, "Wallet/FetchAllWallet", nil)
}

func RegisterInterfaces(registry codecTypes.InterfaceRegistry) {
	// this line is used by starport scaffolding # 3
	registry.RegisterImplementations((*sdk.Msg)(nil),
		&MsgCreateTokenHistoryGlobal{},
		&MsgUpdateTokenHistoryGlobal{},
	)

	registry.RegisterImplementations((*sdk.Msg)(nil),
		&MsgCreateSegmentHistory{},
		&MsgUpdateSegmentHistory{},
	)

	registry.RegisterImplementations((*sdk.Msg)(nil),
		&MsgCreateWalletHistory{},
		&MsgUpdateWalletHistory{},
	)

	registry.RegisterImplementations((*sdk.Msg)(nil),
		&MsgCreateSegment{},
		&MsgUpdateSegment{},
	)

	registry.RegisterImplementations((*sdk.Msg)(nil),
		&MsgCreateWallet{},
		&MsgUpdateWallet{},
	)

	registry.RegisterImplementations((*sdk.Msg)(nil),
		&MsgAssignCosmosAddressToWallet{},
	)

	registry.RegisterImplementations((*sdk.Msg)(nil),
		&MsgCreateTokenRef{},
		&MsgMoveTokenToSegment{},
		&MsgRemoveTokenRefFromSegment{},
	)

	registry.RegisterImplementations((*sdk.Msg)(nil),
		&MsgFetchGetTokenHistoryGlobal{},
		&MsgFetchAllTokenHistoryGlobal{},
		&MsgFetchGetSegmentHistory{},
		&MsgFetchAllSegmentHistory{},
		&MsgFetchGetSegment{},
		&MsgFetchAllSegment{},
		&MsgFetchGetWalletHistory{},
		&MsgFetchAllWalletHistory{},
		&MsgFetchGetWallet{},
		&MsgFetchAllWallet{},
	)

	msgservice.RegisterMsgServiceDesc(registry, &_Msg_serviceDesc)
}

var (
	amino     = codec.NewLegacyAmino()
	ModuleCdc = codec.NewAminoCodec(amino)
)
