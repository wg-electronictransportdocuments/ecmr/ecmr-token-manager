<!--
Copyright Open Logistics Foundation

Licensed under the Open Logistics Foundation License 1.3.
For details on the licensing terms, see the LICENSE file.
SPDX-License-Identifier: OLFL-1.3
-->

# Token Manager

**Token Manager** is a blockchain built using Cosmos SDK and Tendermint and created
with [ignite](https://github.com/ignite/cli).

## Cloning and updating of the repository

The Token Manager uses the modules
[TokenWallet](https://gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/tokenmanager.wallet),
[Token](https://gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/tokenmanager.token),
[BusinessLogic](https://gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/tokenmanager.business-logic)
and
[HashToken](https://gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/tokenmanager.hash-token)
, which are included as submodules. To clone all repositories at once, you can use the command

```
git clone --recursive git@gitlab.cc-asp.fraunhofer.de:silicon-economy/base/blockchainbroker/tokenmanager/tokenmanager.tokenmanager.git
```

To update all repositories at once, you can use

```
git pull --recurse-submodules
```

To update the submodules use

```
git submodule update --init --recursive
```

## Documentation

The [documentation](https://gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/tokenmanager.arc42/-/blob/main/index.adoc)
provides extensive information about the Token Manager and its components.

## Installation and Configuration

The installation and setup of the Token Manager is described
in [chapter 12](https://gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/tokenmanager/tokenmanager.arc42/-/blob/main/index.adoc)
of the documentation.

Your blockchain can be configured with `config.yml` during development. To learn more see
the [ignite documentation](https://github.com/ignite/cli#documentation).

## Launch

To launch your blockchain live on multiple nodes use `ignite network` commands. Learn more
about [Starport Network](https://github.com/tendermint/spn).

## CLI commands

The following commands are supported by the token manager module

### Transactions

All business logic module transaction commands need a preceding `TokenManagerd tx BusinessLogic `

* `create-hash-token [change-message] [segment-id] [document] [hash] [hash-function] [metadata]`
* `fetch-document-hash [id]` to fetch a document hash

## Learn more

- [ignite](https://github.com/ignite/cli)
- [Cosmos SDK documentation](https://docs.cosmos.network)
- [Cosmos SDK Tutorials](https://tutorials.cosmos.network)
- [Discord](https://discord.gg/W8trcGV)
