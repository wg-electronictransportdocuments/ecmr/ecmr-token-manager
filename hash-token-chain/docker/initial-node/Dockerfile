# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

# builder
FROM ignitehq/cli:0.23.0 as builder
WORKDIR /go/src/TokenManager

USER root
COPY . .

RUN ignite chain init

# deployer
FROM alpine as deployer
WORKDIR /root

COPY --from=builder /go/bin/TokenManagerd /bin/TokenManagerd
COPY --from=builder /go/src/TokenManager/docker/initial-node/initialize-node.sh .

# After the volume in K8s is attached, the content in .TokenManager will be emptied
COPY --from=builder /root/.TokenManager /backup

RUN apk add --no-cache \
 bash \
 libc6-compat \
 gcompat \
 libgcc

RUN chmod -R 777 .
RUN chmod -R 777 /backup

EXPOSE 26656 26657

CMD ./initialize-node.sh
