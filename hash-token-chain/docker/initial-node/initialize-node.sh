#!/bin/bash

# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

# Turn on bash's job control
set -m

mkdir -p ~/.TokenManager

# Perform actions only if directory is actually empty
if [ -z "$(ls -A ~/.TokenManager)" ]; then
  echo "### Copy backed up files"
  cp -r /backup/. ~/.TokenManager
  chmod -R 777 ~/.TokenManager
  printf '### Backed up files copied: %s\n\n\n' "$(ls -la ~/.TokenManager)"
fi

# Start the primary process and put it in the background
echo "### Start TokenManager"
TokenManagerd start --log_level error &
printf "### TokenManager started\n\n\n"

echo "### Write node id to ~/.TokenManager/node-id"
TokenManagerd tendermint show-node-id >~/.TokenManager/node-id
printf '### node id written: %s\n\n\n' "$(cat ~/.TokenManager/node-id)"

echo "### Write ip address to ~/.TokenManager/ip-address"
ifconfig eth0 | grep 'inet addr' | cut -d: -f2 | awk '{print $1}' >~/.TokenManager/ip-address
printf '### ip address written: %s\n\n\n' "$(cat ~/.TokenManager/ip-address)"

# Wait for tendermint demon start
sleep 20s

# Create account on demand
if TokenManagerd keys list | grep "$ACCOUNT_NAME"; then
  printf '### Account %s already exists\n\n\n' "$ACCOUNT_NAME"
else
  echo "### Create account $ACCOUNT_NAME"
  printf '%s\n' "$ACCOUNT_MNEMONIC" | TokenManagerd keys add "$ACCOUNT_NAME" --recover || exit 1
  printf '### Account created\n\n\n'

  echo "### Send 1 token from alice to $ACCOUNT_NAME"
  TokenManagerd tx bank send "$(TokenManagerd keys show alice -a)" "$(TokenManagerd keys show "$ACCOUNT_NAME" -a)" 1token -y
  printf '### 1 token sent\n\n\n'
fi

# Create module related application roles

# Bring the primary process back into the foreground
fg %1
