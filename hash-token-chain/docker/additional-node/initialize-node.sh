#!/bin/bash

# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

# Turn on bash's job control
set -m

# Perform actions only if directory is actually empty
if [ -z "$(ls -A ~/.TokenManager)" ]; then
  echo "### Initialize blockchain"
  TokenManagerd init "$NODE_NAME"
  printf '### Blockchain initialized\n\n\n'

  echo "### Create account $ACCOUNT_NAME"
  printf '%s\n%s\n%s\n' "$ACCOUNT_MNEMONIC" "$KEYRING_PASSPHRASE" "$KEYRING_PASSPHRASE" | TokenManagerd keys add "$ACCOUNT_NAME" --recover || exit 1
  printf '### Account created\n\n\n'

  if [ -e ~/files/genesis.json ]; then
    echo "### Copy genesis.json"
    cp ~/files/genesis.json ~/.TokenManager/config/genesis.json
    printf '### File copied: %s\n\n\n' "$(ls -la ~/.TokenManager/config/genesis.json)"
  else
    echo "### genesis.json not found in \"~/files\""
    rm -rf ~/.TokenManager
    echo "### Please provide the genesis.json and restart the container."
    exit 1
  fi

  echo "### Update persistent_peers in config.toml"
  sed -i "s/persistent_peers\ =\ \"\"/persistent_peers\ =\ \"${INITIAL_NODE_ID}@${INITIAL_NODE_IP_ADDRESS}:26656\"/g" ~/.TokenManager/config/config.toml
  printf '### persistent_peers updated: %s\n\n\n' "$(cat $HOME/.TokenManager/config/config.toml | grep 'persistent_peers =')"

  # Explanation: https://github.com/tendermint/tendermint/issues/1736#issuecomment-396884985
  echo "### Update addr_book_strict in config.toml"
  sed -i "s/addr_book_strict\ =\ true/addr_book_strict\ =\ false/g" ~/.TokenManager/config/config.toml
  printf '### addr_book_strict updated: %s\n\n\n' "$(cat $HOME/.TokenManager/config/config.toml | grep 'addr_book_strict =')"
fi

# Start the primary process and put it in the background
echo "### Start TokenManager"

# TODO-MP: add flags
TokenManagerd start &
printf "### TokenManager started\n\n\n"

# Wait for tendermint demon start
sleep 20s

# Create module related application roles

# Bring the primary process back into the foreground
fg %1
